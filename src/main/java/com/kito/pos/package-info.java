/**
 * Main package. This package containing application entry point for spring boot application.
 *
 * @author abdulhakam
 */
package com.kito.pos;