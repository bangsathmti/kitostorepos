package com.kito.pos.config;

import com.kito.pos.domain.master.UserApp;
import lombok.NoArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;

/**
 * @author <a href="https://web.facebook.com/jmd.juraganempangdollar/">Abdul
 * Hakam</a>.<br>
 * Created on Mar 11, 2022, at 2:27:32 AM
 */
@Component
@NoArgsConstructor
@Log
public class RequestFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (request.getRequestURI().startsWith("/login")
                || request.getRequestURI().startsWith("/category")
                || request.getRequestURI().startsWith("/commodity")
                || request.getRequestURI().startsWith("/dashboard")
                || request.getRequestURI().startsWith("/salesMeasurement")
                || request.getRequestURI().startsWith("/supplier")
                || request.getRequestURI().startsWith("/unitMeasurement")
                || request.getRequestURI().startsWith("/userApp")
                || request.getRequestURI().startsWith("/sales")
                || request.getRequestURI().startsWith("/purchasing")
                || request.getRequestURI().startsWith("/reporting")
                || request.getRequestURI().equals("/")) {
            if (!request.getRequestURI().startsWith("/login")) {
                UserApp userApp = (UserApp) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
                log.log(Level.INFO, "=======================================================================================================");
                log.log(Level.INFO, "User : {0}", userApp.getUsername());
            } else {
                log.log(Level.INFO, "=======================================================================================================");
            }
            log.log(Level.INFO, "Access uri : {0}", request.getRequestURI());
            log.log(Level.INFO, "Access method : {0}", request.getMethod());
            log.log(Level.INFO, "Remote address : {0}", request.getRemoteAddr());
            log.log(Level.INFO, "Param : {0}", request.getQueryString());
        }
        filterChain.doFilter(request, response);
    }

}
