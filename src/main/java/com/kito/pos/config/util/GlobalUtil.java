package com.kito.pos.config.util;

import org.jetbrains.annotations.NotNull;

import java.text.DecimalFormat;
import java.util.Random;

/**
 * @author <a href="https://web.facebook.com/jmd.juraganempangdollar/">Abdul
 * Hakam</a>.<br>
 * Created on Mar 12, 2022, at 9:48:08 AM
 */
public class GlobalUtil {

    /**
     * A function to convert a floating-point value to Indonesian currency
     * format in string type. This function uses the DecimalFormat class to do
     * it.
     *
     * @param value Value to be converted
     * @return A decimal format string value of floating-point
     * @see DecimalFormat
     */
    public static @NotNull
    String convertToCurrencyFormat(double value) {
        DecimalFormat formatter = new DecimalFormat();
        if (value < 0) {
            return formatter.format(value).replace("-", "-Rp").replace(",", "#")
                    .replace(".", ",").replace("#", ".");
        } else {
            return "Rp " + formatter.format(value).replace(",", "#")
                    .replace(".", ",").replace("#", ".");
        }
    }

    public static int getCheckSumNumber(@NotNull String code) {
        int val = 0;
        for (int i = 0; i < code.length(); i++) {
            val += (Integer.parseInt(code.charAt(i) + "")) * ((i % 2 == 0) ? 1 : 3);
        }
        int checksum_digit = 10 - (val % 10);
        if (checksum_digit == 10) {
            checksum_digit = 0;
        }
        return checksum_digit;
    }

    public static String upperCaseFirstChar(String string) {
        return string.toUpperCase().charAt(0) + string.toLowerCase().substring(1, string.length());
    }

    public static long generateDefaultBarcode() {
        Random random = new Random();
        char[] digits = new char[12];
        digits[0] = (char) (random.nextInt(9) + '1');
        for (int i = 1; i < digits.length; i++) {
            digits[i] = (char) (random.nextInt(10) + '0');
        }
        return Long.parseLong(new String(digits));
    }
}
