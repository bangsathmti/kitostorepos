package com.kito.pos.config;

import com.kito.pos.service.master.UserAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.session.HttpSessionEventPublisher;

/**
 * @author <a href="https://web.facebook.com/jmd.juraganempangdollar/">Abdul
 * Hakam</a>.<br>
 * Created on Mar 7, 2022, at 6:09:19 AM
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserAppService userAppService;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public SecurityConfig(UserAppService userAppService, PasswordEncoder passwordEncoder) {
        this.userAppService = userAppService;
        this.passwordEncoder = passwordEncoder;
    }

    @Bean
    public HttpSessionEventPublisher httpSessionEventPublisher() {
        return new HttpSessionEventPublisher();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userAppService).passwordEncoder(passwordEncoder);
    }

    @Bean
    @Override
    protected UserDetailsService userDetailsService() {
        return userAppService;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/webjars/**", "/dist/img/**").permitAll()
                .anyRequest().authenticated()
                .and().formLogin().loginPage("/login").defaultSuccessUrl("/", true).permitAll()
                .and().logout().deleteCookies("remember-me", "JSESSIONID").clearAuthentication(true)
                .invalidateHttpSession(true)
                .and().sessionManagement().maximumSessions(1).maxSessionsPreventsLogin(true);
    }

}
