package com.kito.pos.config;

import com.kito.pos.domain.master.UserApp;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

/**
 * @author <a href="https://web.facebook.com/jmd.juraganempangdollar/">Abdul
 * Hakam</a>.<br>
 * Created on Feb 9, 2022, at 5:56:54 PM
 */
@EnableJpaAuditing(auditorAwareRef = "auditorProvider")
@Configuration
public class AuditorConfig implements AuditorAware<UserApp> {

    @Bean
    public AuditorAware<UserApp> auditorProvider() {
        return new AuditorConfig();
    }

    @Override
    public Optional<UserApp> getCurrentAuditor() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null || !authentication.isAuthenticated()) {
            return Optional.empty();
        }
        return Optional.of((UserApp) authentication.getPrincipal());
    }
}
