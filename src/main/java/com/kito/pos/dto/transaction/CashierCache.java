package com.kito.pos.dto.transaction;

import com.kito.pos.config.util.GlobalUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Apr 06, 2022, at 9:15:02 AM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CashierCache {
    private Long commodityId;
    private String commodityName;
    private String salesMeasurementName;
    private int amount;
    private BigDecimal unitPrice;

    public String getUnitPriceString(){
        return GlobalUtil.convertToCurrencyFormat(unitPrice.doubleValue());
    }

    public String getAmountString(){
        return amount + " " + salesMeasurementName;
    }

    public String getTotal(){
        return GlobalUtil.convertToCurrencyFormat(unitPrice.doubleValue() * amount);
    }
}
