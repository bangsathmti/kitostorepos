package com.kito.pos.dto.transaction;

import com.kito.pos.domain.transaction.PurchasingDetail;
import com.kito.pos.domain.transaction.PurchasingDetailPK;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 12, 2022, at 6:06:35 PM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PurchasingDetailLite {

    private PurchasingDetailPK id;
    private Integer amount;
    private BigDecimal unitPrice;

    public PurchasingDetail convert() {
        return new PurchasingDetail(id, amount, unitPrice, null, null);
    }
}
