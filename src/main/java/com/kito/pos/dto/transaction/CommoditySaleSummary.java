package com.kito.pos.dto.transaction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommoditySaleSummary implements Comparable<CommoditySaleSummary>{

    private String name;
    private String salesMeasurementName;
    private Long amount;

    public String getAmountString(){
        return amount + " " + salesMeasurementName;
    }

    @Override
    public int compareTo(@NotNull CommoditySaleSummary o) {
        return o.getAmount().compareTo(amount);
    }
}
