package com.kito.pos.dto.transaction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 21, 2022, at 9:15:02 AM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EditCache {

    private Long commodityId;
    private int amount;
}
