package com.kito.pos.dto.transaction;

import com.kito.pos.domain.transaction.SalesDetail;
import com.kito.pos.domain.transaction.SalesDetailPK;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 19, 2022, at 6:11:33 PM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SalesDetailLite {

    private SalesDetailPK id;
    private Integer amount;
    private BigDecimal unitPrice;

    public SalesDetail convert() {
        return new SalesDetail(id, amount, unitPrice, null, null);
    }
}
