package com.kito.pos.dto.transaction;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.kito.pos.domain.master.Supplier;
import com.kito.pos.domain.transaction.Purchasing;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Lite version of @{@link Purchasing} model.
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 12, 2022, at 5:32:30 PM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PurchasingLite {

    private String purchaseNumber;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date purchaseDate;
    private Long supplierId;
    private String supplierName;
    private String note;

    public Purchasing convert() {
        return new Purchasing(purchaseNumber, new Supplier(supplierId), purchaseDate, note, null);
    }
}
