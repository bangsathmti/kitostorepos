package com.kito.pos.dto.transaction;

import com.kito.pos.domain.transaction.Sales;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 19, 2022, at 6:05:54 PM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SalesLite {

    private String salesNumber;
    private BigDecimal personalDiscount;

    public Sales convert() {
        return new Sales(salesNumber, personalDiscount, null);
    }
}
