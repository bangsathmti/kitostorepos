package com.kito.pos.dto.master;

import com.kito.pos.domain.master.UnitMeasurement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 11, 2022, at 11:00:00 AM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UnitMeasurementLite {

    @NotNull
    private Long id;
    @NotNull
    private String name;
    private String description;

    public UnitMeasurement convert() {
        return new UnitMeasurement(id, name, description);
    }
}
