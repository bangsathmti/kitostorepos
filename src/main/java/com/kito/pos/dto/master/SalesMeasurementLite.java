package com.kito.pos.dto.master;

import com.kito.pos.domain.master.SalesMeasurement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 10, 2022, at 5:15:27 PM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SalesMeasurementLite {

    @NotNull
    private Long id;
    @NotNull
    private String name;
    @NotNull
    private String shortName;
    private String description;

    public SalesMeasurement convert() {
        return new SalesMeasurement(id, name, shortName, description);
    }
}
