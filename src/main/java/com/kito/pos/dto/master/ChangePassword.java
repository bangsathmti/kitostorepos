package com.kito.pos.dto.master;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 20, 2022, at 10:04:14 AM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChangePassword {

    private String oldPassword;
    private String newPassword;
    private String newPasswordConfirmation;
}
