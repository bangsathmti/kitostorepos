package com.kito.pos.dto.master;

import com.kito.pos.domain.master.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 10, 2022, at 10:07:45 AM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategoryLite {

    @NotNull
    private Long id;
    @NotNull
    private String name;
    private String description;

    public Category convert() {
        return new Category(id, name, description);
    }
}
