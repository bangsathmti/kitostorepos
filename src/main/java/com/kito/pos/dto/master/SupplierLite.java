package com.kito.pos.dto.master;

import com.kito.pos.domain.master.Supplier;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 9, 2022, at 12:45:57 PM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SupplierLite {

    @NotNull
    private Long id;
    @NotNull
    private String name;
    @NotNull
    private String contact;
    private String address;

    public Supplier convert() {
        return new Supplier(id, name, contact, address);
    }
}
