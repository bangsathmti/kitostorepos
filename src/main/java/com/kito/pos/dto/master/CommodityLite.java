package com.kito.pos.dto.master;

import com.kito.pos.config.util.GlobalUtil;
import com.kito.pos.domain.master.Category;
import com.kito.pos.domain.master.Commodity;
import com.kito.pos.domain.master.SalesMeasurement;
import com.kito.pos.domain.master.UnitMeasurement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.krysalis.barcode4j.impl.upcean.EAN13Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;

import java.awt.image.BufferedImage;
import java.math.BigDecimal;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 10, 2022, at 4:16:40 PM
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommodityLite implements Comparable<CommodityLite>{

    private Long id;
    private String name;
    private String description;
    private Long category;
    private String categoryName;
    private Long unitMeasurement;
    private String unitMeasurementName;
    private Long salesMeasurement;
    private String salesMeasurementName;
    private BigDecimal salesPrice;
    private Long stock;

    public CommodityLite(Long id, String name, String description, Long category, String categoryName, Long unitMeasurement,
                         String unitMeasurementName, Long salesMeasurement, String salesMeasurementName, BigDecimal salesPrice) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.category = category;
        this.categoryName = categoryName;
        this.unitMeasurement = unitMeasurement;
        this.unitMeasurementName = unitMeasurementName;
        this.salesMeasurement = salesMeasurement;
        this.salesMeasurementName = salesMeasurementName;
        this.salesPrice = salesPrice;
        this.stock = 0L;
    }

    public Commodity convert() {
        return new Commodity(id, name, new Category(category), new UnitMeasurement(unitMeasurement),
                new SalesMeasurement(salesMeasurement), salesPrice, description);
    }

    @Override
    public int compareTo(CommodityLite o) {
        return o.getStock().compareTo(stock);
    }

    public String getStockString(){
        return stock + " " + salesMeasurementName;
    }

    public String getStockInCurrency(){
        return GlobalUtil.convertToCurrencyFormat(stock * salesPrice.doubleValue());
    }

    public BufferedImage generateBarcode4j() {
        EAN13Bean barcodeGenerator = new EAN13Bean();
        barcodeGenerator.setFontName("dejavu");
        BitmapCanvasProvider canvas
                = new BitmapCanvasProvider(160, BufferedImage.TYPE_BYTE_BINARY, false, 0);
        barcodeGenerator.generateBarcode(canvas, String.format("%1$" + 13 + "s", id.toString()
                + GlobalUtil.getCheckSumNumber(id.toString())).replace(' ', '0'));
        return canvas.getBufferedImage();
    }

    public String getSalesPriceString(){
        return GlobalUtil.convertToCurrencyFormat(salesPrice.doubleValue());
    }
}
