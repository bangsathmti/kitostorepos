package com.kito.pos.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kito.pos.domain.master.UserApp;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author <a href="https://web.facebook.com/jmd.juraganempangdollar/">Abdul
 * Hakam</a>.<br>
 * Created on Feb 9, 2022, at 6:00:58 PM
 */
@Data
@MappedSuperclass
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class Auditor implements Serializable {

    @CreatedBy
    @JoinColumn(name = "created_by", referencedColumnName = "username", updatable = false)
    @ManyToOne(optional = false)
    @JsonIgnore
    private UserApp createdBy;
    @Basic(optional = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @Column(updatable = false)
    @JsonIgnore
    private Date createdDate;

    @CreatedBy
    @LastModifiedBy
    @JoinColumn(name = "modified_by", referencedColumnName = "username")
    @ManyToOne(optional = false)
    @JsonIgnore
    private UserApp lastModifiedBy;
    @Basic(optional = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @LastModifiedDate
    @JsonIgnore
    private Date lastModifiedDate;
}
