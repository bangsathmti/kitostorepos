package com.kito.pos.domain.transaction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 12, 2022, at 10:15:02 AM
 */
@Data
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class SalesDetailPK implements Serializable {

    private static final long serialVersionUID = 3287648723648L;
    @Column(name = "sales_number")
    private String salesNumber;
    @Column(name = "commodity_id")
    private Long commodityId;
}
