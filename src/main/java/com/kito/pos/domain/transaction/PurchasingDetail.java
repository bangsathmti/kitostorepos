package com.kito.pos.domain.transaction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kito.pos.config.util.GlobalUtil;
import com.kito.pos.domain.Auditor;
import com.kito.pos.domain.master.Commodity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 9, 2022, at 4:47:02 PM
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "purchasing_details")
@EqualsAndHashCode(callSuper = false)
public class PurchasingDetail extends Auditor implements Serializable {

    private static final long serialVersionUID = 3287648723648L;
    @EmbeddedId
    private PurchasingDetailPK id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private int amount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "unit_price")
    @Min(value = 0)
    private BigDecimal unitPrice;
    @JsonIgnore
    @JoinColumn(name = "purchase_number", referencedColumnName = "purchase_number", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Purchasing purchasing;
    @JoinColumn(name = "commodity_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Commodity commodity;

    public String getUnitPriceString(){
        return GlobalUtil.convertToCurrencyFormat(unitPrice.doubleValue());
    }

    public String getAmountString(){
        return amount + " " + commodity.getSalesMeasurement().getName();
    }

    public String getTotal() {
        return GlobalUtil.convertToCurrencyFormat(unitPrice.doubleValue() * amount);
    }
}