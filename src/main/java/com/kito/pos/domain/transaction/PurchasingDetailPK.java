package com.kito.pos.domain.transaction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 9, 2022, at 4:48:25 PM
 */
@Data
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class PurchasingDetailPK implements Serializable {

    private static final long serialVersionUID = 3287648723648L;
    @Column(name = "purchase_number")
    private String purchaseNumber;
    @Column(name = "commodity_id")
    private Long commodityId;
}
