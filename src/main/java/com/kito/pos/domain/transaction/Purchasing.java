package com.kito.pos.domain.transaction;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.kito.pos.config.util.GlobalUtil;
import com.kito.pos.domain.Auditor;
import com.kito.pos.domain.master.Supplier;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 9, 2022, at 4:41:32 PM
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "purchasing")
@EqualsAndHashCode(callSuper = false)
public class Purchasing extends Auditor implements Serializable {

    private static final long serialVersionUID = 3287648723648L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "purchase_number")
    private String purchaseNumber;
    @JoinColumn(name = "supplier_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Supplier supplier;
    @Basic(optional = false)
    @NotNull()
    @Column(name = "purchase_date")
    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date purchaseDate;
    @Size(max = 255)
    @Column(name = "note")
    private String note;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "purchase_number", referencedColumnName = "purchase_number", insertable = false, updatable = false)
    private Collection<PurchasingDetail> purchasingDetails;

    public String getGrandTotal(){
        double grandTotal = 0;
        if (!purchasingDetails.isEmpty()){
            for (PurchasingDetail purchasingDetail:purchasingDetails) {
                grandTotal += (purchasingDetail.getAmount() * purchasingDetail.getUnitPrice().doubleValue());
            }
        }
        return GlobalUtil.convertToCurrencyFormat(grandTotal);
    }
}