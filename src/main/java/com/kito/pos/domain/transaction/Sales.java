package com.kito.pos.domain.transaction;

import com.kito.pos.config.util.GlobalUtil;
import com.kito.pos.domain.Auditor;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 12, 2022, at 10:09:13 AM
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "sales")
@EqualsAndHashCode(callSuper = false)
public class Sales extends Auditor implements Serializable {

    private static final long serialVersionUID = 3287648723648L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "sales_number")
    private String salesNumber;
    @Basic(optional = false)
    @NotNull
    @Column(name = "personal_discount")
    private BigDecimal personalDiscount;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "sales_number", referencedColumnName = "sales_number", insertable = false, updatable = false)
    private Collection<SalesDetail> salesDetails;

    public String getPersonalDiscountString(){
        return GlobalUtil.convertToCurrencyFormat(personalDiscount.doubleValue());
    }

    public String getGrandTotal(){
        double totalPayment = 0;
        if (!salesDetails.isEmpty()){
            for (SalesDetail salesDetail: salesDetails) {
                totalPayment += (salesDetail.getAmount() * salesDetail.getUnitPrice().doubleValue());
            }
        }
        return GlobalUtil.convertToCurrencyFormat(totalPayment);
    }

    public String getCashierName(){
        return super.getLastModifiedBy().getName();
    }

    public Date getSalesDate() {
        return super.getCreatedDate();
    }

    public String getTotalPayment(){
        double totalPayment = 0;
        if (!salesDetails.isEmpty()){
            for (SalesDetail salesDetail: salesDetails) {
                totalPayment += (salesDetail.getAmount() * salesDetail.getUnitPrice().doubleValue());
            }
        }
        return GlobalUtil.convertToCurrencyFormat(totalPayment - personalDiscount.doubleValue());
    }
}