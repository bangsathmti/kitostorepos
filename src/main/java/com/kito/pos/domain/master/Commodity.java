package com.kito.pos.domain.master;

import com.kito.pos.domain.Auditor;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 9, 2022, at 4:16:24 PM
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "commodity")
@EqualsAndHashCode(callSuper = false)
public class Commodity extends Auditor implements Serializable{

    private static final long serialVersionUID = 3287648723648L;
    @Id
    @Basic(optional = false)
    @Column(length = 12)
    private Long id;
    @Basic(optional = false)
    @NotNull(message = "The commodity name cannot be empty")
    @Size(min = 1, max = 255)
    @Column(name = "name", unique = true)
    private String name;
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Category category;
    @JoinColumn(name = "unit_measurement_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private UnitMeasurement unitMeasurement;
    @JoinColumn(name = "sales_measurement_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private SalesMeasurement salesMeasurement;
    @Min(value = 0, message = "Sales price must be greater than 0")
    @Basic(optional = false)
    @NotNull
    @Column(name = "sales_price")
    private BigDecimal salesPrice;
    @Size(max = 255)
    @Column(name = "description")
    private String description;
}
