package com.kito.pos.domain.master;

import com.kito.pos.domain.Auditor;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 11, 2022, at 10:58:08 AM
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "unit_measurement")
@EqualsAndHashCode(callSuper = false)
public class UnitMeasurement extends Auditor implements Serializable {

    private static final long serialVersionUID = 3287648723648L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "name", unique = true)
    private String name;
    @Size(max = 255)
    @Column(name = "description")
    private String description;

    public UnitMeasurement(long id){
        this.id = id;
    }
}