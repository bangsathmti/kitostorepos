package com.kito.pos.domain.master;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 24, 2022, at 2:36:59 PM
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "commodity_log")
public class CommodityLog implements Serializable {

    private static final long serialVersionUID = 3287648723648L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "log_id")
    private Long logId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id", updatable = false)
    private long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "name", updatable = false)
    private String name;
    @Size(max = 255)
    @Column(name = "description", updatable = false)
    private String description;
    @JoinColumn(name = "category_id", referencedColumnName = "id", updatable = false)
    @ManyToOne(optional = false)
    private Category category;
    @JoinColumn(name = "unit_measurement_id", referencedColumnName = "id", updatable = false)
    @ManyToOne(optional = false)
    private UnitMeasurement unitMeasurement;
    @JoinColumn(name = "sales_measurement_id", referencedColumnName = "id", updatable = false)
    @ManyToOne(optional = false)
    private SalesMeasurement salesMeasurement;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sales_price", updatable = false)
    private BigDecimal salesPrice;
    @JoinColumn(name = "created_by", referencedColumnName = "username", updatable = false)
    @ManyToOne(optional = false)
    private UserApp createdBy;
    @Basic(optional = false)
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date", updatable = false)
    private Date createdDate;
    @JoinColumn(name = "modified_by", referencedColumnName = "username", updatable = false)
    @ManyToOne(optional = false)
    private UserApp lastModifiedBy;
    @Basic(optional = false)
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_modified_date", updatable = false)
    private Date lastModifiedDate;

    public CommodityLog(Commodity commodity) {
        this.id = commodity.getId();
        this.name = commodity.getName();
        this.description = commodity.getName();
        this.category = commodity.getCategory();
        this.unitMeasurement = commodity.getUnitMeasurement();
        this.salesMeasurement = commodity.getSalesMeasurement();
        this.salesPrice = commodity.getSalesPrice();
        this.createdBy = commodity.getCreatedBy();
        this.createdDate = commodity.getCreatedDate();
        this.lastModifiedBy = commodity.getLastModifiedBy();
        this.lastModifiedDate = commodity.getLastModifiedDate();
    }
}
