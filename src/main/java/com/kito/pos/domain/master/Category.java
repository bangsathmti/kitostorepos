package com.kito.pos.domain.master;

import com.kito.pos.domain.Auditor;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * The category is used for classifying commodities so the customer can easily find what they need and the merchant has a standard structure to display items.
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 9, 2022, at 4:07:52 PM
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "category")
@EqualsAndHashCode(callSuper = false)
public class Category extends Auditor implements Serializable {

    /**
     * An identifier that is used to serialize/deserialize.
     * */
    private static final long serialVersionUID = 3287648723648L;
    /**
     * Category identifier.
     * */
    @Id
    @Column(name = "id")
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    /**
     * Category name.
     * */
    @Basic(optional = false)
    @Size(min = 1, max = 255)
    @Column(name = "name",unique = true)
    @NotNull(message = "The Category name cannot be empty")
    private String name;
    /**
     * Category description.
     * */
    @Size(max = 255)
    @Column(name = "description")
    private String description;

    public Category(long id){
        this.id = id;
    }
}