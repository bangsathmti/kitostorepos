package com.kito.pos.domain.master;

import com.kito.pos.domain.Auditor;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 9, 2022, at 3:59:14 PM
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "sales_measurement")
@EqualsAndHashCode(callSuper = false)
public class SalesMeasurement extends Auditor implements Serializable {

    /**
     * An identifier that is used to serialize/deserialize
     * */
    private static final long serialVersionUID = 3287648723648L;
    @Id
    @Column(name = "id")
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "name", unique = true)
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 7)
    @Column(name = "short_name")
    private String shortName;
    @Size(max = 255)
    @Column(name = "description")
    private String description;

    public SalesMeasurement(long id){
        this.id = id;
    }
}