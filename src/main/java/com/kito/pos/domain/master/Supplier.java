package com.kito.pos.domain.master;

import com.kito.pos.domain.Auditor;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 7, 2022, at 6:06:07 AM
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "supplier")
@EqualsAndHashCode(callSuper = false)
public class Supplier extends Auditor implements Serializable {

    private static final long serialVersionUID = 3287648723648L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "name", unique = true)
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "contact")
    private String contact;
    @Size(max = 255)
    @Column(name = "address")
    private String address;

    public Supplier(long id){
        this.id = id;
    }
}
