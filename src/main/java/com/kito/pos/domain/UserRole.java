package com.kito.pos.domain;

/**
 * User role enumeration.
 *
 * @author <a href="https://web.facebook.com/jmd.juraganempangdollar/">Abdul
 * Hakam</a>.<br>
 */
public enum UserRole {
    OWNER,
    MANAGEMENT,
    KEPALA_TOKO,
    ADMIN,
    KASIR
}
