/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Enum.java to edit this template
 */
package com.kito.pos.domain;

import java.util.HashMap;

/**
 *
 * @author abdulhakam
 */
public enum IndonesianMonth {
    JANUARI(0),
    FEBRUARI(1),
    MARET(2),
    APRIL(3),
    MEI(4),
    JUNI(5),
    JULI(6),
    AGUSTUS(7),
    SEPTEMBER(8),
    OKTOBER(9),
    NOVEMBER(10),
    DESEMBER(11);

    private final int monthCode;

    private static HashMap<Integer, IndonesianMonth> mappings;

    private static HashMap<Integer, IndonesianMonth> getMappings() {
        synchronized (IndonesianMonth.class) {
            if (mappings == null) {
                mappings = new java.util.HashMap<>();
            }
        }
        return mappings;
    }

    IndonesianMonth(final int value) {
        monthCode = value;
        getMappings().put(value, this);
    }

    public int getMonthCode() {
        return monthCode;
    }

    public static IndonesianMonth forValue(final int value) {
        IndonesianMonth type = getMappings().get(value);
        if (type == null) {
            throw new IllegalArgumentException(
                    "Invalid data type: " + String.valueOf(value));
        }
        return type;
    }
}
