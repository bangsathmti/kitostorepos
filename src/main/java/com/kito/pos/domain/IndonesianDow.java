package com.kito.pos.domain;

import java.util.HashMap;

/**
 *
 * @author abdulhakam
 */
public enum IndonesianDow {
    MINGGU(1),
    SENIN(2),
    SELASA(3),
    RABU(4),
    KAMIS(5),
    JUMAT(6),
    SABTU(7);

    private final int dayCode;

    private static HashMap<Integer, IndonesianDow> mappings;

    private static HashMap<Integer, IndonesianDow> getMappings() {
        synchronized (IndonesianDow.class) {
            if (mappings == null) {
                mappings = new java.util.HashMap<>();
            }
        }
        return mappings;
    }

    IndonesianDow(final int value) {
        dayCode = value;
        if (value < 1 || value > 7) {
            getMappings().put(1, this);
        } else {
            getMappings().put(value, this);
        }
    }

    public int getDayCode() {
        return dayCode;
    }

    public static IndonesianDow forValue(final int value) {
        IndonesianDow type = getMappings().get(value);
        if (type == null) {
            return MINGGU;
        }
        return type;
    }

}
