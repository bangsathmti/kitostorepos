package com.kito.pos.repository.master;

import com.kito.pos.domain.master.CommodityLog;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author abdulhakam
 */
public interface CommodityLogRepository extends JpaRepository<CommodityLog, Long> {

}
