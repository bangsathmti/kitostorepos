package com.kito.pos.repository.master;

import com.kito.pos.domain.master.Supplier;
import com.kito.pos.dto.master.SupplierLite;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 *
 * @author abdulhakam
 */
public interface SupplierRepository extends JpaRepository<Supplier, Long> {

    boolean existsByName(String name);

    boolean existsByIdNotAndName(Long id, String name);

    @Query("SELECT NEW com.kito.pos.dto.master.SupplierLite(tbl.id, tbl.name, tbl.contact, tbl.address) " +
            "FROM Supplier tbl")
    List<SupplierLite> findAllLite(Sort sort);
}
