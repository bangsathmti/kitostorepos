package com.kito.pos.repository.master;

import com.kito.pos.domain.master.Category;
import com.kito.pos.dto.master.CategoryLite;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 *
 * @author abdulhakam
 */
public interface CategoryRepository extends CrudRepository<Category, Long> {

    boolean existsByName(String name);

    boolean existsByIdNotAndName(Long id, String name);

    @Query("SELECT NEW com.kito.pos.dto.master.CategoryLite(tbl.id, tbl.name, tbl.description) " +
            "FROM Category tbl")
    List<CategoryLite> findAllLite(Sort sort);
}
