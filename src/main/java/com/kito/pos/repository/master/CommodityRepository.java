package com.kito.pos.repository.master;

import com.kito.pos.domain.master.Commodity;
import com.kito.pos.dto.CommonCombo;
import com.kito.pos.dto.master.CommodityLite;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

/**
 *
 * @author abdulhakam
 */
public interface CommodityRepository extends JpaRepository<Commodity, Long> {

    boolean existsByName(String name);

    boolean existsByIdNotAndName(Long id, String name);

    @Query("SELECT NEW com.kito.pos.dto.master.CommodityLite(" +
            "tbl.id, " +
            "tbl.name, " +
            "tbl.description, " +
            "tbl.category.id, " +
            "tbl.category.name, " +
            "tbl.unitMeasurement.id, " +
            "tbl.unitMeasurement.name, " +
            "tbl.salesMeasurement.id, " +
            "tbl.salesMeasurement.name, tbl.salesPrice) " +
            "FROM Commodity tbl")
    List<CommodityLite> findAllLite();

    @Query("SELECT NEW com.kito.pos.dto.master.CommodityLite(" +
            "tbl.id, " +
            "tbl.name, " +
            "tbl.description, " +
            "tbl.category.id, " +
            "tbl.category.name, " +
            "tbl.unitMeasurement.id, " +
            "tbl.unitMeasurement.name, " +
            "tbl.salesMeasurement.id, " +
            "tbl.salesMeasurement.name, tbl.salesPrice) " +
            "FROM Commodity tbl " +
            "WHERE tbl.id = ?1")
    Optional<CommodityLite> findLiteById(Long id);

    @Query("SELECT NEW com.kito.pos.dto.master.CommodityLite(" +
            "tbl.id, " +
            "tbl.name, " +
            "tbl.description, " +
            "tbl.category.id, " +
            "tbl.category.name, " +
            "tbl.unitMeasurement.id, " +
            "tbl.unitMeasurement.name, " +
            "tbl.salesMeasurement.id, " +
            "tbl.salesMeasurement.name, tbl.salesPrice) " +
            "FROM Commodity tbl")
    Page<CommodityLite> findLitePage(Pageable pageable);

    @Query("SELECT NEW com.kito.pos.dto.CommonCombo(tbl.id, tbl.name) " +
            "FROM Commodity tbl")
    List<CommonCombo> findCombo(Sort sort);

    @Query("SELECT NEW com.kito.pos.dto.CommonCombo(tbl.id, tbl.name) " +
            "FROM Commodity tbl " +
            "WHERE tbl.id NOT IN ?1")
    List<CommonCombo> findComboByIdNotIn(List<Long> ids, Sort sort);
}
