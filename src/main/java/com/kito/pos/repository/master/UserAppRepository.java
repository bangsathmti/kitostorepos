package com.kito.pos.repository.master;

import com.kito.pos.domain.master.UserApp;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author abdulhakam
 */
public interface UserAppRepository extends JpaRepository<UserApp, String> {

    boolean existsByName(String name);

    boolean existsByPersonId(long personId);

    boolean existsByUsernameNotAndName(String username, String name);

    boolean existsByUsernameNotAndPersonId(String username, long personId);
}
