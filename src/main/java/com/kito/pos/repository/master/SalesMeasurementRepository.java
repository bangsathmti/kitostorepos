package com.kito.pos.repository.master;

import com.kito.pos.domain.master.SalesMeasurement;
import com.kito.pos.dto.master.SalesMeasurementLite;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 *
 * @author abdulhakam
 */
public interface SalesMeasurementRepository extends JpaRepository<SalesMeasurement, Long> {

    boolean existsByName(String name);

    boolean existsByIdNotAndName(Long id, String name);

    @Query("SELECT NEW com.kito.pos.dto.master.SalesMeasurementLite(tbl.id, tbl.name, tbl.shortName, tbl.description) " +
            "FROM SalesMeasurement tbl")
    List<SalesMeasurementLite> findAllLite(Sort sort);
}
