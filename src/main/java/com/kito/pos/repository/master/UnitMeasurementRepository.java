package com.kito.pos.repository.master;

import com.kito.pos.domain.master.UnitMeasurement;
import com.kito.pos.dto.master.UnitMeasurementLite;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 *
 * @author abdulhakam
 */
public interface UnitMeasurementRepository extends JpaRepository<UnitMeasurement, Long> {
    boolean existsByName(String name);

    boolean existsByIdNotAndName(Long id, String name);

    @Query("SELECT NEW com.kito.pos.dto.master.UnitMeasurementLite(tbl.id, tbl.name, tbl.description) " +
            "FROM UnitMeasurement tbl")
    List<UnitMeasurementLite> findAllLite(Sort sort);
}
