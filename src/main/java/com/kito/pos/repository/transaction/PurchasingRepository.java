package com.kito.pos.repository.transaction;

import com.kito.pos.domain.transaction.Purchasing;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

/**
 *
 * @author abdulhakam
 */
public interface PurchasingRepository extends JpaRepository<Purchasing, String> {
    List<Purchasing> findByPurchaseDateBetween(Date startDate, Date endDate);
}
