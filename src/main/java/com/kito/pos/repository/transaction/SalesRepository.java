package com.kito.pos.repository.transaction;

import com.kito.pos.domain.transaction.Sales;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author abdulhakam
 */
public interface SalesRepository extends JpaRepository<Sales, String> {

    List<Sales> findByCreatedDateBetween(Date startDate, Date endDate);

    @Query("SELECT MAX(SUBSTRING(a.salesNumber, 12, 4)) FROM Sales a "
            + "WHERE SUBSTRING (a.salesNumber, 1, 11) = ?1")
    Optional<String> getLastStationTransactionIndex(String prefix);
}
