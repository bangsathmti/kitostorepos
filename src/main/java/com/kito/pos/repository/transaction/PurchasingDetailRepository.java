package com.kito.pos.repository.transaction;

import com.kito.pos.domain.transaction.PurchasingDetail;
import com.kito.pos.domain.transaction.PurchasingDetailPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

/**
 *
 * @author abdulhakam
 */
public interface PurchasingDetailRepository extends JpaRepository<PurchasingDetail, PurchasingDetailPK> {

    @Query("select tbl.id.commodityId "
            + "from PurchasingDetail tbl where "
            + "tbl.id.purchaseNumber = ?1")
    List<Long> findCommodityIdListByPurchaseNumber(String purchaseNumber);

    @Query("select SUM(tbl.amount) "
            + "from PurchasingDetail tbl where "
            + "tbl.id.commodityId = ?1")
    Optional<Long> findTotalAmountByCommodityId(Long commodityID);
}
