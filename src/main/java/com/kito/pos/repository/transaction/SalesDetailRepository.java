package com.kito.pos.repository.transaction;

import com.kito.pos.domain.transaction.SalesDetail;
import com.kito.pos.domain.transaction.SalesDetailPK;
import com.kito.pos.dto.transaction.CommoditySaleSummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author abdulhakam
 */
public interface SalesDetailRepository extends JpaRepository<SalesDetail, SalesDetailPK> {

    @Query("select tbl.id.commodityId "
            + "from SalesDetail tbl where "
            + "tbl.id.salesNumber = ?1")
    List<Long> findCommodityIdListBySalesNumber(String salesNumber);

    @Query("select SUM(tbl.amount) "
            + "from SalesDetail tbl where "
            + "tbl.id.commodityId = ?1")
    Optional<Long> findTotalAmountByCommodityId(Long commodityID);

    @Query("select distinct NEW com.kito.pos.dto.transaction.CommoditySaleSummary(tbl.commodity.name, " +
            "tbl.commodity.salesMeasurement.name, SUM(tbl.amount)) " +
            "from SalesDetail tbl " +
            "where tbl.sales.createdDate >= ?1 " +
            "and tbl.sales.createdDate <= ?2 " +
            "group by tbl.commodity.name, tbl.commodity.salesMeasurement.name")
    List<CommoditySaleSummary> findCommoditySaleSummary(Date startDate, Date endDate);
}
