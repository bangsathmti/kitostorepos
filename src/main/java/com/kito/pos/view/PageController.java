package com.kito.pos.view;

import com.kito.pos.domain.master.UserApp;
import com.kito.pos.service.master.UserAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 7, 2022, at 9:01:49 PM
 */
@Controller
@RequestMapping
public class PageController {

    @Autowired
    private UserAppService service;

    @GetMapping("/dashboard")
    public String dashboard(Model model) {
        UserApp userApp = service.findCurrentUser();
        model.addAttribute("usernamename", userApp.getName());
        model.addAttribute("userRole", userApp.getUserRole().toString());
        return "pages/dashboard";
    }

    @GetMapping("/login")
    public String login(HttpServletRequest request, Model model) {
        HttpSession session = request.getSession(false);
        String errorMessage = null;
        if (session != null) {
            AuthenticationException ex = (AuthenticationException) session
                    .getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
            if (ex != null) {
                errorMessage = ex.getMessage();
            }
        }
        model.addAttribute("errorMessage", errorMessage);
        return "pages/login";
    }

    @GetMapping("/master/unitMeasurement")
    public String unitMeasurement(Model model) {
        UserApp userApp = service.findCurrentUser();
        model.addAttribute("usernamename", userApp.getName());
        model.addAttribute("userRole", userApp.getUserRole().toString());
        return "pages/master/unitMeasurementOld";
    }

    @GetMapping("/master/supplier")
    public String supplier(Model model) {
        UserApp userApp = service.findCurrentUser();
        model.addAttribute("usernamename", userApp.getName());
        model.addAttribute("userRole", userApp.getUserRole().toString());
        return "pages/master/supplierOld";
    }

    @GetMapping("/master/category")
    public String category(Model model) {
        UserApp userApp = service.findCurrentUser();
        model.addAttribute("usernamename", userApp.getName());
        model.addAttribute("userRole", userApp.getUserRole().toString());
        return "pages/master/categoryOld";
    }

    @GetMapping("/master/commodity")
    public String commodity(Model model) {
        UserApp userApp = service.findCurrentUser();
        model.addAttribute("usernamename", userApp.getName());
        model.addAttribute("userRole", userApp.getUserRole().toString());
        return "pages/master/commodityOld";
    }

    @GetMapping("/master/salesMeasurement")
    public String salesMeasurement(Model model) {
        UserApp userApp = service.findCurrentUser();
        model.addAttribute("usernamename", userApp.getName());
        model.addAttribute("userRole", userApp.getUserRole().toString());
        return "pages/master/salesMeasurementOld";
    }

    @GetMapping("/transaction/purchasing")
    public String purchasing(Model model) {
        UserApp userApp = service.findCurrentUser();
        model.addAttribute("usernamename", userApp.getName());
        model.addAttribute("userRole", userApp.getUserRole().toString());
        return "pages/transaction/purchasingOld";
    }

    @GetMapping("/transaction/purchasing/Detail")
    public String purchasingDetail(@RequestParam String purchaseNumber, Model model) {
        UserApp userApp = service.findCurrentUser();
        model.addAttribute("usernamename", userApp.getName());
        model.addAttribute("userRole", userApp.getUserRole().toString());
        model.addAttribute("purchaseNumberId", purchaseNumber);
        return "pages/transaction/purchasingDetailOld";
    }

    @GetMapping("/transaction/cashier")
    public String cashier(Model model) {
        UserApp userApp = service.findCurrentUser();
        model.addAttribute("usernamename", userApp.getName());
        model.addAttribute("userRole", userApp.getUserRole().toString());
        return "pages/transaction/cashierOld";
    }

    @GetMapping("/transaction/sales")
    public String sales(Model model) {
        UserApp userApp = service.findCurrentUser();
        model.addAttribute("usernamename", userApp.getName());
        model.addAttribute("userRole", userApp.getUserRole().toString());
        return "pages/transaction/salesOld";
    }

    @GetMapping("/transaction/sales/detail")
    public String SalesDetail(@RequestParam String salesNumber, Model model) {
        UserApp userApp = service.findCurrentUser();
        model.addAttribute("usernamename", userApp.getName());
        model.addAttribute("userRole", userApp.getUserRole().toString());
        model.addAttribute("salesNumberId", salesNumber);
        return "pages/transaction/salesDetailOld";
    }

    @GetMapping("/transaction/cashier/struct")
    public String struct(@RequestParam String salesNumber, Model model) {
        model.addAttribute("salesNumber", salesNumber);
        return "pages/transaction/struct";
    }

    @GetMapping("/transaction/cashier/printStruct")
    public String structPrint(@RequestParam String salesNumber, Model model) {
        model.addAttribute("salesNumber", salesNumber);
        return "pages/transaction/printStruct";
    }
}
