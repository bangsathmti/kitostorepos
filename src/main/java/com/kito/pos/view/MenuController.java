package com.kito.pos.view;

import com.kito.pos.domain.UserRole;
import com.kito.pos.domain.master.UserApp;
import com.kito.pos.service.master.UserAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MenuController {

    private final UserAppService service;

    @Autowired
    public MenuController(UserAppService service) {
        this.service = service;
    }

    @GetMapping("/")
    public String start(Model model) {
        UserApp userApp = service.findCurrentUser();
        model.addAttribute("userUsername", userApp.getName());
        model.addAttribute("userRole", userApp.getUserRole().toString());
        return "main";
    }

    @GetMapping("/start")
    public String index(Model model) {
        UserApp userApp = service.findCurrentUser();
//        model.addAttribute("usernamename", userApp.getName());
        model.addAttribute("userUsername", userApp.getName());
        model.addAttribute("userRole", userApp.getUserRole().toString());
        if (userApp.getUserRole().equals(UserRole.KASIR)) {
            return "pages/transaction/cashierOld";
        }
//        return "pages/dashboard";
        return "main";
    }

    @GetMapping("/page")
    public String getPage(@RequestParam String page) {
        return "pages/" + page;
    }

    @GetMapping("/fragment")
    public String getFragment(@RequestParam String page) {return "fragment/" + page;}
}