/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kito.pos.view.transaction;

import com.kito.pos.domain.transaction.Purchasing;
import com.kito.pos.dto.CommonCombo;
import com.kito.pos.dto.transaction.PurchasingDetailLite;
import com.kito.pos.dto.transaction.PurchasingLite;
import com.kito.pos.service.transaction.PurchasingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 9, 2022, at 10:19:17 PM
 */
@RestController
@RequestMapping("/purchasing")
public class PurchasingController {

    @Autowired
    private PurchasingService service;

    @GetMapping
    public Page<Purchasing> findPage(@RequestParam int page) {
        return service.findPage(page);
    }

    @GetMapping("/id")
    public Purchasing findById(@RequestParam String purchaseNumber) {
        return service.findById(purchaseNumber);
    }

    @PostMapping
    public void save(@RequestBody PurchasingLite purchasing) {
        service.save(purchasing);
    }

    @PutMapping
    public void Update(@RequestBody PurchasingLite purchasing) {
        service.Update(purchasing);
    }

    @PostMapping("/detail")
    public void addDetail(@RequestBody PurchasingDetailLite purchasingDetail) {
        service.addDetail(purchasingDetail);
    }

    @PutMapping("/detail")
    public void UpdateDetail(@RequestBody PurchasingDetailLite purchasingDetail) {
        service.UpdateDetail(purchasingDetail);
    }

    @GetMapping("/availableCommodity")
    public List<CommonCombo> findAvailableCommodity(@RequestParam String purchaseNumber) {
        return service.findAvailableCommodity(purchaseNumber);
    }
}
