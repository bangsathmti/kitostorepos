/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kito.pos.view.transaction;

import com.kito.pos.domain.transaction.Sales;
import com.kito.pos.dto.CommonCombo;
import com.kito.pos.dto.transaction.CashierCache;
import com.kito.pos.dto.transaction.EditCache;
import com.kito.pos.dto.transaction.SalesDetailLite;
import com.kito.pos.dto.transaction.SalesLite;
import com.kito.pos.service.transaction.SalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 21, 2022, at 9:12:38 AM
 */
@RestController
@RequestMapping("/sales")
public class SalesController {

    @Autowired
    private SalesService service;

    @GetMapping("/cashier")
    public Set<CashierCache> findCacheByUsername() {
        return service.findCacheByUsername();
    }

    @GetMapping("/cashier/grandTotal")
    public ResponseEntity findGrandTotalCache() {
        return service.findGrandTotalCache();
    }

    @PutMapping("/cashier")
    public void editCache(@RequestBody EditCache editCache) {
        service.editCache(editCache);
    }

    @PostMapping("/cashier")
    public void addToCache(@RequestParam String barcode) {
        service.addToCache(barcode);
    }

    @DeleteMapping("/cashier/commodity")
    public void removeSalesDetailLite(@RequestParam Long commodityId) {
        service.removeSalesDetailLite(commodityId);
    }

    @GetMapping("/salesNumber")
    public Sales findBySalesNumber(@RequestParam String salesNumber) {
        return service.findBySalesNumber(salesNumber);
    }

    @PostMapping("/add")
    public ResponseEntity save(@RequestBody SalesLite salesLite) {
        return service.save(salesLite);
    }

    @GetMapping
    public Page<Sales> findPage(@RequestParam int page) {
        return service.findPage(page);
    }

    @GetMapping("/id")
    public Sales findById(@RequestParam String salesNumber) {
        return service.findBySalesNumber(salesNumber);
    }

    @PostMapping
    public ResponseEntity add(@RequestBody SalesLite sales) {
        return service.add(sales);
    }

    @PutMapping
    public void Update(@RequestBody SalesLite sales) {
        service.Update(sales);
    }

    @PostMapping("/detail")
    public void addDetail(@RequestBody SalesDetailLite salesDetail) {
        System.out.println("salesDetail controller = " + salesDetail);
        service.addDetail(salesDetail);
    }

    @PutMapping("/detail")
    public void UpdateDetail(@RequestBody SalesDetailLite salesDetail) {
        service.UpdateDetail(salesDetail);
    }

    @GetMapping("/availableCommodity")
    public List<CommonCombo> findAvailableCommodity(@RequestParam String salesNumber) {
        return service.findAvailableCommodity(salesNumber);
    }
}
