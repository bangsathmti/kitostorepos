package com.kito.pos.view.master;

import com.kito.pos.dto.master.CategoryLite;
import com.kito.pos.service.master.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 10, 2022, at 10:18:04 AM
 */
@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService service;

    @GetMapping
    public Collection<CategoryLite> findAll() {
        return service.findAll();
    }

    @PostMapping
    public void save(@RequestBody CategoryLite category) {
        service.save(category);
    }

    @PutMapping
    public void update(@RequestBody CategoryLite category) {
        service.update(category);
    }

    @DeleteMapping
    public void update(@RequestParam Long id) {
        service.delete(id);
    }
}
