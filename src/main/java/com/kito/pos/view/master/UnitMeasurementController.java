package com.kito.pos.view.master;

import com.kito.pos.dto.master.UnitMeasurementLite;
import com.kito.pos.service.master.UnitMeasurementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 11, 2022, at 11:06:17 AM
 */
@RestController
@RequestMapping("/unitMeasurement")
public class UnitMeasurementController {

    private final UnitMeasurementService service;

    @Autowired
    public UnitMeasurementController(UnitMeasurementService service) {
        this.service = service;
    }

    @GetMapping
    public List<UnitMeasurementLite> findAll() {
        return service.findAll();
    }

    @PostMapping
    public void save(@RequestBody UnitMeasurementLite unitMeasurement) {
        service.save(unitMeasurement);
    }

    @PutMapping
    public void update(@RequestBody UnitMeasurementLite unitMeasurement) {
        service.update(unitMeasurement);
    }

    @DeleteMapping
    public void update(@RequestParam Long id) {
        service.delete(id);
    }

}
