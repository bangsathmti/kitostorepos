package com.kito.pos.view.master;

import com.kito.pos.dto.master.CommodityLite;
import com.kito.pos.service.master.CommodityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.awt.image.BufferedImage;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 9, 2022, at 5:57:49 PM
 */
@RestController
@RequestMapping("/commodity")
public class CommodityController {

    private final CommodityService service;

    @Autowired
    public CommodityController(CommodityService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity findPage(@RequestParam int page) {return service.findPage(page);}

    @GetMapping("/defaultBarcode")
    public ResponseEntity findDefaultBarcode() {return service.generateDefaultBarcode();}

    @PostMapping
    public void save(@RequestBody CommodityLite commodity) {
        service.save(commodity);
    }

    @PutMapping
    public void update(@RequestBody CommodityLite commodity) {
        service.update(commodity);
    }

    @DeleteMapping
    public void delete(@RequestParam Long id) {
        service.delete(id);
    }

    @GetMapping(value = "/barcode", produces = MediaType.IMAGE_PNG_VALUE)
    public ResponseEntity<BufferedImage> createBarcodeImage(@RequestParam Long commodityId){
        return service.createBarcodeImage(commodityId);
    }
}
