package com.kito.pos.view.master;

import com.kito.pos.dto.master.SupplierLite;
import com.kito.pos.service.master.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 9, 2022, at 1:54:40 PM
 */
@RestController
@RequestMapping("/supplier")
public class SupplierController {

    @Autowired
    private SupplierService service;

    @GetMapping
    public List<SupplierLite> findAll() {
        return service.findAll();
    }

    @PostMapping
    public void save(@RequestBody SupplierLite supplier) {
        service.save(supplier);
    }

    @PutMapping
    public void update(@RequestBody SupplierLite supplier) {
        service.update(supplier);
    }

    @DeleteMapping
    public void update(@RequestParam Long id) {
        service.delete(id);
    }
}
