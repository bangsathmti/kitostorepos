package com.kito.pos.view.master;

import com.kito.pos.domain.master.UserApp;
import com.kito.pos.dto.master.ChangePassword;
import com.kito.pos.service.master.UserAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 10, 2022, at 10:03:42 PM
 */
@Controller
public class UserAppController {

    private final UserAppService service;

    @Autowired
    public UserAppController(UserAppService service) {
        this.service = service;
    }

    @GetMapping("/master/userApp")
    public String userApp(Model model) {
        UserApp userApp = service.findCurrentUser();
        model.addAttribute("usernamename", userApp.getName());
        model.addAttribute("userRole", userApp.getUserRole().toString());
        return "pages/master/userAppOld";
    }

    @ResponseBody
    @GetMapping("/userApp")
    public Collection<UserApp> findAllExceptCurrentUser() {
        return service.findAllExceptCurrentUser();
    }

    @ResponseBody
    @PostMapping("/userApp")
    public void save(@RequestBody UserApp user) {
        service.save(user);
    }

    @ResponseBody
    @PutMapping("/userApp")
    public void update(@RequestBody UserApp user) {
        service.update(user);
    }

    @ResponseBody
    @DeleteMapping("/userApp")
    public void delete(@RequestParam String username) {
        service.delete(username);
    }

    @ResponseBody
    @PutMapping("/userApp/changePassword")
    public void changePassword(@RequestBody ChangePassword changePassword) {
        service.changePassword(changePassword);
    }
}
