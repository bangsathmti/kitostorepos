/*
 * Copyright 2002-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kito.pos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.http.converter.BufferedImageHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.awt.image.BufferedImage;

/**
 * @author <a href="https://web.facebook.com/jmd.juraganempangdollar/">Abdul
 * Hakam</a>.<br>
 * Created on Mar 7, 2022, at 6:06:07 AM
 */
@SpringBootApplication
public class Main {

    /**
     * Application entry point
     * @param args String array of argumentation
     * @throws Exception Throwing to exception if error found
     *
     * @see Exception
     * @see SpringApplication
     */
    public static void main(String[] args) throws Exception {
        SpringApplication.run(Main.class, args);
    }


    /**
     * Image converter. This bean used for generating an PNG image of barcode
     *
     * @return New object of {@link BufferedImageHttpMessageConverter}
     *
     * @see HttpMessageConverter
     * @see BufferedImage
     * @see BufferedImageHttpMessageConverter
     */
    @Bean
    public HttpMessageConverter<BufferedImage> createImageHttpMessageConverter() {
        return new BufferedImageHttpMessageConverter();
    }

    /**
     * Password encoder bean
     * @return Delegating password encoder
     *
     * @see PasswordEncoder
     * @see PasswordEncoderFactories#createDelegatingPasswordEncoder()
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    /**
     * Message source bean. This bean containing message source for custom message.
     *
     * @return {@link MessageSource} object.
     *
     * @see MessageSource
     * @see ResourceBundleMessageSource
     * @see ResourceBundleMessageSource#setBasename(String)
     */
    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("messages_id");
        return messageSource;
    }

}
