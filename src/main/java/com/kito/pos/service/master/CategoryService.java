package com.kito.pos.service.master;

import com.kito.pos.dto.master.CategoryLite;
import com.kito.pos.repository.master.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 10, 2022, at 10:07:16 AM
 */
@Service
@Transactional
public class CategoryService {

    private final CategoryRepository repository;

    @Autowired
    public CategoryService(CategoryRepository repository) {
        this.repository = repository;
    }

    public List<CategoryLite> findAll() {
        return repository.findAllLite(Sort.by(Sort.Direction.ASC, "name"));
    }

    public void save(CategoryLite category) {
        if (repository.existsByName(category.getName())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The category name(" + category.getName() + ") "
                    + "already exist");
        }
        category.setId(null);
        try {
            repository.save(category.convert());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
        }
    }

    public void update(CategoryLite category) {
        if (repository.existsByIdNotAndName(category.getId(), category.getName())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The category name(" + category.getName() + ") "
                    + "already exist");
        }
        if (category.getId() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The category id cannot be null");
        }
        if (!repository.existsById(category.getId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The category id(" + category.getId() + ") "
                    + "is not exist");
        }
        try {
            repository.save(category.convert());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
        }
    }

    public void delete(Long id) {
        if (repository.existsById(id)) {
            try {
                repository.deleteById(id);
            } catch (Exception e) {
                throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
            }
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The category id(" + id + ") "
                    + "is not exist");
        }
    }

}
