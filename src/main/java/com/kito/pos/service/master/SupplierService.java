package com.kito.pos.service.master;

import com.kito.pos.dto.master.SupplierLite;
import com.kito.pos.repository.master.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 10, 2022, at 6:20:15 AM
 */
@Service
@Transactional
public class SupplierService {

    private final SupplierRepository repository;

    @Autowired
    public SupplierService(SupplierRepository repository) {
        this.repository = repository;
    }

    public List<SupplierLite> findAll() {return repository.findAllLite(Sort.by(Sort.Direction.ASC, "name"));}

    public void save(SupplierLite supplier) {
        if (supplier.getContact().length() > 15) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Contact value cannot more than 15 character, "
                    + supplier.getContact() + " has " + supplier.getContact().length() + " character");
        }
        if (repository.existsByName(supplier.getName())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The supplier name(" + supplier.getName() + ") "
                    + "already taken");
        }
        supplier.setId(null);
        try {
            repository.save(supplier.convert());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
        }
    }

    public void update(SupplierLite supplier) {
        if (supplier.getContact().length() > 15) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Contact value cannot more than 15 character, "
                    + supplier.getContact() + " has " + supplier.getContact().length() + " character");
        }
        if (repository.existsByIdNotAndName(supplier.getId(), supplier.getName())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The supplier name(" + supplier.getName() + ") "
                    + "already taken by other supplier");
        }
        if (supplier.getId() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The supplier id cannot be null");
        }
        if (!repository.existsById(supplier.getId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The supplier id(" + supplier.getId() + ") "
                    + "is not exist");
        }
        try {
            repository.save(supplier.convert());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
        }
    }

    public void delete(Long id) {
        if (repository.existsById(id)) {
            try {
                repository.deleteById(id);
            } catch (Exception e) {
                throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
            }
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The supplier id(" + id + ") "
                    + "is not exist");
        }
    }
}
