package com.kito.pos.service.master;

import com.kito.pos.config.util.GlobalUtil;
import com.kito.pos.domain.master.Commodity;
import com.kito.pos.domain.master.CommodityLog;
import com.kito.pos.dto.master.CommodityLite;
import com.kito.pos.repository.master.CommodityLogRepository;
import com.kito.pos.repository.master.CommodityRepository;
import com.kito.pos.repository.transaction.PurchasingDetailRepository;
import com.kito.pos.repository.transaction.SalesDetailRepository;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 10, 2022, at 4:15:23 PM
 */
@Service
@Transactional
public class CommodityService {

    private final CommodityRepository repository;
    private final CommodityLogRepository logRepository;
    private final PurchasingDetailRepository purchasingDetailRepository;
    private final SalesDetailRepository salesDetailRepository;

    @Autowired
    public CommodityService(CommodityRepository repository, CommodityLogRepository logRepository, PurchasingDetailRepository purchasingDetailRepository, SalesDetailRepository salesDetailRepository) {
        this.repository = repository;
        this.logRepository = logRepository;
        this.purchasingDetailRepository = purchasingDetailRepository;
        this.salesDetailRepository = salesDetailRepository;
    }

    public ResponseEntity findPage(int page) {
        Pageable pageable = PageRequest.of(page, 10, Sort.Direction.ASC, "description", "name");
        Page<CommodityLite> lites = repository.findLitePage(pageable);
        List<CommodityLite> commodityLites = new ArrayList<>();
        for (CommodityLite commodityLite : lites) {
            Optional<Long> purchaseAmount = purchasingDetailRepository.findTotalAmountByCommodityId(commodityLite.getId());
            Optional<Long> salesAmount = salesDetailRepository.findTotalAmountByCommodityId(commodityLite.getId());
            commodityLite.setStock((purchaseAmount.orElse(0L)) - (salesAmount.orElse(0L)));
            commodityLites.add(commodityLite);
        }
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("content", commodityLites);
        hashMap.put("totalElements", repository.count());
        return ResponseEntity.ok(hashMap);
    }

    public void save(@NotNull CommodityLite commodity) {
        if (repository.existsById(commodity.getId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The commodity "
                    + commodity.getId() + " already exist");
        }
        if (repository.existsByName(commodity.getName())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The commodity "
                    + commodity.getName() + " already exist");
        }
        try {
            repository.save(commodity.convert());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
        }
    }

    public void update(@NotNull CommodityLite commodity) {
        if (repository.existsByIdNotAndName(commodity.getId(), commodity.getName())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The commodity name(" + commodity.getName() + ") "
                    + "is not exist");
        }
        if (commodity.getId() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The commodity id cannot be null");
        }
        Optional<Commodity> optional = repository.findById(commodity.getId());
        if (optional.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The commodity id(" + commodity.getId() + ") "
                    + "is not exist");
        }
        try {
            Commodity c = optional.get();
            logRepository.save(new CommodityLog(c));
            repository.save(commodity.convert());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
        }
    }

    public void delete(Long id) {
        if (repository.existsById(id)) {
            try {
                repository.deleteById(id);
            } catch (Exception e) {
                throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
            }
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The category id(" + id + ") "
                    + "is not exist");
        }
    }

    public ResponseEntity<BufferedImage> createBarcodeImage(Long commodityId) {
        Optional<CommodityLite> optional = repository.findLiteById(commodityId);
        if (optional.isPresent()) {
            try {
                CommodityLite commodity = optional.get();
                return ResponseEntity.ok(commodity.generateBarcode4j());
            } catch (Exception e) {
                throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
            }
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The category id(" + commodityId + ") "
                    + "is not exist");
        }
    }

    public ResponseEntity generateDefaultBarcode(){
        Long defaultBarcode = GlobalUtil.generateDefaultBarcode();
        HashMap<String, Object> hashMap = new HashMap<>();
        if (repository.existsById(defaultBarcode)){
            return generateDefaultBarcode();
        }else{
            hashMap.put("defaultBarcode", defaultBarcode);
            return ResponseEntity.ok(hashMap);
        }
    }
}
