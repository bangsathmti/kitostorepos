package com.kito.pos.service.master;

import com.kito.pos.dto.master.UnitMeasurementLite;
import com.kito.pos.repository.master.UnitMeasurementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 11, 2022, at 11:03:31 AM
 */
@Service
@Transactional
public class UnitMeasurementService {

    private final UnitMeasurementRepository repository;

    @Autowired
    public UnitMeasurementService(UnitMeasurementRepository repository) {
        this.repository = repository;
    }

    public List<UnitMeasurementLite> findAll() {
        return repository.findAllLite(Sort.by(Sort.Direction.ASC, "name"));
    }

    public void save(UnitMeasurementLite unitMeasurement) {
        if (repository.existsByName(unitMeasurement.getName())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The unit measurement name("
                    + unitMeasurement.getName() + ") already exist");
        }
        unitMeasurement.setId(null);
        try {
            repository.save(unitMeasurement.convert());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
        }
    }

    public void update(UnitMeasurementLite unitMeasurement) {
        if (repository.existsByIdNotAndName(unitMeasurement.getId(), unitMeasurement.getName())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The unit measurement name("
                    + unitMeasurement.getName() + ") already taken by other supplier");
        }
        if (unitMeasurement.getId() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The unit measurement id cannot be null");
        }
        if (!repository.existsById(unitMeasurement.getId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The unit measurement id("
                    + unitMeasurement.getId() + ") is not exist");
        }
        try {
            repository.save(unitMeasurement.convert());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
        }
    }

    public void delete(Long id) {
        if (repository.existsById(id)) {
            try {
                repository.deleteById(id);
            } catch (Exception e) {
                throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
            }
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The unit measurement id(" + id + ") "
                    + "is not exist");
        }
    }
}
