package com.kito.pos.service.master;

import com.kito.pos.domain.master.UserApp;
import com.kito.pos.dto.master.ChangePassword;
import com.kito.pos.repository.master.UserAppRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;
import java.util.Optional;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 10, 2022, at 9:57:38 PM
 */
@Service
@Transactional
public class UserAppService implements UserDetailsService {

    private final UserAppRepository repository;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserAppService(UserAppRepository repository, PasswordEncoder passwordEncoder) {
        this.repository = repository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<UserApp> optional = repository.findById(username);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            throw new UsernameNotFoundException("Username " + username + " not found");
        }
    }

    @PostFilter("filterObject.username != principal.username")
    public Collection<UserApp> findAllExceptCurrentUser() {
        return repository.findAll(Sort.by(Sort.Direction.ASC,"name"));
    }

    public void save(UserApp user) {
        if (repository.existsById(user.getUsername())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The username(" + user.getUsername() + ") "
                    + "already taken");
        }
        if (repository.existsByName(user.getName())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The name(" + user.getName() + ") "
                    + "already taken");
        }
        if (repository.existsByPersonId(user.getPersonId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The person ID(" + user.getPersonId() + ") "
                    + "already taken");
        }
        user.setPassword(passwordEncoder.encode("password"));
        repository.save(user);
    }

    public void update(UserApp user) {
        if (!repository.existsById(user.getUsername())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The username " + user.getUsername()
                    + " is not exist");
        }
        if (repository.existsByUsernameNotAndName(user.getUsername(), user.getName())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The name(" + user.getName() + ") "
                    + "already taken");
        }
        if (repository.existsByUsernameNotAndPersonId(user.getUsername(), user.getPersonId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The person ID(" + user.getPersonId() + ") "
                    + "already taken");
        }
        user.setPassword(passwordEncoder.encode("password"));
        repository.save(user);
    }

    public void delete(String username) {
        if (repository.existsById(username)) {
            repository.deleteById(username);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The username" + username + " is not exist");
        }
    }

    public UserApp findCurrentUser() {
        return (UserApp) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    public void changePassword(ChangePassword changePassword) {
        UserApp userApp = findCurrentUser();
        if (!passwordEncoder.matches(changePassword.getOldPassword(), userApp.getPassword())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The old password was wrong");
        }
        if (!changePassword.getNewPassword().equals(changePassword.getNewPasswordConfirmation())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        userApp.setPassword(passwordEncoder.encode(changePassword.getNewPassword()));
        try {
            repository.save(userApp);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
        }
    }
}