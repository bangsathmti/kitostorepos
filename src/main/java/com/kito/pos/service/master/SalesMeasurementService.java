package com.kito.pos.service.master;

import com.kito.pos.dto.master.SalesMeasurementLite;
import com.kito.pos.repository.master.SalesMeasurementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 10, 2022, at 5:19:12 PM
 */
@Service
@Transactional
public class SalesMeasurementService {

    private final SalesMeasurementRepository repository;

    @Autowired
    public SalesMeasurementService(SalesMeasurementRepository repository) {
        this.repository = repository;
    }

    public List<SalesMeasurementLite> findAll() {
        return repository.findAllLite(Sort.by(Sort.Direction.ASC, "name"));
    }

    public void save(SalesMeasurementLite salesMeasurement) {
        if (salesMeasurement.getShortName().length() > 7) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Short name value cannot more than 7 character, "
                    + salesMeasurement.getShortName() + " has " + salesMeasurement.getShortName().length() + " character");
        }
        if (repository.existsByName(salesMeasurement.getName())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The sales measurement name("
                    + salesMeasurement.getName() + ") already exist");
        }
        salesMeasurement.setId(null);
        try {
            repository.save(salesMeasurement.convert());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
        }
    }

    public void update(SalesMeasurementLite salesMeasurement) {
        if (salesMeasurement.getShortName().length() > 7) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Short name value cannot more than 7 character, "
                    + salesMeasurement.getShortName() + " has " + salesMeasurement.getShortName().length() + " character");
        }
        if (repository.existsByIdNotAndName(salesMeasurement.getId(), salesMeasurement.getName())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The sales measurement name("
                    + salesMeasurement.getName() + ") already exists");
        }
        if (salesMeasurement.getId() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The sales measurement id cannot be null");
        }
        if (!repository.existsById(salesMeasurement.getId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The sales measurement id("
                    + salesMeasurement.getId() + ") is not exist");
        }
        try {
            repository.save(salesMeasurement.convert());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
        }
    }

    public void delete(Long id) {
        if (repository.existsById(id)) {
            try {
                repository.deleteById(id);
            } catch (Exception e) {
                throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
            }
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The sales measurement id(" + id + ") "
                    + "is not exist");
        }
    }

}
