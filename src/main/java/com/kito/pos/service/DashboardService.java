package com.kito.pos.service;

import com.kito.pos.config.util.GlobalUtil;
import com.kito.pos.domain.IndonesianDow;
import com.kito.pos.domain.IndonesianMonth;
import com.kito.pos.domain.transaction.Purchasing;
import com.kito.pos.domain.transaction.PurchasingDetail;
import com.kito.pos.domain.transaction.Sales;
import com.kito.pos.domain.transaction.SalesDetail;
import com.kito.pos.dto.master.CommodityLite;
import com.kito.pos.dto.transaction.CommoditySaleSummary;
import com.kito.pos.repository.master.CommodityRepository;
import com.kito.pos.repository.master.SupplierRepository;
import com.kito.pos.repository.transaction.PurchasingDetailRepository;
import com.kito.pos.repository.transaction.PurchasingRepository;
import com.kito.pos.repository.transaction.SalesDetailRepository;
import com.kito.pos.repository.transaction.SalesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 11, 2022, at 2:55:42 AM
 */
@Service
@Transactional
public class DashboardService {

    private final SupplierRepository supplierRepository;
    private final PurchasingRepository purchasingRepository;
    private final PurchasingDetailRepository purchasingDetailRepository;
    private final SalesRepository salesRepository;
    private final SalesDetailRepository salesDetailRepository;
    private final CommodityRepository commodityRepository;

    @Autowired
    public DashboardService(SupplierRepository supplierRepository, PurchasingRepository purchasingRepository, PurchasingDetailRepository purchasingDetailRepository, SalesRepository salesRepository, SalesDetailRepository salesDetailRepository, CommodityRepository commodityRepository) {
        this.supplierRepository = supplierRepository;
        this.purchasingRepository = purchasingRepository;
        this.purchasingDetailRepository = purchasingDetailRepository;
        this.salesRepository = salesRepository;
        this.salesDetailRepository = salesDetailRepository;
        this.commodityRepository = commodityRepository;
    }

    public HashMap<String, Object> findDashboardData() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("supplierCount", supplierRepository.count());
        Calendar startDay = Calendar.getInstance();
        startDay.set(Calendar.HOUR_OF_DAY, 0);
        startDay.set(Calendar.MINUTE, 0);
        startDay.set(Calendar.SECOND, 0);
        startDay.set(Calendar.MILLISECOND, 0);
        Calendar endDay = Calendar.getInstance();
        endDay.set(Calendar.HOUR_OF_DAY, 23);
        endDay.set(Calendar.MINUTE, 59);
        endDay.set(Calendar.SECOND, 59);
        endDay.set(Calendar.MILLISECOND, 999);
        double salesGrandTotal = 0;
        List<Sales> salesList = salesRepository.findByCreatedDateBetween(startDay.getTime(), endDay.getTime());
        if (!salesList.isEmpty()) {
            for (Sales sales : salesList) {
                salesGrandTotal -= sales.getPersonalDiscount().doubleValue();
                if (!sales.getSalesDetails().isEmpty()) {
                    for (SalesDetail salesDetail : sales.getSalesDetails()) {
                        salesGrandTotal += (salesDetail.getAmount() * salesDetail.getUnitPrice().doubleValue());
                    }
                }
            }
        }
        hashMap.put("thisDaySalesGrandTotal", GlobalUtil.convertToCurrencyFormat(salesGrandTotal));
        List<CommodityLite> commodityList = commodityRepository.findAllLite();
        List<CommodityLite> fiveMostStock = new ArrayList<>();
        for (CommodityLite commodityLite : commodityList) {
            Optional<Long> purchaseAmount = purchasingDetailRepository.findTotalAmountByCommodityId(commodityLite.getId());
            Optional<Long> salesAmount = salesDetailRepository.findTotalAmountByCommodityId(commodityLite.getId());
            commodityLite.setStock((purchaseAmount.orElse(0L)) - (salesAmount.orElse(0L)));
            fiveMostStock.add(commodityLite);
        }
        Collections.sort(fiveMostStock);
        if (fiveMostStock.size() < 5) {
            hashMap.put("fiveMostStock", fiveMostStock.subList(0, fiveMostStock.size()));
        } else {
            hashMap.put("fiveMostStock", fiveMostStock.subList(0, 5));
        }
        double totalStockInCurrency = 0;
        for (CommodityLite commodity : fiveMostStock) {
            totalStockInCurrency += (commodity.getStock() * commodity.getSalesPrice().doubleValue());
        }
        hashMap.put("totalStockInCurrency", GlobalUtil.convertToCurrencyFormat(totalStockInCurrency));
        startDay.set(Calendar.DATE, 1);
        endDay.set(Calendar.DATE, endDay.getActualMaximum(Calendar.DATE));
        List<CommoditySaleSummary> commoditySaleSummaries
                = new ArrayList<>(salesDetailRepository.findCommoditySaleSummary(startDay.getTime(), endDay.getTime()));
        Collections.sort(commoditySaleSummaries);
        if (commoditySaleSummaries.size() < 5) {
            hashMap.put("fiveMostSale", commoditySaleSummaries.subList(0, commoditySaleSummaries.size()));
        } else {
            hashMap.put("fiveMostSale", commoditySaleSummaries.subList(0, 5));
        }
        double thisMonthSalesGrandTotal = 0;
        List<Sales> salesListThisMonth = salesRepository.findByCreatedDateBetween(startDay.getTime(), endDay.getTime());
        if (!salesListThisMonth.isEmpty()) {
            for (Sales sales : salesListThisMonth) {
                thisMonthSalesGrandTotal -= sales.getPersonalDiscount().doubleValue();
                if (!sales.getSalesDetails().isEmpty()) {
                    for (SalesDetail salesDetail : sales.getSalesDetails()) {
                        thisMonthSalesGrandTotal += (salesDetail.getAmount() * salesDetail.getUnitPrice().doubleValue());
                    }
                }
            }
        }
        hashMap.put("thisMonthSalesGrandTotal", GlobalUtil.convertToCurrencyFormat(thisMonthSalesGrandTotal));
        double purchaseGrandTotal = 0;
        List<Purchasing> purchasingList = purchasingRepository.findByPurchaseDateBetween(startDay.getTime(), endDay.getTime());
        if (!purchasingList.isEmpty()) {
            for (Purchasing purchasing : purchasingList) {
                if (!purchasing.getPurchasingDetails().isEmpty()) {
                    for (PurchasingDetail purchasingDetail : purchasing.getPurchasingDetails()) {
                        purchaseGrandTotal += (purchasingDetail.getAmount() * purchasingDetail.getUnitPrice().doubleValue());
                    }
                }
            }
        }
        hashMap.put("thisMonthPurchaseGrandTotal", GlobalUtil.convertToCurrencyFormat(purchaseGrandTotal));
        Calendar calendar = Calendar.getInstance();
        String monthName = GlobalUtil.upperCaseFirstChar(IndonesianMonth.forValue(calendar.get(Calendar.MONTH)).toString()) + " " +
                calendar.get(Calendar.YEAR);
        String todayName = GlobalUtil.upperCaseFirstChar(IndonesianDow.forValue(calendar.get(Calendar.DAY_OF_WEEK)).toString())
                + ", " + calendar.get(Calendar.DAY_OF_MONTH) + " " + monthName;
        hashMap.put("todayName", todayName);
        hashMap.put("monthName", monthName);
        return hashMap;
    }
}
