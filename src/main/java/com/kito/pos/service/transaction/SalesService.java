package com.kito.pos.service.transaction;

import com.kito.pos.config.util.GlobalUtil;
import com.kito.pos.domain.master.Commodity;
import com.kito.pos.domain.master.UserApp;
import com.kito.pos.domain.transaction.Sales;
import com.kito.pos.domain.transaction.SalesDetail;
import com.kito.pos.domain.transaction.SalesDetailPK;
import com.kito.pos.dto.CommonCombo;
import com.kito.pos.dto.transaction.CashierCache;
import com.kito.pos.dto.transaction.EditCache;
import com.kito.pos.dto.transaction.SalesDetailLite;
import com.kito.pos.dto.transaction.SalesLite;
import com.kito.pos.repository.master.CommodityRepository;
import com.kito.pos.repository.transaction.SalesDetailRepository;
import com.kito.pos.repository.transaction.SalesRepository;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 21, 2022, at 8:10:48 AM
 */
@Service
@Transactional
public class SalesService {

    private final HashMap<String, Set<CashierCache>> cache = new HashMap<>();
    private Calendar lastTransactionCalendar;
    private final SalesRepository repository;
    private final SalesDetailRepository detailRepository;
    private final CommodityRepository commodityRepository;

    @Autowired
    public SalesService(SalesRepository repository, SalesDetailRepository detailRepository, CommodityRepository commodityRepository) {
        this.repository = repository;
        this.detailRepository = detailRepository;
        this.commodityRepository = commodityRepository;
    }

    private Commodity findByBarcode(String barcode) {
        if (barcode.length() != 13){
            throw new ResponseStatusException(HttpStatus.PRECONDITION_FAILED, barcode + " bukan barcode yang valid");
        }
        int checkSum = Integer.parseInt(barcode.substring(barcode.length() - 1, barcode.length()));
        String realBarcode = barcode.substring(0, barcode.length() - 1);
        if (checkSum != GlobalUtil.getCheckSumNumber(realBarcode)) {
            throw new ResponseStatusException(HttpStatus.PRECONDITION_FAILED, barcode + " bukan barcode yang valid");
        }
        Optional<Commodity> optional = commodityRepository.findById(Long.parseLong(realBarcode));
        if (optional.isPresent()){
            return optional.get();
        }else{
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Tidak ada produk ber-barcode " + barcode);
        }
    }

    private void checkLastTransaction() {
        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);
        if (lastTransactionCalendar == null || lastTransactionCalendar.before(today)) {
            lastTransactionCalendar = today;
        }
    }

    public void removeSalesDetailLite(Long commodityId) {
        UserApp cashier = (UserApp) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (cache.containsKey(cashier.getUsername())) {
            boolean notFound = true;
            for (CashierCache cashierCache : cache.get(cashier.getUsername())) {
                if (cashierCache.getCommodityId().equals(commodityId)) {
                    notFound = false;
                    cache.get(cashier.getUsername()).remove(cashierCache);
                }
            }
            if (notFound) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            }
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    public Set<CashierCache> findCacheByUsername() {
        UserApp cashier = (UserApp) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (cache.containsKey(cashier.getUsername())) {
            return cache.get(cashier.getUsername());
        } else {
            return new HashSet<>();
        }
    }

    public void editCache(EditCache editCache) {
        UserApp cashier = (UserApp) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Set<CashierCache> details = new HashSet<>();
        boolean notFound = true;
        for (CashierCache cashierCache : cache.get(cashier.getUsername())) {
            if (cashierCache.getCommodityId().toString().equals(editCache.getCommodityId().toString())) {
                notFound = false;
                cashierCache.setAmount(editCache.getAmount());
            }
            details.add(cashierCache);
        }
        if (notFound) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        cache.put(cashier.getUsername(), details);
    }

    public void addToCache(String barcode) {
        UserApp cashier = (UserApp) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Commodity commodity = findByBarcode(barcode);
        Set<CashierCache> details = new HashSet<>();
        boolean notFound = true;
        if (cache.containsKey(cashier.getUsername())) {
            for (CashierCache cashierCache : cache.get(cashier.getUsername())) {
                if (cashierCache.getCommodityId().toString().equals(commodity.getId().toString())) {
                    notFound = false;
                    int amount = cashierCache.getAmount() + 1;
                    cashierCache.setAmount(amount);
                }
                details.add(cashierCache);
            }
            if (notFound) {
                CashierCache cashierCache = new CashierCache(commodity.getId(), commodity.getName(),
                        commodity.getSalesMeasurement().getName(), 1, commodity.getSalesPrice());
                details.add(cashierCache);
            }
        } else {
            CashierCache cashierCache = new CashierCache(commodity.getId(), commodity.getName(),
                    commodity.getSalesMeasurement().getName(), 1, commodity.getSalesPrice());
            details.add(cashierCache);
        }
        cache.put(cashier.getUsername(), details);
    }

    public ResponseEntity findGrandTotalCache() {
        UserApp cashier = (UserApp) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        double grandTotal = 0;
        if (cache.containsKey(cashier.getUsername())) {
            for (CashierCache cashierCache: cache.get(cashier.getUsername())) {
                grandTotal += (cashierCache.getAmount() * cashierCache.getUnitPrice().doubleValue());
            }
        }
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("grandTotal", GlobalUtil.convertToCurrencyFormat(grandTotal));
        return ResponseEntity.ok(hashMap);
    }

    public ResponseEntity save(SalesLite salesLite) {
        UserApp cashier = (UserApp) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!cache.containsKey(cashier.getUsername())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Not any transaction yet");
        }
        if (cache.get(cashier.getUsername()).isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Not any transaction yet");
        }
        checkLastTransaction();
        String salesNumberPrefix = "SL/" + lastTransactionCalendar.get(Calendar.YEAR)
                + "/" + (String.format("%02d", lastTransactionCalendar.get(Calendar.MONTH)))
                + "/";
        Optional<String> optional = repository.getLastStationTransactionIndex(salesNumberPrefix);
        int index = Integer.parseInt(optional.orElse("0"));
        String salesNumber = salesNumberPrefix + (String.format("%04d", index + 1));
        salesLite.setSalesNumber(salesNumber);
        Set<SalesDetail> details = new HashSet<>();
        for (CashierCache cashierCache : cache.get(cashier.getUsername())) {
            SalesDetailPK pk = new SalesDetailPK(salesNumber, cashierCache.getCommodityId());
            SalesDetail salesDetail = new SalesDetail(pk, cashierCache.getAmount(), cashierCache.getUnitPrice(), null,null);
            details.add(salesDetail);
        }
        repository.save(salesLite.convert());
        for (SalesDetail salesDetail: details) {
            detailRepository.save(salesDetail);
        }
        cache.remove(cashier.getUsername());
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("salesNumber", salesNumber);
        return ResponseEntity.ok(hashMap);
    }

    public Page<Sales> findPage(int pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber, 10, Sort.Direction.DESC, "createdDate");
        return repository.findAll(pageable);
    }

    public Sales findBySalesNumber(String salesNumber) {
        Optional<Sales> optional = repository.findById(salesNumber);
        if (optional.isPresent()){
            return optional.get();
        }else{
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Sales number " + salesNumber + " not found");
        }
    }

    public ResponseEntity add(@NotNull SalesLite sales) {
        checkLastTransaction();
        String salesNumberPrefix = "SL/" + lastTransactionCalendar.get(Calendar.YEAR)
                + "/" + (String.format("%02d", lastTransactionCalendar.get(Calendar.MONTH)))
                + "/";
        Optional<String> optional = repository.getLastStationTransactionIndex(salesNumberPrefix);
        int index = Integer.parseInt(optional.orElse("0"));
        checkLastTransaction();
        String salesNumber = salesNumberPrefix + (String.format("%04d", index + 1));
        sales.setSalesNumber(salesNumber);
        repository.save(sales.convert());
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("salesNumber", salesNumber);
        return ResponseEntity.ok(hashMap);
    }

    public void Update(@NotNull SalesLite sales) {
        if (!repository.existsById(sales.getSalesNumber())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        repository.save(sales.convert());
    }

    public void addDetail(SalesDetailLite salesDetail) {
        if (detailRepository.existsById(salesDetail.getId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        Optional<Commodity> optional = commodityRepository.findById(salesDetail.getId().getCommodityId());
        if (optional.isEmpty()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        Commodity commodity = optional.get();
        salesDetail.setUnitPrice(commodity.getSalesPrice());
        detailRepository.save(salesDetail.convert());
    }

    public void UpdateDetail(SalesDetailLite salesDetail) {
        if (!detailRepository.existsById(salesDetail.getId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        Optional<Commodity> optional = commodityRepository.findById(salesDetail.getId().getCommodityId());
        if (optional.isEmpty()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        Commodity commodity = optional.get();
        salesDetail.setUnitPrice(commodity.getSalesPrice());
        detailRepository.save(salesDetail.convert());
    }

    public List<CommonCombo> findAvailableCommodity(String salesNumber) {
        List<Long> ids = detailRepository.findCommodityIdListBySalesNumber(salesNumber);
        Sort sort = Sort.by(Sort.Direction.ASC, "name");
        if (ids.isEmpty()){
            return commodityRepository.findCombo(sort);
        }
        return commodityRepository.findComboByIdNotIn(ids, sort);
    }
}
