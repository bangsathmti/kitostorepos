package com.kito.pos.service.transaction;

import com.kito.pos.domain.transaction.Purchasing;
import com.kito.pos.dto.CommonCombo;
import com.kito.pos.dto.transaction.PurchasingDetailLite;
import com.kito.pos.dto.transaction.PurchasingLite;
import com.kito.pos.repository.master.CommodityRepository;
import com.kito.pos.repository.transaction.PurchasingDetailRepository;
import com.kito.pos.repository.transaction.PurchasingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

/**
 * @author <a href="http://www.tabs.co.id/">PT. Tab Solutions</a> - Abdul
 * Hakam.<br>
 * Created on Mar 12, 2022, at 5:38:57 PM
 */
@Service
@Transactional
public class PurchasingService {

    private final PurchasingRepository repository;
    private final PurchasingDetailRepository detailRepository;
    private final CommodityRepository commodityRepository;

    @Autowired
    public PurchasingService(PurchasingRepository repository, PurchasingDetailRepository detailRepository, CommodityRepository commodityRepository) {
        this.repository = repository;
        this.detailRepository = detailRepository;
        this.commodityRepository = commodityRepository;
    }

    public Page<Purchasing> findPage(int pageNumber) {
        String[] properties = {"supplier.name","purchaseDate", "createdDate"};
        Pageable pageable = PageRequest.of(pageNumber, 10, Sort.Direction.ASC, properties);
        return repository.findAll(pageable);
    }

    public Purchasing findById(String purchaseNumber) {
        Optional<Purchasing> optional = repository.findById(purchaseNumber);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Purchase number " + purchaseNumber + " is not exists");
        }
    }

    public void save(PurchasingLite purchasing) {
        if (repository.existsById(purchasing.getPurchaseNumber())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        try {
            repository.save(purchasing.convert());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
        }
    }

    public void Update(PurchasingLite purchasing) {
        if (!repository.existsById(purchasing.getPurchaseNumber())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        try {
            repository.save(purchasing.convert());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
        }
    }

    public void addDetail(PurchasingDetailLite purchasingDetail) {
        if (detailRepository.existsById(purchasingDetail.getId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        try {
            detailRepository.save(purchasingDetail.convert());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
        }
    }

    public void UpdateDetail(PurchasingDetailLite purchasingDetail) {
        if (!detailRepository.existsById(purchasingDetail.getId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        try {
            detailRepository.save(purchasingDetail.convert());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
        }
    }

    public List<CommonCombo> findAvailableCommodity(String purchaseNumber) {
        List<Long> ids = detailRepository.findCommodityIdListByPurchaseNumber(purchaseNumber);
        Sort sort = Sort.by(Sort.Direction.ASC, "name");
        if (ids.isEmpty()){
            return commodityRepository.findCombo(sort);
        }
        return commodityRepository.findComboByIdNotIn(ids, sort);
    }
}
