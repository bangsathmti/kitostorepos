"use strict";
function setDashboardData() {
    $.ajax({
        url: "/dashboard/data",
        dataType: "json"
    }).done(
        function (data) {
            $('#dashboard').addClass("active");
            $('#totalStockInCurrency').text(data.totalStockInCurrency);
            $('#thisMonthPurchaseGrandTotal').text(data.thisMonthPurchaseGrandTotal);
            $('#thisDaySalesGrandTotal').text(data.thisDaySalesGrandTotal);
            $('#thisMonthSalesGrandTotal').text(data.thisMonthSalesGrandTotal);
            $('#monthSalesGrandTotalName').text(data.monthName);
            $('#todaySalesGrandTotalName').text(data.todayName);
            $('#monthPurchaseGrandTotalName').text(data.monthName);
            var tabelFill = '<table class="table table-striped table-valign-middle"><thead><tr><th>Commodity</th>' +
                    '<th>Stock</th><th>Price</th><th>Total</th></tr></thead><tbody>';
            for (var i = 0; i < data.fiveMostStock.length; i++) {
                tabelFill += '<tr><td>' + data.fiveMostStock[i].name + '</td><td>' + data.fiveMostStock[i].stockString
                        + '</td><td>' + data.fiveMostStock[i].salesPriceString + '</td><td>'
                        + data.fiveMostStock[i].stockInCurrency + '</td></tr>';
            }
            if (data.fiveMostStock.length < 5) {
                for (var i = 0; i < 5 - data.fiveMostStock.length; i++) {
                    tabelFill += '<tr><td>-</td><td>-</td><td>-</td></tr>';
                }
            }
            tabelFill += '</tbody></table>';
            document.getElementById("fms").outerHTML = tabelFill;
            var tabelfmsl = '<table class="table table-striped table-valign-middle"><thead><tr><th>Commodity</th>' +
                '<th>Jumlah terjual</th></tr></thead><tbody>';
            for (var i = 0; i < data.fiveMostSale.length; i++) {
                tabelfmsl += '<tr><td>' + data.fiveMostSale[i].name + '</td><td>' + data.fiveMostSale[i].amountString +
                    '</td></tr>';
            }
            if (data.fiveMostSale.length < 5) {
                for (var i = 0; i < 5 - data.fiveMostSale.length; i++) {
                    tabelfmsl += '<tr><td>-</td><td>-</td></tr>';
                }
            }
           tabelfmsl += '</tbody></table>';
           document.getElementById("fmsl").outerHTML = tabelfmsl;
        }
    );
}
