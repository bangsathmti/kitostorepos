$(function () {
    "use strict";
    /*
     ============================================================================
     =================                Initiation              ===================
     ============================================================================
     **/
    var changePasswordForm = $("#changePasswordForm");
    changePasswordForm.validate({
        rules: {
            oldPassword: {required: true},
            newPassword: {required: true},
            newPasswordConfirmation: {required: true}
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("is-invalid");
        }
    });
    /*
     ============================================================================
     =================                Page Add               ====================
     ============================================================================
     **/
    $("#changePasswordButton").click(function () {
        changePasswordForm[0].reset();
        $("#modal_change_password").modal("show");
    });
    $("#cancelChangePasswordAddButton").click(function () {
        $("#modal_change_password").modal("hide");
        parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
    });
    $("#submitChangePasswordButton").click(function () {
        if (changePasswordForm.valid()) {
            submitForm();
        } else {
            Swal.fire({
                icon: "warning",
                title: "Warning",
                text: "Please complete the input form"
            });
        }
        parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
        parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
    });
    /*
     ============================================================================
     =================             Fungsi Submit             ====================
     ============================================================================
     **/
    function getFormValue($form) {
        var unindexed_array = $form.serializeArray();
        var indexed_array = {};
        var form = {};
        $.map(unindexed_array, function (n) {
            indexed_array[n["name"]] = n["value"];
        });
        form = {
            oldPassword: indexed_array.oldPassword,
            newPassword: indexed_array.newPassword,
            newPasswordConfirmation: indexed_array.newPasswordConfirmation
        };
        return JSON.stringify(form);
    }

    function submitForm() {
        var value = getFormValue(changePasswordForm);
        $.ajax({
            url: "/userApp/changePassword",
            type: "PUT",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            data: value,
            success: function () {
                Swal.fire({
                    title: "Success",
                    text: "Change password success"
                }).then(function () {
                    suksesSubmit();
                });
            },
            error: function (data) {
                Swal.fire({
                    icon: "error",
                    title: data.statusText,
                    text: data.responseJSON.message
                });
            }
        });
    }
    function suksesSubmit() {
        $("#modal_change_password").modal("hide");
        window.parent.postMessage({message: "to_top"}, window.location.origin);
    }
});

