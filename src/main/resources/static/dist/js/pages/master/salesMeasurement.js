"use strict";
var salesMeasurementInputForm = $("#salesMeasurementForm");
var salesMeasurementSubmitType = "";
var salesMeasurementNameVar = "";
/*
============================================================================
=================                Initiation              ===================
============================================================================
**/
function setSalesMeasurementMenuContent() {
    salesMeasurementInputForm = $("#salesMeasurementForm");
    salesMeasurementSubmitType = "";
    salesMeasurementNameVar = "";
    salesMeasurementInputForm.validate({
        rules: {
            salesMeasurementNameInputField: {required: true},
            salesMeasurementShortNameInputField: {required: true}
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("is-invalid");
        }
    });
    reloadSalesMeasurementList();
}
function reloadSalesMeasurementList() {
    $("#salesMeasurementList").DataTable().clear().destroy();
    var table = $("#salesMeasurementList").DataTable({
        serverMethod: "GET",
        order:[],
        language: {
            processing: '<img src="/dist/img/logokito.png">'
        },
        ajax: {
            url: "/salesMeasurement",
            dataSrc: ""
        },
        columns: [
            {data: "id", visible: false, searchable: false},
            {data: "name"},
            {data: "shortName"},
            {data: "description"},
            {
                mRender: function () {
                    return `<div align="center"><a id="salesMeasurementEditTrigger" href="javascript:void(0);" title="Ubah satuan"><i class="fas fa-edit fa-fw mr-1 edit-data-grid"></i></a><a id="salesMeasurementDeleteTrigger" href="javascript:void(0);" title="Hapus satuan"><i class="fas fa-trash fa-fw text-red"></i></a></div>`;
                }, orderable: false, searchable: false
            }
        ]
    });
    $("#salesMeasurementList").find("tbody").off("click", "#salesMeasurementEditTrigger");
    $("#salesMeasurementList").find("tbody").off("click", "#salesMeasurementDeleteTrigger");
    $("#salesMeasurementList tbody").on("click", "#salesMeasurementEditTrigger", function () {
        var data = table.row($(this).parents("tr")).data();
        editSalesMeasurementInitiation(data);
    });
    $("#salesMeasurementList tbody").on("click", "#salesMeasurementDeleteTrigger", function () {
        var data = table.row($(this).parents("tr")).data();
        deleteSalesMeasurement(data);
    });
}
/*
============================================================================
=================                Cancel modal            ===================
============================================================================
**/
function cancelSalesMeasurementOperation(){
    salesMeasurementSubmitType = "";
    $("#salesMeasurementModal").modal("hide");
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
}
/*
============================================================================
=================                add modal               ===================
============================================================================
**/
function addSalesMeasurementInitiation() {
    salesMeasurementSubmitType = "POST";
    salesMeasurementInputForm[0].reset();
    document.getElementById("salesMeasurementIdInputField").disabled = true;
    document.getElementById("salesMeasurementNameInputField").disabled = false;
    document.getElementById("salesMeasurementShortNameInputField").disabled = false;
    document.getElementById("salesMeasurementDescriptionInputField").disabled = false;
    $("#salesMeasurementModalTitle").text("Tambah satuan");
    $("#salesMeasurementModal").modal("show");
}
/*
============================================================================
=================                edit modal              ===================
============================================================================
**/
function editSalesMeasurementInitiation(data) {
     salesMeasurementSubmitType = "PUT";
     $("#salesMeasurementIdInputField").val(data.id);
     document.getElementById("salesMeasurementIdInputField").disabled = false;
     $("#salesMeasurementNameInputField").val(data.name);
     document.getElementById("salesMeasurementNameInputField").disabled = false;
     $("#salesMeasurementShortNameInputField").val(data.shortName);
     document.getElementById("salesMeasurementShortNameInputField").disabled = false;
     $("#salesMeasurementDescriptionInputField").val(data.description);
     document.getElementById("salesMeasurementDescriptionInputField").disabled = false;
     $("#salesMeasurementModalTitle").text("Ubah satuan " + data.name);
     $("#salesMeasurementModal").modal("show");
}
/*
============================================================================
=================               Submit operation         ===================
============================================================================
**/
function submitSalesMeasurementForm(){
    if (salesMeasurementInputForm.valid()) {
        var unIndexed_array = salesMeasurementInputForm.serializeArray();
        var indexed_array = {};
        var form = {};
        $.map(unIndexed_array, function (n) {
            indexed_array[n["name"]] = n["value"];
        });
        salesMeasurementNameVar = indexed_array.salesMeasurementNameInputField;
        if (salesMeasurementSubmitType == "POST") {
            form = {
                name: indexed_array.salesMeasurementNameInputField,
                shortName: indexed_array.salesMeasurementShortNameInputField,
                description: indexed_array.salesMeasurementDescriptionInputField
            };
        } else {
            form = {
                id: indexed_array.salesMeasurementIdInputField,
                name: indexed_array.salesMeasurementNameInputField,
                shortName: indexed_array.salesMeasurementShortNameInputField,
                description: indexed_array.salesMeasurementDescriptionInputField
            };
        }
        var value = JSON.stringify(form);
        $.ajax({
            url: "/salesMeasurement",
            type: salesMeasurementSubmitType,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            data: value,
            success: function () {
                Swal.fire({
                    icon: "success",
                    title: "Success",
                    text: getSalesMeasurementOperationMessage()
                }).then(function () {
                    salesMeasurementNameVar = "";
                    $("#salesMeasurementModal").modal("hide");
                    reloadSalesMeasurementList();
                    window.parent.postMessage({message: "to_top"}, window.location.origin);
                });
            },
            error: function (data) {
                Swal.fire({
                    icon: "error",
                    title: data.responseJSON.error,
                    text: data.responseJSON.message
                });
            }
        });
    } else {
        Swal.fire({
            icon: "warning",
            title: "Warning",
            text: "Mohon lengkapi data yang dibutuhkan"
        });
    }
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
}
function getSalesMeasurementOperationMessage() {
    if (salesMeasurementSubmitType == "POST") {
        return "Satuan " + salesMeasurementNameVar + " berhasil ditambahkan";
    } else {
        return "Satuan diubah ke " + salesMeasurementNameVar;
    }
}
/*
============================================================================
=================                delete process          ===================
============================================================================
**/
function deleteSalesMeasurement(dataSalesMeasurement) {
    salesMeasurementNameVar = "";
    Swal.fire({
        title: "Anda yakin akan menghapus satuan " + dataSalesMeasurement.name,
        icon: "warning",
        showCancelButton: true,
        dangerMode: true
    }).then(function (result) {
        if (result.isConfirmed) {
          $.ajax({
              url: "/salesMeasurement?id=" + dataSalesMeasurement.id,
              type: "DELETE",
              dataType: "json",
              contentType: "application/json; charset=utf-8",
              success: function (data) {
                  Swal.fire({
                      title: "Satuan " + dataSalesMeasurement.name + " berhasil dihapus",
                      icon: "success"
                  });
                  reloadSalesMeasurementList();
              },
              error: function (data) {
                  Swal.close();
                  if (data.status === 200 || data.status === 201) {
                      Swal.fire({
                          icon: "success",
                          title: "Success",
                          text: "Satuan " + dataSalesMeasurement.name + " berhasil dihapus"
                      }).then(function () {
                          reloadSalesMeasurementList();
                      });
                  } else {
                      Swal.fire({
                          icon: "error",
                          title: data.responseJSON.status,
                          text: data.responseJSON.message
                      });
                  }
              }
          });
        }
  });
}