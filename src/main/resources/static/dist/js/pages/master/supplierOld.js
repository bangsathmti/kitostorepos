$(function () {
    "use strict";
    var formInput = $("#formRequestAdd");
    var jenisSubmit = "";
    var supplierName = "";
    formInput.validate({
        rules: {
            name: {required: true},
            contact: {required: true}
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("is-invalid");
        }
    });
    function setDataTable() {
        $('#masterSupplier').addClass("active");
        $("#supplierList").DataTable().clear().destroy();
        var table = $("#supplierList").DataTable({
            serverMethod: "GET",
            ajax: {
                url: "/supplier",
                dataSrc: ""
            },
            columns: [
                {data: "id", visible: false, searchable: false},
                {data: "name"},
                {data: "contact"},
                {data: "address"},
                {
                    mRender: function () {
                        return `<div align="center">
                      <a id="editFunc" href="javascript:void(0);" title="Ubah supplier"><i class="fas fa-edit fa-fw mr-1 edit-data-grid"></i></a>
                      <a id="deleteFunc" href="javascript:void(0);" title="Hapus supplier"><i class="fas fa-trash fa-fw text-red"></i></a>
                    </div>`;
                    }, orderable: false, searchable: false
                }
            ]
        });
        $("#supplierList").find("tbody").off("click", "#editFunc");
        $("#supplierList").find("tbody").off("click", "#deleteFunc");
        $("#supplierList tbody").on("click", "#editFunc", function () {
            var data = table.row($(this).parents("tr")).data();
            setValueEdit(data);
        });
        $("#supplierList tbody").on("click", "#deleteFunc", function () {
            var data = table.row($(this).parents("tr")).data();
            deleteFunc(data);
        });
    }
    setDataTable();
    $("#addButton").click(function () {
        addForm();
        $(".modal-title").text("Tambah supplier");
        $("#modal_add").modal("show");
    });
    function addForm() {
        jenisSubmit = "POST";
        formInput[0].reset();
        document.getElementById("supplierId").disabled = true;
        document.getElementById("name").disabled = false;
        document.getElementById("contact").disabled = false;
        document.getElementById("address").disabled = false;
    }
    $("#backAddButton").click(function () {
        jenisSubmit = "";
        setDataTable();
        $("#modal_add").modal("hide");
        parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
    });
    $("#submitAddButton").click(function () {
        if (formInput.valid()) {
            submitForm();
        } else {
            Swal.fire({
                icon: "warning",
                title: "Warning",
                text: "Mohon lengkapi data yang dibutuhkan"
            });
        }
        parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
    });
    function setValueEdit(data) {
        jenisSubmit = "PUT";
        $("#supplierId").val(data.id);
        document.getElementById("supplierId").disabled = false;
        $("#name").val(data.name);
        document.getElementById("name").disabled = false;
        $("#contact").val(data.contact);
        document.getElementById("contact").disabled = false;
        $("#address").val(data.address);
        document.getElementById("address").disabled = false;
        $(".modal-title").text("Ubah supplier " + data.name);
        $("#modal_add").modal("show");
    }
    function deleteFunc(supplier) {
        supplierName = "";
        Swal.fire({
            title: "Anda yakin akan menghapus supplier " + supplier.name,
            icon: "warning",
            showCancelButton: true,
            dangerMode: true
        }).then(function (result) {
            if (result.isConfirmed) {
                $.ajax({
                    url: "/supplier?id=" + supplier.id,
                    type: "DELETE",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        Swal.fire({
                            title: "Supplier " + supplier.name + " berhasil dihapus",
                            icon: "success"
                        });
                        setDataTable();
                    },
                    error: function (data) {
                        Swal.close();
                        if (data.status === 200 || data.status === 201) {
                            Swal.fire({
                                title: "Success",
                                text: "Supplier " + supplier.name + " berhasil dihapus"
                            }).then(function () {
                                setDataTable();
                            });
                        } else {
                            Swal.fire({
                                icon: "error",
                                title: "Error",
                                text: data.responseJSON.message
                            });
                        }
                    },
                })
            } else {
                Swal.fire("Cancel", "supplier " + supplier.name + " batal dihapus");
            }
        });
    }
    function getMessage() {
        if (jenisSubmit == "POST") {
            return "Supplier " + supplierName + " berhasil ditambahkan";
        } else {
            return "Supplier diubah ke " + supplierName;
        }
    }
    function getFormValue($form) {
        var unindexed_array = $form.serializeArray();
        var indexed_array = {};
        var form = {};
        $.map(unindexed_array, function (n) {
            indexed_array[n["name"]] = n["value"];
        });
        supplierName = indexed_array.name;
        if (jenisSubmit == "POST") {
            form = {
                name: indexed_array.name,
                contact: indexed_array.contact,
                address: indexed_array.address
            };
            return JSON.stringify(form);
        } else {
            form = {
                id: indexed_array.supplierId,
                name: indexed_array.name,
                contact: indexed_array.contact,
                address: indexed_array.address
            };
            return JSON.stringify(form);
        }
    }
    function submitForm() {
        var value = getFormValue(formInput);
        $.ajax({
            url: "/supplier",
            type: jenisSubmit,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            data: value,
            success: function () {
                Swal.fire({
                    title: "Success",
                    text: getMessage()
                }).then(function () {
                    suksesSubmit();
                });
            },
            error: function (data) {
                Swal.fire({
                    icon: "error",
                    title: data.statusText,
                    text: data.responseJSON.message
                });
            }
        });
    }
    function suksesSubmit() {
        supplierName = "";
        $("#modal_add").modal("hide");
        setDataTable();
        window.parent.postMessage({message: "to_top"}, window.location.origin);
    }

});