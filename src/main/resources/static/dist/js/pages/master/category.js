"use strict";
var formInputCategory = $("#formCategory");
var formCategorySubmitType = "";
var categoryNameVar = "";
/*
============================================================================
=================                Initiation              ===================
============================================================================
**/
function setCategoryMenuContent() {
    formInputCategory = $("#formCategory");
    formCategorySubmitType = "";
    categoryNameVar = "";
    formInputCategory.validate({
        rules: {
            name: {required: true}
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("is-invalid");
        }
    });
    reloadCategoryList();
}
/*
============================================================================
=================                reload list             ===================
============================================================================
**/
function reloadCategoryList(){
    $("#categoryList").DataTable().clear().destroy();
    var table = $("#categoryList").DataTable({
        serverMethod: "GET",
        order:[],
        language: {
            processing: '<img src="/dist/img/logokito.png">'
        },
        ajax: {
            url: "/category",
            dataSrc: ""
        },
        columns: [
            {data: "id", visible: false, searchable: false, orderable: false},
            {data: "name"},
            {data: "description"},
            {
                mRender: function () {
                    return `<div align="center"><a id="editCategoryMenu" href="javascript:void(0);" title="Ubah category"><i class="fas fa-edit fa-fw mr-1 edit-data-grid"></i></a><a id="deleteCategoryMenu" href="javascript:void(0);" title="Hapus category"><i class="fas fa-trash fa-fw text-red"></i></a></div>`;
                }, orderable: false, searchable: false
            }
        ]
    });
    $("#categoryList").find("tbody").off("click", "#editCategoryMenu");
    $("#categoryList").find("tbody").off("click", "#deleteCategoryMenu");
    $("#categoryList tbody").on("click", "#editCategoryMenu", function () {
        var data = table.row($(this).parents("tr")).data();
        editCategoryInitiation(data);
    });
    $("#categoryList tbody").on("click", "#deleteCategoryMenu", function () {
        var data = table.row($(this).parents("tr")).data();
        deleteCategory(data);
    });
}
/*
============================================================================
=================                Cancel modal            ===================
============================================================================
**/
function cancelCategoryOperation(){
    formCategorySubmitType = "";
    $("#modalCategory").modal("hide");
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
}
/*
============================================================================
=================                add modal               ===================
============================================================================
**/
function addCategoryInitiation() {
    formCategorySubmitType = "POST";
    formInputCategory[0].reset();
    document.getElementById("categoryIdInputField").disabled = true;
    document.getElementById("categoryNameInputField").disabled = false;
    document.getElementById("categoryDescriptionInputField").disabled = false;
    $("#modalCategoryTitle").text("Tambah kategori baru");
    $("#modalCategory").modal("show");
}
/*
============================================================================
=================                edit modal              ===================
============================================================================
**/
function editCategoryInitiation(data) {
    formCategorySubmitType = "PUT";
    $("#categoryIdInputField").val(data.id);
    document.getElementById("categoryIdInputField").disabled = false;
    $("#categoryNameInputField").val(data.name);
    document.getElementById("categoryNameInputField").disabled = false;
    $("#categoryDescriptionInputField").val(data.description);
    document.getElementById("categoryDescriptionInputField").disabled = false;
    $("#modalCategoryTitle").text("Ubah kategori " + data.name);
    $("#modalCategory").modal("show");
}
/*
============================================================================
=================               Submit operation         ===================
============================================================================
**/
function submitCategoryForm(){
    if (formInputCategory.valid()) {
        var unIndexed_array = formInputCategory.serializeArray();
        var indexed_array = {};
        var form = {};
        $.map(unIndexed_array, function (n) {
            indexed_array[n["name"]] = n["value"];
        });
        categoryNameVar = indexed_array.categoryNameInputField;
        if (formCategorySubmitType == "POST") {
            form = {
                name: indexed_array.categoryNameInputField,
                description: indexed_array.categoryDescriptionInputField
            };
        } else {
            form = {
                id: indexed_array.categoryIdInputField,
                name: indexed_array.categoryNameInputField,
                description: indexed_array.categoryDescriptionInputField
            };
        }
        var value = JSON.stringify(form);
        $.ajax({
            url: "/category",
            type: formCategorySubmitType,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            data: value,
            success: function () {
                Swal.fire({
                    icon: "success",
                    title: "Success",
                    text: getCategoryOperationMessage()
                }).then(function () {
                    categoryNameVar = "";
                    $("#modalCategory").modal("hide");
                    reloadCategoryList();
                    window.parent.postMessage({message: "to_top"}, window.location.origin);
                });
            },
            error: function (data) {
                Swal.fire({
                    icon: "error",
                    title: data.responseJSON.error,
                    text: data.responseJSON.message
                });
            }
        });
    } else {
        Swal.fire({
            icon: "warning",
            title: "Warning",
            text: "Mohon lengkapi data yang dibutuhkan"
        });
    }
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
}
function getCategoryOperationMessage() {
    if (formCategorySubmitType == "POST") {
        return "Kategori " + categoryNameVar + " berhasil ditambahkan";
    } else {
        return "Kategori diubah ke " + categoryNameVar;
    }
}
/*
============================================================================
=================                delete process          ===================
============================================================================
**/
function deleteCategory(dataCategory) {
    categoryNameVar = "";
    Swal.fire({
        title: "Anda yakin akan menghapus kategori " + dataCategory.name,
        icon: "question",
        showCancelButton: true,
        dangerMode: true
    }).then(function (result) {
        if (result.isConfirmed) {
          $.ajax({
              url: "/category?id=" + dataCategory.id,
              type: "DELETE",
              dataType: "json",
              contentType: "application/json; charset=utf-8",
              success: function (data) {
                  Swal.fire({
                      title: "Kategori " + dataCategory.name + " berhasil dihapus",
                      icon: "success"
                  });
                  reloadCategoryList();
              },
              error: function (data) {
                  Swal.close();
                  if (data.status === 200 || data.status === 201) {
                      Swal.fire({
                          icon: "success",
                          title: "Success",
                          title: "Kategori " + dataCategory.name + " berhasil dihapus"
                      }).then(function () {
                          reloadCategoryList();
                      });
                  } else {
                      Swal.fire({
                          icon: "error",
                          title: data.responseJSON.status,
                          text: data.responseJSON.message
                      });
                  }
              }
          });
        }
  });
}