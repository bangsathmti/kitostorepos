$(function () {
    "use strict";
    var formInput = $("#formRequestAdd");
    var jenisSubmit = "";
    var usernameName = "";
    formInput.validate({
        rules: {
            username: {required: true},
            personId: {required: true},
            name: {required: true}
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("is-invalid");
        }
    });
    function setDataTable() {
        $('#masterUserApp').addClass("active");
        $("#userList").DataTable().clear().destroy();
        var table = $("#userList").DataTable({
            serverMethod: "GET",
            ajax: {
                url: "/userApp",
                dataSrc: ""
            },
            columns: [
                {data: "username"},
                {data: "personId"},
                {data: "name"},
                {data: "userRole"},
                {
                    mRender: function () {
                        return `<div align="center">
                      <a id="editFunc" href="javascript:void(0);" title="Ubah user"><i class="fas fa-edit fa-fw mr-1 edit-data-grid"></i></a>
                      <a id="deleteFunc" href="javascript:void(0);" title="Hapus user"><i class="fas fa-trash fa-fw text-red"></i></a>
                    </div>`;
                    }, orderable: false, searchable: false
                }
            ]
        });
        $("#userList").find("tbody").off("click", "#editFunc");
        $("#userList").find("tbody").off("click", "#deleteFunc");
        $("#userList tbody").on("click", "#editFunc", function () {
            var data = table.row($(this).parents("tr")).data();
            setValueEdit(data);
        });
        $("#userList tbody").on("click", "#deleteFunc", function () {
            var data = table.row($(this).parents("tr")).data();
            deleteFunc(data);
        });
    }
    setDataTable();
    $("#addButton").click(function () {
        addForm();
        $(".modal-title").text("Tambah user");
        $("#modal_add").modal("show");
    });
    function addForm() {
        jenisSubmit = "POST";
        formInput[0].reset();
        document.getElementById("username").disabled = false;
        document.getElementById("username").readOnly = false;
        document.getElementById("personId").disabled = false;
        document.getElementById("name").disabled = false;
        document.getElementById("userRole").disabled = false;
    }
    $("#backAddButton").click(function () {
        jenisSubmit = "";
        setDataTable();
        $("#modal_add").modal("hide");
        parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
    });
    $("#submitAddButton").click(function () {
        if (formInput.valid()) {
            submitForm();
        } else {
            Swal.fire({
                icon: "warning",
                title: "Warning",
                text: "Mohon lengkapi data yang dibutuhkan"
            });
        }
        parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
    });
    function setValueEdit(data) {
        jenisSubmit = "PUT";
        $("#username").val(data.username);
        document.getElementById("username").disabled = false;
        document.getElementById("username").readOnly = true;
        $("#personId").val(data.personId);
        document.getElementById("personId").disabled = false;
        $("#name").val(data.name);
        document.getElementById("name").disabled = false;
        $("#userRole").val(data.userRole);
        $("#userRole").trigger("change");
        document.getElementById("userRole").disabled = false;
        $(".modal-title").text("Ubah user " + data.username);
        $("#modal_add").modal("show");
    }
    function deleteFunc(datauser) {
        usernameName = "";
        Swal.fire({
            title: "Anda yakin akan menghapus user " + datauser.username,
            icon: "warning",
            showCancelButton: true,
            dangerMode: true
        }).then(function (result) {
            if (result.isConfirmed) {
                $.ajax({
                    url: "/userApp?username=" + datauser.username,
                    type: "DELETE",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        Swal.fire({
                            title: "User " + datauser.username + " berhasil dihapus",
                            icon: "success"
                        });
                        setDataTable();
                    },
                    error: function (data) {
                        Swal.close();
                        if (data.status === 200 || data.status === 201) {
                            Swal.fire({
                                title: "Success",
                                text: "User " + datauser.username + " berhasil dihapus"
                            }).then(function () {
                                setDataTable();
                            });
                        } else {
                            Swal.fire({
                                icon: "error",
                                title: "Error",
                                text: data.responseJSON.message
                            });
                        }
                    }
                });
            } else {
                Swal.fire("Cancel", "User " + datauser.username + " batal dihapus");
            }
        });
    }
    function getMessage() {
        if (jenisSubmit == "POST") {
            return "User " + usernameName + " berhasil ditambahkan";
        } else {
            return "User diubah ke " + usernameName;
        }
    }
    function getFormValue($form) {
        var unindexed_array = $form.serializeArray();
        var indexed_array = {};
        var form = {};
        $.map(unindexed_array, function (n) {
            indexed_array[n["name"]] = n["value"];
        });
        usernameName = indexed_array.username;
        form = {
            username: indexed_array.username,
            personId: indexed_array.personId,
            name: indexed_array.name,
            userRole: indexed_array.userRole
        };
        return JSON.stringify(form);
    }
    function submitForm() {
        var value = getFormValue(formInput);
        $.ajax({
            url: "/userApp",
            type: jenisSubmit,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            data: value,
            success: function () {
                Swal.fire({
                    title: "Success",
                    text: getMessage()
                }).then(function () {
                    suksesSubmit();
                });
            },
            error: function (data) {
                Swal.fire({
                    icon: "error",
                    title: data.statusText,
                    text: data.responseJSON.message
                });
            }
        });
    }
    function suksesSubmit() {
        usernameName = "";
        $("#modal_add").modal("hide");
        setDataTable();
        window.parent.postMessage({message: "to_top"}, window.location.origin);
    }
});