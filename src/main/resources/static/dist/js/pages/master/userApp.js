"use strict";
var formInputUser = $("#formUser");
var formUserSubmitType = "";
var userUsernameVar = "";
/*
============================================================================
=================                Initiation              ===================
============================================================================
**/
function setUserMenuContent() {
    formInputUser = $("#formUser");
    formUserSubmitType = "";
    userUsernameVar = "";
    formInputUser.validate({
        rules: {
            userUsername: {required: true},
            userPersonId: {required: true},
            userName: {required: true},
            userUserRole: {required: true}
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("is-invalid");
        }
    });
    reloadUserList();
}
/*
============================================================================
=================                reload list             ===================
============================================================================
**/
function reloadUserList(){
    $("#userList").DataTable().clear().destroy();
    var table = $("#userList").DataTable({
        serverMethod: "GET",
        order:[],
        language: {
            processing: '<img src="/dist/img/logokito.png">'
        },
        ajax: {
            url: "/userApp",
            dataSrc: ""
        },
        columns: [
            {data: "username"},
            {data: "personId"},
            {data: "name"},
            {data: "userRole"},
            {
                mRender: function () {
                    return `<div align="center"><a id="editUserFunc" href="javascript:void(0);" title="Ubah user"><i class="fas fa-edit fa-fw mr-1 edit-data-grid"></i></a><a id="deleteUserFunc" href="javascript:void(0);" title="Hapus user"><i class="fas fa-trash fa-fw text-red"></i></a></div>`;
                }, orderable: false, searchable: false
            }
        ]
    });
    $("#userList").find("tbody").off("click", "#editUserFunc");
    $("#userList").find("tbody").off("click", "#deleteUserFunc");
    $("#userList tbody").on("click", "#editUserFunc", function () {
        var data = table.row($(this).parents("tr")).data();
        editUserInitiation(data);
    });
    $("#userList tbody").on("click", "#deleteUserFunc", function () {
        var data = table.row($(this).parents("tr")).data();
        deleteUser(data);
    });
}
/*
============================================================================
=================                Cancel modal            ===================
============================================================================
**/
function cancelUserOperation(){
    formUserSubmitType = "";
    $("#userModal").modal("hide");
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
}
/*
============================================================================
=================                add modal               ===================
============================================================================
**/
function addUserInitiation() {
    formUserSubmitType = "POST";
    formInputUser[0].reset();
    document.getElementById("userUsername").disabled = false;
    document.getElementById("userUsername").readOnly = false;
    document.getElementById("userPersonId").disabled = false;
    document.getElementById("userName").disabled = false;
    document.getElementById("userUserRole").disabled = false;
    $("#userModalTittle").text("Tambah user");
    $("#userModal").modal("show");
}
/*
============================================================================
=================                edit modal              ===================
============================================================================
**/
function editUserInitiation(data) {
     formUserSubmitType = "PUT";
     $("#userUsername").val(data.username);
     document.getElementById("userUsername").disabled = false;
     document.getElementById("userUsername").readOnly = true;
     $("#userPersonId").val(data.personId);
     document.getElementById("userPersonId").disabled = false;
     $("#userName").val(data.name);
     document.getElementById("userName").disabled = false;
     $("#userUserRole").val(data.userRole);
     $("#userUserRole").trigger("change");
     document.getElementById("userUserRole").disabled = false;
     $("#userModalTittle").text("Ubah user " + data.username);
     $("#userModal").modal("show");
}
/*
============================================================================
=================               Submit operation         ===================
============================================================================
**/
function submitUserForm(){
    if (formInputUser.valid()) {
        var unIndexed_array = formInputUser.serializeArray();
        var indexed_array = {};
        var form = {};
        $.map(unIndexed_array, function (n) {
            indexed_array[n["name"]] = n["value"];
        });
        userUsernameVar = indexed_array.userUsername;
        form = {
            username: indexed_array.userUsername,
            personId: indexed_array.userPersonId,
            name: indexed_array.userName,
            userRole: indexed_array.userUserRole
        };
        var value = JSON.stringify(form);
        $.ajax({
            url: "/userApp",
            type: formUserSubmitType,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            data: value,
            success: function () {
                Swal.fire({
                    icon: "success",
                    title: "Success",
                    text: getUserOperationMessage()
                }).then(function () {
                    userUsernameVar = "";
                    $("#userModal").modal("hide");
                    reloadUserList();
                    window.parent.postMessage({message: "to_top"}, window.location.origin);
                });
            },
            error: function (data) {
                Swal.fire({
                    icon: "error",
                    title: data.responseJSON.error,
                    text: data.responseJSON.message
                });
            }
        });
    } else {
        Swal.fire({
            icon: "warning",
            title: "Warning",
            text: "Mohon lengkapi data yang dibutuhkan"
        });
    }
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
}
function getUserOperationMessage() {
    if (formUserSubmitType == "POST") {
        return "User " + userUsernameVar + " berhasil ditambahkan";
    } else {
        return "User diubah ke " + userUsernameVar;
    }
}
/*
============================================================================
=================                delete process          ===================
============================================================================
**/
function deleteUser(dataUser) {
    userUsernameVar = "";
    Swal.fire({
        title: "Anda yakin akan menghapus user " + dataUser.username,
        icon: "warning",
        showCancelButton: true,
        dangerMode: true
    }).then(function (result) {
        if (result.isConfirmed) {
          $.ajax({
              url: "/userApp?username=" + dataUser.username,
              type: "DELETE",
              dataType: "json",
              contentType: "application/json; charset=utf-8",
              success: function (data) {
                  Swal.fire({
                      title: "User " + dataUser.username + " berhasil dihapus",
                      icon: "success"
                  });
                  reloadUserList();
              },
              error: function (data) {
                  Swal.close();
                  if (data.status === 200 || data.status === 201) {
                      Swal.fire({
                          icon: "success",
                          title: "Success",
                          text: "User " + dataUser.username + " berhasil dihapus"
                      }).then(function () {
                          reloadUserList();
                      });
                  } else {
                      Swal.fire({
                          icon: "error",
                          title: data.responseJSON.status,
                          text: data.responseJSON.message
                      });
                  }
              }
          });
        }
  });
}