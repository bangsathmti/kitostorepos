"use strict";
var unitMeasurementInputForm = $("#unitMeasurementForm");
var unitMeasurementSubmitType = "";
var unitMeasurementNameVar = "";
/*
============================================================================
=================                Initiation              ===================
============================================================================
**/
function setUnitMeasurementMenuContent() {
    unitMeasurementInputForm = $("#unitMeasurementForm");
    unitMeasurementSubmitType = "";
    unitMeasurementNameVar = "";
    unitMeasurementInputForm.validate({
        rules: {
            unitMeasurementNameInputField: {required: true}
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("is-invalid");
        }
    });
    reloadUnitMeasurementList();
}
function reloadUnitMeasurementList() {
    $("#unitMeasurementList").DataTable().clear().destroy();
    var table = $("#unitMeasurementList").DataTable({
        serverMethod: "GET",
        order:[],
        language: {
            processing: '<img src="/dist/img/logokito.png">'
        },
        ajax: {
            url: "/unitMeasurement",
            dataSrc: ""
        },
        columns: [
            {data: "id", visible: false, searchable: false},
            {data: "name"},
            {data: "description"},
            {
                mRender: function () {
                    return `<div align="center"><a id="unitMeasurementEditTrigger" href="javascript:void(0);" title="Ubah ukuran"><i class="fas fa-edit fa-fw mr-1 edit-data-grid"></i></a><a id="unitMeasurementDeleteTrigger" href="javascript:void(0);" title="Hapus ukuran"><i class="fas fa-trash fa-fw text-red"></i></a></div>`;
                }, orderable: false, searchable: false
            }
        ]
    });
    $("#unitMeasurementList").find("tbody").off("click", "#unitMeasurementEditTrigger");
    $("#unitMeasurementList").find("tbody").off("click", "#unitMeasurementDeleteTrigger");
    $("#unitMeasurementList tbody").on("click", "#unitMeasurementEditTrigger", function () {
        var data = table.row($(this).parents("tr")).data();
        editUnitMeasurementInitiation(data);
    });
    $("#unitMeasurementList tbody").on("click", "#unitMeasurementDeleteTrigger", function () {
        var data = table.row($(this).parents("tr")).data();
        deleteUnitMeasurement(data);
    });
}
/*
============================================================================
=================                Cancel modal            ===================
============================================================================
**/
function cancelUnitMeasurementOperation(){
    unitMeasurementSubmitType = "";
    $("#unitMeasurementModal").modal("hide");
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
}
/*
============================================================================
=================                add modal               ===================
============================================================================
**/
function addUnitMeasurementInitiation() {
    unitMeasurementSubmitType = "POST";
    unitMeasurementInputForm[0].reset();
    document.getElementById("unitMeasurementIdInputField").disabled = true;
    document.getElementById("unitMeasurementNameInputField").disabled = false;
    document.getElementById("unitMeasurementDescriptionInputField").disabled = false;
    $("#unitMeasurementModalTitle").text("Tambah ukuran");
    $("#unitMeasurementModal").modal("show");
}
/*
============================================================================
=================                edit modal              ===================
============================================================================
**/
function editUnitMeasurementInitiation(data) {
     unitMeasurementSubmitType = "PUT";
     $("#unitMeasurementIdInputField").val(data.id);
     document.getElementById("unitMeasurementIdInputField").disabled = false;
     $("#unitMeasurementNameInputField").val(data.name);
     document.getElementById("unitMeasurementNameInputField").disabled = false;
     $("#unitMeasurementDescriptionInputField").val(data.description);
     document.getElementById("unitMeasurementDescriptionInputField").disabled = false;
     $("#unitMeasurementModalTitle").text("Ubah ukuran " + data.name);
     $("#unitMeasurementModal").modal("show");
}
/*
============================================================================
=================               Submit operation         ===================
============================================================================
**/
function submitUnitMeasurementForm(){
    if (unitMeasurementInputForm.valid()) {
        var unIndexed_array = unitMeasurementInputForm.serializeArray();
        var indexed_array = {};
        var form = {};
        $.map(unIndexed_array, function (n) {
            indexed_array[n["name"]] = n["value"];
        });
        unitMeasurementNameVar = indexed_array.unitMeasurementNameInputField;
        if (unitMeasurementSubmitType == "POST") {
            form = {
                name: indexed_array.unitMeasurementNameInputField,
                description: indexed_array.unitMeasurementDescriptionInputField
            };
        } else {
            form = {
                id: indexed_array.unitMeasurementIdInputField,
                name: indexed_array.unitMeasurementNameInputField,
                description: indexed_array.unitMeasurementDescriptionInputField
            };
        }
        var value = JSON.stringify(form);
        $.ajax({
            url: "/unitMeasurement",
            type: unitMeasurementSubmitType,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            data: value,
            success: function () {
                Swal.fire({
                    icon: "success",
                    title: "Success",
                    text: getUnitMeasurementOperationMessage()
                }).then(function () {
                    unitMeasurementNameVar = "";
                    $("#unitMeasurementModal").modal("hide");
                    reloadUnitMeasurementList();
                    window.parent.postMessage({message: "to_top"}, window.location.origin);
                });
            },
            error: function (data) {
                Swal.fire({
                    icon: "error",
                    title: data.responseJSON.error,
                    text: data.responseJSON.message
                });
            }
        });
    } else {
        Swal.fire({
            icon: "warning",
            title: "Warning",
            text: "Mohon lengkapi data yang dibutuhkan"
        });
    }
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
}
function getUnitMeasurementOperationMessage() {
    if (unitMeasurementSubmitType == "POST") {
        return "Ukuran " + unitMeasurementNameVar + " berhasil ditambahkan";
    } else {
        return "Ukuran diubah ke " + unitMeasurementNameVar;
    }
}
/*
============================================================================
=================                delete process          ===================
============================================================================
**/
function deleteUnitMeasurement(dataUnitMeasurement) {
    unitMeasurementNameVar = "";
    Swal.fire({
        title: "Anda yakin akan menghapus ukuran " + dataUnitMeasurement.name,
        icon: "warning",
        showCancelButton: true,
        dangerMode: true
    }).then(function (result) {
        if (result.isConfirmed) {
          $.ajax({
              url: "/unitMeasurement?id=" + dataUnitMeasurement.id,
              type: "DELETE",
              dataType: "json",
              contentType: "application/json; charset=utf-8",
              success: function (data) {
                  Swal.fire({
                      title: "Ukuran " + dataUnitMeasurement.name + " berhasil dihapus",
                      icon: "success"
                  });
                  reloadUnitMeasurementList();
              },
              error: function (data) {
                  Swal.close();
                  if (data.status === 200 || data.status === 201) {
                      Swal.fire({
                          icon: "success",
                          title: "Success",
                          text: "Ukuran " + dataUnitMeasurement.name + " berhasil dihapus"
                      }).then(function () {
                          reloadUnitMeasurementList();
                      });
                  } else {
                      Swal.fire({
                          icon: "error",
                          title: data.responseJSON.status,
                          text: data.responseJSON.message
                      });
                  }
              }
          });
        }
  });
}