"use strict";
var formInputSupplier = $("#formSupplier");
var formSupplierSubmitType = "";
var supplierNameVar = "";
/*
============================================================================
=================                Initiation              ===================
============================================================================
**/
function setSupplierMenuContent() {
    formInputSupplier = $("#formSupplier");
    formSupplierSubmitType = "";
    supplierNameVar = "";
    formInputSupplier.validate({
        rules: {
            name: {required: true},
            contact: {required: true}
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("is-invalid");
        }
    });
    reloadSupplierList();
}
function reloadSupplierList() {
    $("#supplierList").DataTable().clear().destroy();
    var table = $("#supplierList").DataTable({
        serverMethod: "GET",
        order:[],
        language: {
            processing: '<img src="/dist/img/logokito.png">'
        },
        ajax: {
            url: "/supplier",
            dataSrc: ""
        },
        columns: [
            {data: "id", visible: false, searchable: false},
            {data: "name"},
            {data: "contact"},
            {data: "address"},
            {
                mRender: function () {
                    return `<div align="center"><a id="editSupplierFunc" href="javascript:void(0);" title="Ubah supplier"><i class="fas fa-edit fa-fw mr-1 edit-data-grid"></i></a><a id="deleteSupplierFunc" href="javascript:void(0);" title="Hapus supplier"><i class="fas fa-trash fa-fw text-red"></i></a></div>`;
                }, orderable: false, searchable: false
            }
        ]
    });
    $("#supplierList").find("tbody").off("click", "#editSupplierFunc");
    $("#supplierList").find("tbody").off("click", "#deleteSupplierFunc");
    $("#supplierList tbody").on("click", "#editSupplierFunc", function () {
        var data = table.row($(this).parents("tr")).data();
        editSupplierInitiation(data);
    });
    $("#supplierList tbody").on("click", "#deleteSupplierFunc", function () {
        var data = table.row($(this).parents("tr")).data();
        deleteSupplier(data);
    });
}
/*
============================================================================
=================                Cancel modal            ===================
============================================================================
**/
function cancelSupplierOperation(){
    formSupplierSubmitType = "";
    $("#supplierModal").modal("hide");
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
}
/*
============================================================================
=================                add modal               ===================
============================================================================
**/
function addSupplierInitiation() {
    formSupplierSubmitType = "POST";
    formInputSupplier[0].reset();
    document.getElementById("supplierIdInputFiled").disabled = true;
    document.getElementById("supplierNameInputFiled").disabled = false;
    document.getElementById("supplierContactInputFiled").disabled = false;
    document.getElementById("supplierAddressInputFiled").disabled = false;
    $("#supplierModalTitle").text("Tambah supplier");
    $("#supplierModal").modal("show");
}
/*
============================================================================
=================                edit modal              ===================
============================================================================
**/
function editSupplierInitiation(data) {
     formSupplierSubmitType = "PUT";
     $("#supplierIdInputFiled").val(data.id);
     document.getElementById("supplierIdInputFiled").disabled = false;
     $("#supplierNameInputFiled").val(data.name);
     document.getElementById("supplierNameInputFiled").disabled = false;
     $("#supplierContactInputFiled").val(data.contact);
     document.getElementById("supplierContactInputFiled").disabled = false;
     $("#supplierAddressInputFiled").val(data.address);
     document.getElementById("supplierAddressInputFiled").disabled = false;
     $("#supplierModalTitle").text("Ubah supplier " + data.name);
     $("#supplierModal").modal("show");
}
/*
============================================================================
=================               Submit operation         ===================
============================================================================
**/
function submitSupplierForm(){
    if (formInputSupplier.valid()) {
        var unIndexed_array = formInputSupplier.serializeArray();
        var indexed_array = {};
        var form = {};
        $.map(unIndexed_array, function (n) {
            indexed_array[n["name"]] = n["value"];
        });
        supplierNameVar = indexed_array.supplierNameInputFiled;
        if (formSupplierSubmitType == "POST") {
            form = {
                name: indexed_array.supplierNameInputFiled,
                contact: indexed_array.supplierContactInputFiled,
                address: indexed_array.supplierAddressInputFiled
            };
        } else {
            form = {
                id: indexed_array.supplierIdInputFiled,
                name: indexed_array.supplierNameInputFiled,
                contact: indexed_array.supplierContactInputFiled,
                address: indexed_array.supplierAddressInputFiled
            };
        }
        var value = JSON.stringify(form);
        $.ajax({
            url: "/supplier",
            type: formSupplierSubmitType,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            data: value,
            success: function () {
                Swal.fire({
                    icon: "success",
                    title: "Success",
                    text: getSupplierOperationMessage()
                }).then(function () {
                    supplierNameVar = "";
                    $("#supplierModal").modal("hide");
                    reloadSupplierList();
                    window.parent.postMessage({message: "to_top"}, window.location.origin);
                });
            },
            error: function (data) {
                Swal.fire({
                    icon: "error",
                    title: data.responseJSON.error,
                    text: data.responseJSON.message
                });
            }
        });
    } else {
        Swal.fire({
            icon: "warning",
            title: "Warning",
            text: "Mohon lengkapi data yang dibutuhkan"
        });
    }
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
}
function getSupplierOperationMessage() {
    if (formSupplierSubmitType == "POST") {
        return "Supplier " + supplierNameVar + " berhasil ditambahkan";
    } else {
        return "Supplier diubah ke " + supplierNameVar;
    }
}
/*
============================================================================
=================                delete process          ===================
============================================================================
**/
function deleteSupplier(dataSupplier) {
    supplierNameVar = "";
    Swal.fire({
        title: "Anda yakin akan menghapus supplier " + dataSupplier.name,
        icon: "warning",
        showCancelButton: true,
        dangerMode: true
    }).then(function (result) {
        if (result.isConfirmed) {
          $.ajax({
              url: "/supplier?id=" + dataSupplier.id,
              type: "DELETE",
              dataType: "json",
              contentType: "application/json; charset=utf-8",
              success: function (data) {
                  Swal.fire({
                      title: "Supplier " + dataSupplier.name + " berhasil dihapus",
                      icon: "success"
                  });
                  reloadSupplierList();
              },
              error: function (data) {
                  Swal.close();
                  if (data.status === 200 || data.status === 201) {
                      Swal.fire({
                          icon: "success",
                          title: "Success",
                          text: "Supplier " + dataSupplier.name + " berhasil dihapus"
                      }).then(function () {
                          reloadSupplierList();
                      });
                  } else {
                      Swal.fire({
                          icon: "error",
                          title: data.responseJSON.status,
                          text: data.responseJSON.message
                      });
                  }
              }
          });
        }
  });
}