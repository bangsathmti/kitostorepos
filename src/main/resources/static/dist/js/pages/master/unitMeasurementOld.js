$(function () {
    "use strict";
    var formInput = $("#formRequestAdd");
    var jenisSubmit = "";
    var unitMeasurementName = "";
    formInput.validate({
        rules: {
            name: {required: true}
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("is-invalid");
        }
    });
    function setDataTable() {
        $('#masterUnitMeasurement').addClass("active");
        $("#unitMeasurementList").DataTable().clear().destroy();
        var table = $("#unitMeasurementList").DataTable({
            serverMethod: "GET",
            ajax: {
                url: "/unitMeasurement",
                dataSrc: ""
            },
            columns: [
                {data: "id", visible: false, searchable: false},
                {data: "name"},
                {data: "description"},
                {
                    mRender: function () {
                        return `<div align="center">
                      <a id="editFunc" href="javascript:void(0);" title="Ubah ukuran"><i class="fas fa-edit fa-fw mr-1 edit-data-grid"></i></a>
                      <a id="deleteFunc" href="javascript:void(0);" title="Hapus ukuran"><i class="fas fa-trash fa-fw text-red"></i></a>
                    </div>`;
                    }, orderable: false, searchable: false
                }
            ]
        });
        $("#unitMeasurementList").find("tbody").off("click", "#editFunc");
        $("#unitMeasurementList").find("tbody").off("click", "#deleteFunc");
        $("#unitMeasurementList tbody").on("click", "#editFunc", function () {
            var data = table.row($(this).parents("tr")).data();
            setValueEdit(data);
        });
        $("#unitMeasurementList tbody").on("click", "#deleteFunc", function () {
            var data = table.row($(this).parents("tr")).data();
            deleteFunc(data);
        });
    }
    setDataTable();
    $("#addButton").click(function () {
        addForm();
        $(".modal-title").text("Tambah ukuran");
        $("#modal_add").modal("show");
    });
    function addForm() {
        jenisSubmit = "POST";
        formInput[0].reset();
        document.getElementById("unitMeasurementId").disabled = true;
        document.getElementById("name").disabled = false;
        document.getElementById("description").disabled = false;
    }
    $("#backAddButton").click(function () {
        jenisSubmit = "";
        setDataTable();
        $("#modal_add").modal("hide");
        parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
    });
    $("#submitAddButton").click(function () {
        if (formInput.valid()) {
            submitForm();
        } else {
            Swal.fire({
                icon: "warning",
                title: "Warning",
                text: "Mohon lengkapi data yang dibutuhkan"
            });
        }
        parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
    });
    function setValueEdit(data) {
        jenisSubmit = "PUT";
        $("#unitMeasurementId").val(data.id);
        document.getElementById("unitMeasurementId").disabled = false;
        $("#name").val(data.name);
        document.getElementById("name").disabled = false;
        $("#description").val(data.description);
        document.getElementById("description").disabled = false;
        $(".modal-title").text("Ubah satuan " + data.name);
        $("#modal_add").modal("show");
    }
    function deleteFunc(unitMeasurement) {
        unitMeasurementName = "";
        Swal.fire({
            title: "Anda yakin akan menghapus satuan " + unitMeasurement.name,
            icon: "warning",
            showCancelButton: true,
            dangerMode: true
        }).then(function (result) {
            if (result.isConfirmed) {
                $.ajax({
                    url: "/unitMeasurement?id=" + unitMeasurement.id,
                    type: "DELETE",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        Swal.fire({
                            title: "Satuan " + unitMeasurement.name + " berhasil dihapus",
                            icon: "success"
                        });
                        setDataTable();
                    },
                    error: function (data) {
                        Swal.close();
                        if (data.status === 200 || data.status === 201) {
                            Swal.fire({
                                title: "Success",
                                text: "Satuan " + unitMeasurement.name + " berhasil dihapus"
                            }).then(function () {
                                setDataTable();
                            });
                        } else {
                            Swal.fire({
                                icon: "error",
                                title: "Error",
                                text: data.responseJSON.message
                            });
                        }
                    }
                });
            } else {
                Swal.fire("Cancel", "Satuan " + unitMeasurement.name + " batal dihapus");
            }
        });
    }
    function getMessage() {
        if (jenisSubmit == "POST") {
            return "Satuan " + unitMeasurementName + " berhasil ditambahkan";
        } else {
            return "Satuan diubah ke " + unitMeasurementName;
        }
    }
    function getFormValue($form) {
        var unindexed_array = $form.serializeArray();
        var indexed_array = {};
        var form = {};
        $.map(unindexed_array, function (n) {
            indexed_array[n["name"]] = n["value"];
        });
        unitMeasurementName = indexed_array.name;
        if (jenisSubmit == "POST") {
            form = {
                name: indexed_array.name,
                description: indexed_array.description
            };
            return JSON.stringify(form);
        } else {
            form = {
                id: indexed_array.unitMeasurementId,
                name: indexed_array.name,
                description: indexed_array.description
            };
            return JSON.stringify(form);
        }
    }
    function submitForm() {
        var value = getFormValue(formInput);
        $.ajax({
            url: "/unitMeasurement",
            type: jenisSubmit,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            data: value,
            success: function () {
                Swal.fire({
                    title: "Success",
                    text: getMessage()
                }).then(function () {
                    suksesSubmit();
                });
            },
            error: function (data) {
                Swal.fire({
                    icon: "error",
                    title: data.statusText,
                    text: data.responseJSON.message
                });
            }
        });
    }
    function suksesSubmit() {
        unitMeasurementName = "";
        $("#modal_add").modal("hide");
        setDataTable();
        window.parent.postMessage({message: "to_top"}, window.location.origin);
    }
});