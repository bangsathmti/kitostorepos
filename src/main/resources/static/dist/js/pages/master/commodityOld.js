$(function () {
    "use strict";
    var formInput = $("#formRequestAdd");
    var jenisSubmit = "";
    var commodityName = "";
    getComboCategory();
    getComboSalesMeasurement();
    getComboUnitMeasurement();
    formInput.validate({
        rules: {
            name: {required: true},
            category: {required: true},
            salesMeasurement: {required: true},
            unitMeasurement: {required: true},
            salesPrice: {required: true}
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("is-invalid");
        }
    });
    function setDataTable() {
        $('#masterCommodity').addClass("active");
        $("#commodityList").DataTable().clear().destroy();
        var table = $("#commodityList").DataTable({
            serverSide: true,
            processing: true,
            paging: true,
            pagingType: "full_numbers",
            searching: false,
            lengthChange: false,
            //            ordering: false,
            order: [],
            serverMethod: "GET",
            ajax: {
                url: "/commodity",
                dataSrc: function (data) {
                    data.recordsTotal = data.totalElements;
                    data.recordsFiltered = data.totalElements;
                    return data.content;
                },
                data: function (data) {
                    var page = 0;
                    if (data.start != 0) {
                        page = data.start / data.length;
                    }
                    const exUrl = "page=" + page;
                    return exUrl;
                }
            },
            columns: [
                {data: "id", visible: false, searchable: false},
                {data: "name", orderable: false},
                {data: "category", visible: false, searchable: false},
                {data: "categoryName", orderable: false},
                {data: "unitMeasurement", visible: false, searchable: false},
                {data: "unitMeasurementName", orderable: false},
                {data: "salesMeasurement", visible: false, searchable: false},
                {data: "salesMeasurementName", orderable: false},
                {data: "salesPrice", visible: false, searchable: false},
                {data: "salesPriceString", orderable: false},
                {data: "stockString", orderable: false, searchable: false},
                {data: "stockInCurrency", orderable: false, searchable: false},
                {data: "description", orderable: false},
                {
                    mRender: function () {
                        return `<div align="center">
                      <a id="editFunc" href="javascript:void(0);" title="Ubah commodity"><i class="fas fa-edit fa-fw mr-1 edit-data-grid"></i></a>
                      <a id="deleteFunc" href="javascript:void(0);" title="Hapus commodity"><i class="fas fa-trash fa-fw text-red"></i></a>
                      <a id="viewBarcode" href="javascript:void(0);" title="Lihat barcode" class="btn btn-app"><i class="fas fa-barcode"></i>View Barcode</a>
                    </div>`;
                    }, orderable: false, searchable: false
                }
            ]
        });
        $("#commodityList").find("tbody").off("click", "#editFunc");
        $("#commodityList").find("tbody").off("click", "#deleteFunc");
        $("#commodityList").find("tbody").off("click", "#viewBarcode");
        $("#commodityList tbody").on("click", "#editFunc", function () {
            var data = table.row($(this).parents("tr")).data();
            setValueEdit(data);
        });
        $("#commodityList tbody").on("click", "#viewBarcode", function () {
            var data = table.row($(this).parents("tr")).data();
            openBarcode(data);
        });
        $("#commodityList tbody").on("click", "#deleteFunc", function () {
            var data = table.row($(this).parents("tr")).data();
            deleteFunc(data);
        });
    }
    setDataTable();
    $("#addButton").click(function () {
        addForm();
        $(".modal-title").text("Tambah commodity");
        $("#modal_add").modal("show");
    });
    function addForm() {
        jenisSubmit = "POST";
        formInput[0].reset();
        document.getElementById("commodityId").disabled = false;
        document.getElementById("commodityId").readOnly = false;
        document.getElementById("name").disabled = false;
        document.getElementById("category").disabled = false;
        document.getElementById("unitMeasurement").disabled = false;
        document.getElementById("salesMeasurement").disabled = false;
        document.getElementById("salesPrice").disabled = false;
        document.getElementById("description").disabled = false;
    }
    $("#backAddButton").click(function () {
        jenisSubmit = "";
        setDataTable();
        $("#modal_add").modal("hide");
        parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
    });
    $("#submitAddButton").click(function () {
        if (formInput.valid()) {
            submitForm();
        } else {
            Swal.fire({
                icon: "warning",
                title: "Warning",
                text: "Mohon lengkapi data yang dibutuhkan"
            });
        }
        parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
    });
    function setValueEdit(data) {
        jenisSubmit = "PUT";
        $("#commodityId").val(data.id);
        document.getElementById("commodityId").disabled = false;
        document.getElementById("commodityId").readOnly = true;
        $("#name").val(data.name);
        document.getElementById("name").disabled = false;
        $("#category").val(data.category);
        $("#category").trigger("change");
        document.getElementById("category").disabled = false;
        $("#unitMeasurement").val(data.unitMeasurement);
        $("#unitMeasurement").trigger("change");
        document.getElementById("unitMeasurement").disabled = false;
        $("#salesMeasurement").val(data.salesMeasurement);
        $("#salesMeasurement").trigger("change");
        document.getElementById("salesMeasurement").disabled = false;
        $("#salesPrice").val(data.salesPrice);
        document.getElementById("salesPrice").disabled = false;
        $("#description").val(data.description);
        document.getElementById("description").disabled = false;
        $(".modal-title").text("Ubah commodity " + data.name);
        $("#modal_add").modal("show");
    }
    function deleteFunc(commodity) {
        commodityName = "";
        Swal.fire({
            title: "Anda yakin akan menghapus commodity " + commodity.name,
            icon: "warning",
            showCancelButton: true,
            dangerMode: true
        }).then(function (result) {
            if (result.isConfirmed) {
                $.ajax({
                    url: "/commodity?id=" + commodity.id,
                    type: "DELETE",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        Swal.fire({
                            title: "Commodity " + commodity.name + " berhasil dihapus",
                            icon: "success"
                        });
                        setDataTable();
                    },
                    error: function (data) {
                        Swal.close();
                        if (data.status === 200 || data.status === 201) {
                            Swal.fire({
                                title: "Success",
                                text: "Commodity " + commodity.name + " berhasil dihapus"
                            }).then(function () {
                                setDataTable();
                            });
                        } else {
                            Swal.fire({
                                icon: "error",
                                title: "Error",
                                text: data.responseJSON.message
                            });
                        }
                    }
                });
            } else {
                Swal.fire("Cancel", "Commodity " + commodity.name + " batal dihapus");
            }
        });
    }
    function getMessage() {
        if (jenisSubmit == "POST") {
            return "Commodity " + commodityName + " berhasil ditambahkan";
        } else {
            return "Commodity diubah ke " + commodityName;
        }
    }
    function getFormValue($form) {
        var unindexed_array = $form.serializeArray();
        var indexed_array = {};
        var form = {};
        $.map(unindexed_array, function (n) {
            indexed_array[n["name"]] = n["value"];
        });
        commodityName = indexed_array.name;
        form = {
            id: indexed_array.commodityId,
            name: indexed_array.name,
            category: indexed_array.category,
            unitMeasurement: indexed_array.unitMeasurement,
            salesMeasurement: indexed_array.salesMeasurement,
            salesPrice: indexed_array.salesPrice,
            description: indexed_array.description
        };
        return JSON.stringify(form);
    }
    function submitForm() {
        var value = getFormValue(formInput);
        $.ajax({
            url: "/commodity",
            type: jenisSubmit,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            data: value,
            success: function () {
                Swal.fire({
                    title: "Success",
                    text: getMessage()
                }).then(function () {
                    suksesSubmit();
                });
            },
            error: function (data) {
                Swal.fire({
                    icon: "error",
                    title: data.statusText,
                    text: data.responseJSON.message
                });
            }
        });
    }
    function suksesSubmit() {
        commodityName = "";
        $("#modal_add").modal("hide");
        setDataTable();
        window.parent.postMessage({message: "to_top"}, window.location.origin);
    }
    function openBarcode(commodity) {
        document.getElementById("barcodeFrame").src = "/commodity/barcode?commodityId=" + commodity.id + "&output=embed";
        $(".modalb-title").text(commodity.name);
        $("#modal_barcode").modal("show");
    }
    $("#closeViewButton").click(function () {
        setDataTable();
        $("#modal_barcode").modal("hide");
        parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
    });
});