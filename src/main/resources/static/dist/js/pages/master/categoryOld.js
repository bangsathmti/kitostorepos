$(function () {
    "use strict";
    var inputForm = $("#formRequestAdd");
    var httpMethod = "";
    var categoryName = "";
    inputForm.validate({
        rules: {
            name: {required: true}
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("is-invalid");
        }
    });
    function setDataTable() {
        $('#masterCategory').addClass("active");
        $("#categoryList").DataTable().clear().destroy();
        var table = $("#categoryList").DataTable({
            serverMethod: "GET",
            order:[1,"ASC"],
            ajax: {
                url: "/category",
                dataSrc: ""
            },
            columns: [
                {data: "id", visible: false, searchable: false, orderable: false},
                {data: "name"},
                {data: "description"},
                {
                    mRender: function () {
                        return `<div align="center">
                      <a id="editFunc" href="javascript:void(0);" title="Ubah category"><i class="fas fa-edit fa-fw mr-1 edit-data-grid"></i></a>
                      <a id="deleteFunc" href="javascript:void(0);" title="Hapus category"><i class="fas fa-trash fa-fw text-red"></i></a>
                    </div>`;
                    }, orderable: false, searchable: false
                }
            ]
        });
        $("#categoryList").find("tbody").off("click", "#editFunc");
        $("#categoryList").find("tbody").off("click", "#deleteFunc");
        $("#categoryList tbody").on("click", "#editFunc", function () {
            var data = table.row($(this).parents("tr")).data();
            setValueEdit(data);
        });
        $("#categoryList tbody").on("click", "#deleteFunc", function () {
            var data = table.row($(this).parents("tr")).data();
            deleteFunc(data);
        });
    }
    setDataTable();
    $("#addButton").click(function () {
        addForm();
        $(".modal-title").text("Tambah kategori baru");
        $("#modal_add").modal("show");
    });
    function addForm() {
        httpMethod = "POST";
        inputForm[0].reset();
        document.getElementById("categoryId").disabled = true;
        document.getElementById("name").disabled = false;
        document.getElementById("description").disabled = false;
    }
    $("#backAddButton").click(function () {
        httpMethod = "";
        setDataTable();
        $("#modal_add").modal("hide");
        parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
    });
    $("#submitAddButton").click(function () {
        if (inputForm.valid()) {
            submitForm();
        } else {
            Swal.fire({
                icon: "warning",
                title: "Warning",
                text: "Mohon lengkapi data yang dibutuhkan"
            });
        }
        parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
    });
    function setValueEdit(data) {
        httpMethod = "PUT";
        $("#categoryId").val(data.id);
        document.getElementById("categoryId").disabled = false;
        $("#name").val(data.name);
        document.getElementById("name").disabled = false;
        $("#description").val(data.description);
        document.getElementById("description").disabled = false;
        $(".modal-title").text("Ubah kategori " + data.name);
        $("#modal_add").modal("show");
    }
    function deleteFunc(category) {
        categoryName = "";
        Swal.fire({
            title: "Anda yakin akan menghapus kategori " + category.name,
            icon: "warning",
            showCancelButton: true,
            dangerMode: true
        }).then(function (result) {
            if (result.isConfirmed) {
                $.ajax({
                    url: "/category?id=" + category.id,
                    type: "DELETE",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        Swal.fire({
                            title: "Kategori " + category.name + " berhasil dihapus",
                            icon: "success"
                        });
                        setDataTable();
                    },
                    error: function (data) {
                        Swal.close();
                        if (data.status === 200 || data.status === 201) {
                            Swal.fire({
                                title: "Success",
                                text: "Kategori " + category.name + " berhasil dihapus"
                            }).then(function () {
                                setDataTable();
                            });
                        } else {
                            Swal.fire({
                                icon: "error",
                                title: "Error",
                                text: data.responseJSON.message
                            });
                        }
                    }
                });
            } else {
                Swal.fire("Cancel", "Kategory " + category.name + " batal dihapus");
            }
        });
    }
    function getMessage() {
        if (httpMethod == "POST") {
            return "Kategori " + categoryName + " berhasil ditambahkan";
        } else {
            return "Kategori diubah ke " + categoryName;
        }
    }
    function getFormValue($form) {
        var unindexed_array = $form.serializeArray();
        var indexed_array = {};
        var form = {};
        $.map(unindexed_array, function (n) {
            indexed_array[n["name"]] = n["value"];
        });
        categoryName = indexed_array.name;
        if (httpMethod == "POST") {
            form = {
                name: indexed_array.name,
                description: indexed_array.description
            };
            return JSON.stringify(form);
        } else {
            form = {
                id: indexed_array.categoryId,
                name: indexed_array.name,
                description: indexed_array.description
            };
            return JSON.stringify(form);
        }
    }
    function submitForm() {
        var value = getFormValue(inputForm);
        $.ajax({
            url: "/category",
            type: httpMethod,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            data: value,
            success: function () {
                Swal.fire({
                    title: "Success",
                    text: getMessage()
                }).then(function () {
                    suksesSubmit();
                });
            },
            error: function (data) {
                Swal.fire({
                    icon: "error",
                    title: data.statusText,
                    text: data.responseJSON.message
                });
            }
        });
    }
    function suksesSubmit() {
        categoryName = "";
        $("#modal_add").modal("hide");
        setDataTable();
        window.parent.postMessage({message: "to_top"}, window.location.origin);
    }
});