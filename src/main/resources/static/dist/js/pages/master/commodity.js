"use strict";
var commodityInputForm = $("#commodityForm");
var commoditySubmitType = "";
var commodityNameVar = "";
/*
============================================================================
=================                Initiation              ===================
============================================================================
**/
function setCommodityMenuContent() {
    commodityInputForm = $("#commodityForm");
    commoditySubmitType = "";
    commodityNameVar = "";
    getComboCategory();
    getComboUnitMeasurement();
    getComboSalesMeasurement();
    commodityInputForm.validate({
        rules: {
            commodityIdInputField: {required: true},
            commodityNameInputField: {required: true},
            commodityCategoryInputField: {required: true},
            commodityUnitMeasurementInputField: {required: true},
            commoditySalesMeasurementInputField: {required: true},
            commoditySalesPriceInputField: {required: true}
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("is-invalid");
        }
    });
    reloadCommodityList();
}
function reloadCommodityList() {
    $("#commodityList").DataTable().clear().destroy();
    var table = $("#commodityList").DataTable({
        serverSide: true,
        processing: true,
        paging: true,
        pagingType: "full_numbers",
        searching: false,
        lengthChange: false,
        order: [],
        serverMethod: "GET",
        language: {
            processing: '<img src="/dist/img/logokito.png">'
        },
        ajax: {
            url: "/commodity",
            dataSrc: function (data) {
                data.recordsTotal = data.totalElements;
                data.recordsFiltered = data.totalElements;
                return data.content;
            },
            data: function (data) {
                var page = 0;
                if (data.start != 0) {
                    page = data.start / data.length;
                }
                const exUrl = "page=" + page;
                return exUrl;
            }
        },
        columns: [
            {data: "id", visible: false, searchable: false},
            {data: "name", orderable: false},
            {data: "category", visible: false, searchable: false},
            {data: "categoryName", orderable: false},
            {data: "unitMeasurement", visible: false, searchable: false},
            {data: "unitMeasurementName", orderable: false},
            {data: "salesMeasurement", visible: false, searchable: false},
            {data: "salesMeasurementName", orderable: false},
            {data: "salesPrice", visible: false, searchable: false},
            {data: "salesPriceString", orderable: false},
            {data: "stockString", orderable: false, searchable: false},
            {data: "stockInCurrency", orderable: false, searchable: false},
            {data: "description", orderable: false},
            {
                mRender: function () {
                    return `<div align="center"><a id="commodityEditTrigger" href="javascript:void(0);" title="Ubah commodity"><i class="fas fa-edit fa-fw mr-1 edit-data-grid"></i></a><a id="commodityDeleteTrigger" href="javascript:void(0);" title="Hapus commodity"><i class="fas fa-trash fa-fw text-red"></i></a><a id="commodityViewBarcodeTrigger" href="javascript:void(0);" title="Lihat barcode" class="btn btn-app"><i class="fas fa-barcode"></i>View Barcode</a></div>`;
                }, orderable: false, searchable: false
            }
        ]
    });
    $("#commodityList").find("tbody").off("click", "#commodityEditTrigger");
    $("#commodityList").find("tbody").off("click", "#commodityDeleteTrigger");
    $("#commodityList").find("tbody").off("click", "#commodityViewBarcodeTrigger");
    $("#commodityList tbody").on("click", "#commodityEditTrigger", function () {
        var data = table.row($(this).parents("tr")).data();
        editCommodityInitiation(data);
    });
    $("#commodityList tbody").on("click", "#commodityViewBarcodeTrigger", function () {
        var data = table.row($(this).parents("tr")).data();
        openBarcode(data);
    });
    $("#commodityList tbody").on("click", "#commodityDeleteTrigger", function () {
        var data = table.row($(this).parents("tr")).data();
        deleteCommodity(data);
    });
}
/*
============================================================================
=================             open barcode modal         ===================
============================================================================
**/
function openBarcode(commodity) {
console.log(commodity);
    document.getElementById("barcodeCommodityFrame").src = "/commodity/barcode?commodityId=" + commodity.id + "&output=embed";
    $("#barcodeCommodityModalTitle").text(commodity.name);
    $("#barcodeCommodityModal").modal("show");
}
/*
============================================================================
=================            close barcode modal         ===================
============================================================================
**/
function closeViewBarcodeModal(){
    $("#barcodeCommodityModal").modal("hide");
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
}
/*
============================================================================
=================                Cancel modal            ===================
============================================================================
**/
function cancelCommodityOperation(){
    commoditySubmitType = "";
    $("#commodityModal").modal("hide");
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
}
/*
============================================================================
=================                add modal               ===================
============================================================================
**/
function addCommodityInitiation() {
    $.ajax({
        url: "/commodity/defaultBarcode",
        type: "get",
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            commoditySubmitType = "POST";
            commodityInputForm[0].reset();
            $("#commodityIdInputField").val(response.defaultBarcode);
            document.getElementById("commodityIdInputField").disabled = false;
            document.getElementById("commodityIdInputField").readOnly = false;
            document.getElementById("commodityNameInputField").disabled = false;
            document.getElementById("categoryInputField").disabled = false;
            document.getElementById("unitMeasurementInputField").disabled = false;
            document.getElementById("salesMeasurementInputField").disabled = false;
            document.getElementById("commoditySalesPriceInputField").disabled = false;
            document.getElementById("commodityDescriptionInputField").disabled = false;
            $("#commodityModalTitle").text("Tambah commodity");
            $("#commodityModal").modal("show");
        },
        error: function (response) {
            console.log(response);
        }
    });
}
/*
============================================================================
=================                edit modal              ===================
============================================================================
**/
function editCommodityInitiation(data) {
    commoditySubmitType = "PUT";
    $("#commodityIdInputField").val(data.id);
    document.getElementById("commodityIdInputField").disabled = false;
    document.getElementById("commodityIdInputField").readOnly = true;
    $("#commodityNameInputField").val(data.name);
    document.getElementById("commodityNameInputField").disabled = false;
    $("#categoryInputField").val(data.category);
    $("#categoryInputField").trigger("change");
    document.getElementById("categoryInputField").disabled = false;
    $("#unitMeasurementInputField").val(data.unitMeasurement);
    $("#unitMeasurementInputField").trigger("change");
    document.getElementById("unitMeasurementInputField").disabled = false;
    $("#salesMeasurementInputField").val(data.salesMeasurement);
    $("#salesMeasurementInputField").trigger("change");
    document.getElementById("salesMeasurementInputField").disabled = false;
    $("#commoditySalesPriceInputField").val(data.salesPrice);
    document.getElementById("commoditySalesPriceInputField").disabled = false;
    $("#commodityDescriptionInputField").val(data.description);
    document.getElementById("commodityDescriptionInputField").disabled = false;
    $("#commodityModalTitle").text("Ubah commodity " + data.name);
    $("#commodityModal").modal("show");
}
/*
============================================================================
=================               Submit operation         ===================
============================================================================
**/
function submitCommodityForm(){
    if (commodityInputForm.valid()) {
        var unIndexed_array = commodityInputForm.serializeArray();
        var indexed_array = {};
        var form = {};
        $.map(unIndexed_array, function (n) {
            indexed_array[n["name"]] = n["value"];
        });
        commodityNameVar = indexed_array.commodityNameInputField;
        form = {
            id: indexed_array.commodityIdInputField,
            name: indexed_array.commodityNameInputField,
            category: indexed_array.categoryInputField,
            unitMeasurement: indexed_array.unitMeasurementInputField,
            salesMeasurement: indexed_array.salesMeasurementInputField,
            salesPrice: indexed_array.commoditySalesPriceInputField,
            description: indexed_array.commodityDescriptionInputField
        };
        var value = JSON.stringify(form);
        $.ajax({
            url: "/commodity",
            type: commoditySubmitType,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            data: value,
            success: function () {
                Swal.fire({
                    icon: "success",
                    title: "Success",
                    text: getCommodityOperationMessage()
                }).then(function () {
                    commodityNameVar = "";
                    $("#commodityModal").modal("hide");
                    reloadCommodityList();
                    window.parent.postMessage({message: "to_top"}, window.location.origin);
                });
            },
            error: function (data) {
                Swal.fire({
                    icon: "error",
                    title: data.responseJSON.error,
                    text: data.responseJSON.message
                });
            }
        });
    } else {
        Swal.fire({
            icon: "warning",
            title: "Warning",
            text: "Mohon lengkapi data yang dibutuhkan"
        });
    }
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
}
function getCommodityOperationMessage() {
    if (commoditySubmitType == "POST") {
        return "Commodity " + commodityNameVar + " berhasil ditambahkan";
    } else {
        return "Commodity diubah ke " + commodityNameVar;
    }
}
/*
============================================================================
=================                delete process          ===================
============================================================================
**/
function deleteCommodity(dataCommodity) {
    commodityNameVar = "";
    Swal.fire({
        title: "Anda yakin akan menghapus commodity " + dataCommodity.name,
        icon: "warning",
        showCancelButton: true,
        dangerMode: true
    }).then(function (result) {
        if (result.isConfirmed) {
          $.ajax({
              url: "/commodity?id=" + dataCommodity.id,
              type: "DELETE",
              dataType: "json",
              contentType: "application/json; charset=utf-8",
              success: function (data) {
                  Swal.fire({
                      title: "Commodity " + dataCommodity.name + " berhasil dihapus",
                      icon: "success"
                  });
                  reloadCommodityList();
              },
              error: function (data) {
                  Swal.close();
                  if (data.status === 200 || data.status === 201) {
                      Swal.fire({
                          icon: "success",
                          title: "Success",
                          text: "Commodity " + dataCommodity.name + " berhasil dihapus"
                      }).then(function () {
                          reloadCommodityList();
                      });
                  } else {
                      Swal.fire({
                          icon: "error",
                          title: data.responseJSON.status,
                          text: data.responseJSON.message
                      });
                  }
              }
          });
        }
  });
}