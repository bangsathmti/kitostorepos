$(function () {
    "use strict";
    var formInput = $("#formRequestAdd");
    var jenisSubmit = "";
    var purchaseNumberVar = "";
    var date = new Date();
    var day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
    var month = (date.getMonth() + 1) < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1);
    var defaultValue = date.getFullYear() + "-" + month + "-" + day;
    getComboSupplier();
    formInput.validate({
        rules: {
            purchaseNumber: {required: true},
            purchaseDate: {required: true},
            supplier: {required: true}
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("is-invalid");
        }
    });
    function setDataTable() {
        $('#transactionPurchasing').addClass("active");
        $("#purchasingList").DataTable().clear().destroy();
        var table = $("#purchasingList").DataTable({
            serverSide: true,
            processing: true,
            paging: true,
            searching: false,
            lengthChange: false,
//            ordering: false,
            order: [],
            serverMethod: "GET",
            ajax: {
                url: "/purchasing",
                dataSrc: function (data) {
                    data.recordsTotal = data.totalElements;
                    data.recordsFiltered = data.totalElements;
                    return data.content;
                },
                data: function (data) {
                    var page = 0;
                    if (data.start != 0) {
                        page = data.start / data.length;
                    }
                    const exUrl = "page=" + page;
                    return exUrl;
                }
            },
            columns: [
                {data: "purchaseNumber", orderable: false},
                {data: "purchaseDate", orderable: false},
                {data: "supplier.id", visible: false, orderable: false},
                {data: "supplier.name", orderable: false},
                {data: "note", orderable: false},
                {data: "grandTotal", orderable: false},
                {
                    mRender: function () {
                        return `<div align="center">
                        <a id="viewDetail" href="javascript:void(0);" title="Lihat detail"><i class="fas fa-eye fa-fw mr-1 edit-data-grid text-green"></i></a>
                      <a id="editFunc" href="javascript:void(0);" title="Ubah data pembelian"><i class="fas fa-edit fa-fw mr-1 edit-data-grid"></i></a>
                    </div>`;
                    }, orderable: false
                }
            ]
        });
        $("#purchasingList").find("tbody").off("click", "#viewDetail");
        $("#purchasingList").find("tbody").off("click", "#editFunc");
        $("#purchasingList tbody").on("click", "#editFunc", function () {
            var data = table.row($(this).parents("tr")).data();
            setValueEdit(data);
        });
        $("#purchasingList tbody").on("click", "#viewDetail", function () {
            var data = table.row($(this).parents("tr")).data();
            viewDetail(data);
        });
    }
    setDataTable();
    function viewDetail(data) {
        location.replace("/transaction/purchasing/Detail?purchaseNumber=" + data.purchaseNumber);
    }
    $("#addButton").click(function () {
        addForm();
        $(".modal-title").text("Tambah data pembelian");
        $("#modal_add").modal("show");
    });
    function addForm() {
        jenisSubmit = "POST";
        formInput[0].reset();
        document.getElementById("purchaseNumber").disabled = false;
        document.getElementById("purchaseNumber").readOnly = false;
        document.getElementById("purchaseDate").disabled = false;
        $("#purchaseDate").val(defaultValue);
        document.getElementById("supplier").disabled = false;
        document.getElementById("note").disabled = false;
    }
    $("#backAddButton").click(function () {
        jenisSubmit = "";
        setDataTable();
        $("#modal_add").modal("hide");
        parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
    });
    $("#submitAddButton").click(function () {
        if (formInput.valid()) {
            submitForm();
        } else {
            Swal.fire({
                icon: "warning",
                title: "Warning",
                text: "Mohon lengkapi data yang dibutuhkan"
            });
        }
        parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
    });
    function setValueEdit(data) {
        jenisSubmit = "PUT";
        $("#purchaseNumber").val(data.purchaseNumber);
        document.getElementById("purchaseNumber").disabled = false;
        document.getElementById("purchaseNumber").readOnly = true;
        $("#purchaseDate").val(data.purchaseDate);
        document.getElementById("purchaseDate").disabled = false;
        $("#supplier").val(data.supplier.id);
        $("#supplier").trigger("change");
        document.getElementById("supplier").disabled = false;
        $("#note").val(data.note);
        document.getElementById("note").disabled = false;
        $(".modal-title").text("Ubah data pembelian " + data.purchaseNumber);
        $("#modal_add").modal("show");
    }
    function getMessage() {
        if (jenisSubmit == "POST") {
            return "Pembelian dengan nota " + purchaseNumberVar + " berhasil ditambahkan";
        } else {
            return "Pembelian " + purchaseNumberVar + " berhasil diubah";
        }
    }
    function getFormValue($form) {
        var unindexed_array = $form.serializeArray();
        var indexed_array = {};
        var form = {};
        $.map(unindexed_array, function (n) {
            indexed_array[n["name"]] = n["value"];
        });
        purchaseNumberVar = indexed_array.purchaseNumber;
        form = {
            purchaseNumber: indexed_array.purchaseNumber,
            purchaseDate: indexed_array.purchaseDate,
            supplierId: indexed_array.supplier,
            note: indexed_array.note
        };
        return JSON.stringify(form);
    }
    function submitForm() {
        var value = getFormValue(formInput);
        $.ajax({
            url: "/purchasing",
            type: jenisSubmit,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            data: value,
            success: function () {
                Swal.fire({
                    title: "Success",
                    text: getMessage()
                }).then(function () {
                    suksesSubmit();
                });
            },
            error: function (data) {
                Swal.fire({
                    icon: "error",
                    title: data.statusText,
                    text: data.responseJSON.message
                });
            }
        });
    }
    function suksesSubmit() {
        purchaseNumberVar = "";
        $("#modal_add").modal("hide");
        setDataTable();
        window.parent.postMessage({message: "to_top"}, window.location.origin);
    }
});