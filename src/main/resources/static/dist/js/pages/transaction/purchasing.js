"use strict";
var purchasingInputForm = $("#purchasingForm");
var purchasingSubmitType = "";
var purchaseNumberVar = "";
var date = new Date();
var day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
var month = (date.getMonth() + 1) < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1);
var defaultValue = date.getFullYear() + "-" + month + "-" + day;
/*
============================================================================
=================                Initiation              ===================
============================================================================
**/
function setPurchasingMenuContent() {
    purchasingInputForm = $("#purchasingForm");
    purchasingSubmitType = "";
    purchaseNumberVar = "";
    date = new Date();
    day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
    month = (date.getMonth() + 1) < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1);
    defaultValue = date.getFullYear() + "-" + month + "-" + day;
    getComboSupplier();
    purchasingInputForm.validate({
        rules: {
            purchasingPurchaseNumberInputField: {required: true},
            purchasingPurchaseDateInputField: {required: true},
            supplierInputField: {required: true}
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("is-invalid");
        }
    });
    reloadPurchasingList();
}
function reloadPurchasingList() {
    $("#purchasingList").DataTable().clear().destroy();
    var table = $("#purchasingList").DataTable({
        serverSide: true,
        processing: true,
        paging: true,
        pagingType: "full_numbers",
        searching: false,
        lengthChange: false,
        order: [],
        serverMethod: "GET",
        language: {
            processing: '<img src="/dist/img/logokito.png">'
        },
        ajax: {
            url: "/purchasing",
            dataSrc: function (data) {
                data.recordsTotal = data.totalElements;
                data.recordsFiltered = data.totalElements;
                return data.content;
            },
            data: function (data) {
                var page = 0;
                if (data.start != 0) {
                    page = data.start / data.length;
                }
                const exUrl = "page=" + page;
                return exUrl;
            }
        },
        columns: [
            {data: "purchaseNumber", orderable: false},
            {data: "purchaseDate", orderable: false},
            {data: "supplier.id", visible: false, orderable: false},
            {data: "supplier.name", orderable: false},
            {data: "note", orderable: false},
            {data: "grandTotal", orderable: false},
            {
                mRender: function () {
                    return `<div align="center"><a id="purchaseViewTrigger" href="javascript:void(0);" title="Lihat detail"><i class="fas fa-eye fa-fw mr-1 edit-data-grid text-green"></i></a><a id="purchaseEditTrigger" href="javascript:void(0);" title="Ubah data pembelian"><i class="fas fa-edit fa-fw mr-1 edit-data-grid"></i></a></div>`;
                }, orderable: false
            }
        ]
    });
    $("#purchasingList").find("tbody").off("click", "#purchaseEditTrigger");
    $("#purchasingList").find("tbody").off("click", "#purchaseViewTrigger");
    $("#purchasingList tbody").on("click", "#purchaseEditTrigger", function () {
        var data = table.row($(this).parents("tr")).data();
        editPurchasingInitiation(data);
    });
    $("#purchasingList tbody").on("click", "#purchaseViewTrigger", function () {
        var data = table.row($(this).parents("tr")).data();
        changeContentToPurchaseDetail(data);
    });
}
/*
============================================================================
=================                Cancel modal            ===================
============================================================================
**/
function cancelPurchasingOperation(){
    purchasingSubmitType = "";
    $("#purchasingModal").modal("hide");
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
}
/*
============================================================================
=================                add modal               ===================
============================================================================
**/
function addPurchasingInitiation() {
    purchasingSubmitType = "POST";
    purchasingInputForm[0].reset();
    document.getElementById("purchasingPurchaseNumberInputField").disabled = false;
    document.getElementById("purchasingPurchaseNumberInputField").readOnly = false;
    document.getElementById("purchasingPurchaseDateInputField").disabled = false;
    $("#purchasingPurchaseDateInputField").val(defaultValue);
    document.getElementById("supplierInputField").disabled = false;
    document.getElementById("purchasingNoteInputField").disabled = false;
    $("#purchasingModalTitle").text("Tambah data pembelian");
    $("#purchasingModal").modal("show");
}
/*
============================================================================
=================                edit modal              ===================
============================================================================
**/
function editPurchasingInitiation(data) {
     purchasingSubmitType = "PUT";
     $("#purchasingPurchaseNumberInputField").val(data.purchaseNumber);
     document.getElementById("purchasingPurchaseNumberInputField").disabled = false;
     document.getElementById("purchasingPurchaseNumberInputField").readOnly = true;
     $("#purchasingPurchaseDateInputField").val(data.purchaseDate);
     document.getElementById("purchasingPurchaseDateInputField").disabled = false;
     $("#supplierInputField").val(data.supplier.id);
     $("#supplierInputField").trigger("change");
     document.getElementById("supplierInputField").disabled = false;
     $("#purchasingNoteInputField").val(data.note);
     document.getElementById("purchasingNoteInputField").disabled = false;
     $("#purchasingModalTitle").text("Ubah data pembelian " + data.purchaseNumber);
     $("#purchasingModal").modal("show");
}
/*
============================================================================
=================               Submit operation         ===================
============================================================================
**/
function submitPurchasingForm(){
    if (purchasingInputForm.valid()) {
        var unIndexed_array = purchasingInputForm.serializeArray();
        var indexed_array = {};
        var form = {};
        $.map(unIndexed_array, function (n) {
            indexed_array[n["name"]] = n["value"];
        });
        purchaseNumberVar = indexed_array.purchasingPurchaseNumberInputField;
        form = {
            purchaseNumber: indexed_array.purchasingPurchaseNumberInputField,
            purchaseDate: indexed_array.purchasingPurchaseDateInputField,
            supplierId: indexed_array.supplierInputField,
            note: indexed_array.purchasingNoteInputField
        };
        var value = JSON.stringify(form);
        $.ajax({
            url: "/purchasing",
            type: purchasingSubmitType,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            data: value,
            success: function () {
                Swal.fire({
                    icon: "success",
                    title: "Success",
                    text: getPurchasingOperationMessage()
                }).then(function () {
                    purchaseNumberVar = "";
                    $("#purchasingModal").modal("hide");
                    reloadPurchasingList();
                    window.parent.postMessage({message: "to_top"}, window.location.origin);
                });
            },
            error: function (data) {
                Swal.fire({
                    icon: "error",
                    title: data.responseJSON.error,
                    text: data.responseJSON.message
                });
            }
        });
    } else {
        Swal.fire({
            icon: "warning",
            title: "Warning",
            text: "Mohon lengkapi data yang dibutuhkan"
        });
    }
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
}
function getPurchasingOperationMessage() {
    if (purchasingSubmitType == "POST") {
        return "Pembelian dengan nota " + purchaseNumberVar + " berhasil ditambahkan";
    } else {
        return "Pembelian " + purchaseNumberVar + " berhasil diubah";
    }
}
/*
============================================================================
=================                delete process          ===================
============================================================================
**/
function changeContentToPurchaseDetail(data){
    document.getElementById("purchaseNumberSession").innerHTML = data.purchaseNumber;
    changeContent('purchasingDetail');
}