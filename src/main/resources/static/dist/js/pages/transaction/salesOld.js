$(function () {
    "use strict";
    var formInput = $("#formRequestAdd");
    var jenisSubmit = "";
    var salesNumberVar = "";
    formInput.validate({
        rules: {
            userApp: {required: true},
            personalDiscount: {required: true}
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("is-invalid");
        }
    });
    function setDataTable() {
        $('#transactionSales').addClass("active");
        $("#salesList").DataTable().clear().destroy();
        var table = $("#salesList").DataTable({
            serverSide: true,
            processing: true,
            paging: true,
            searching: false,
            ordering:false,
            order: [],
            lengthChange: false,
            serverMethod: "GET",
            ajax: {
                url: "/sales",
                dataSrc: function (data) {
                    data.recordsTotal = data.totalElements;
                    data.recordsFiltered = data.totalElements;
                    return data.content;
                },
                data: function (data) {
                    var page = 0;
                    if (data.start != 0) {
                        page = data.start / data.length;
                    }
                    const exUrl = "page=" + page;
                    return exUrl;
                }
            },
            columns: [
                {data: "salesNumber", orderable: false},
                {data: "salesDate", orderable: false},
                {data: "cashierName", orderable: false},
                {data: "grandTotal", orderable: false},
                {data: "personalDiscount", visible: false, searchable: false},
                {data: "personalDiscountString", orderable: false},
                {data: "totalPayment", orderable: false},
                {
                    mRender: function () {
                        return `<div align="center">
                        <a id="viewDetail" href="javascript:void(0);" title="Lihat detail"><i class="fas fa-eye fa-fw mr-1 edit-data-grid text-green"></i></a>
                      <a id="editFunc" href="javascript:void(0);" title="Ubah data penjualan"><i class="fas fa-edit fa-fw mr-1 edit-data-grid"></i></a>
                    <a id="viewReceipt" href="javascript:void(0);" title="Lihat struk"><i class="fas fa-receipt fa-fw mr-1 edit-data-grid text-red"></i></a>
                    </div>`;
                    }, orderable: false, searchable: false
                }
            ]
        });
        $("#salesList").find("tbody").off("click", "#viewDetail");
        $("#salesList").find("tbody").off("click", "#editFunc");
        $("#salesList").find("tbody").off("click", "#viewReceipt");
        $("#salesList tbody").on("click", "#editFunc", function () {
            var data = table.row($(this).parents("tr")).data();
            setValueEdit(data);
        });
        $("#salesList tbody").on("click", "#viewDetail", function () {
            var data = table.row($(this).parents("tr")).data();
            viewDetail(data);
        });
        $("#salesList tbody").on("click", "#viewReceipt", function () {
             var data = table.row($(this).parents("tr")).data();
             window.location.href = "/transaction/cashier/struct?salesNumber=" + data.salesNumber+"&source=sales";
        });
    }
    setDataTable();
    function viewDetail(data) {
        location.replace("/transaction/sales/detail?salesNumber=" + data.salesNumber);
    }
    $("#addButton").click(function () {
        addForm();
        $(".modal-title").text("Tambah data penjualan");
        $("#modal_add").modal("show");
    });
    function addForm() {
        jenisSubmit = "POST";
        formInput[0].reset();
        document.getElementById("personalDiscount").disabled = false;
        document.getElementById("personalDiscount").defaultValue = 0;
    }
    $("#backAddButton").click(function () {
        jenisSubmit = "";
        setDataTable();
        $("#modal_add").modal("hide");
        parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
    });
    $("#submitAddButton").click(function () {
        if (formInput.valid()) {
            submitForm();
        } else {
            Swal.fire({
                icon: "warning",
                title: "Warning",
                text: "Mohon lengkapi data yang dibutuhkan"
            });
        }
        parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
    });
    function setValueEdit(data) {
        jenisSubmit = "PUT";
        $("#salesNumber").val(data.salesNumber);
        document.getElementById("salesNumber").disabled = false;
        $("#personalDiscount").val(data.personalDiscount);
        document.getElementById("personalDiscount").disabled = false;
        $(".modal-title").text("Ubah data penjualan " + data.salesNumber);
        $("#modal_add").modal("show");
    }
    function getMessage() {
        if (jenisSubmit == "POST") {
            return "Penjualan dengan nota " + salesNumberVar + " berhasil ditambahkan";
        } else {
            return "Penjualan " + salesNumberVar + " berhasil diubah";
        }
    }
    function getFormValue($form) {
        var unindexed_array = $form.serializeArray();
        var indexed_array = {};
        var form = {};
        $.map(unindexed_array, function (n) {
            indexed_array[n["name"]] = n["value"];
        });
        salesNumberVar = indexed_array.salesNumber;
        if (jenisSubmit == "POST") {
            form = {
                personalDiscount: indexed_array.personalDiscount
            };
        } else {
            form = {
                salesNumber: indexed_array.salesNumber,
                personalDiscount: indexed_array.personalDiscount
            };
        }
        return JSON.stringify(form);
    }
    function submitForm() {
        var value = getFormValue(formInput);
        $.ajax({
            url: "/sales",
            type: jenisSubmit,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            data: value,
            success: function (response) {
                if (jenisSubmit == "POST") {
                    salesNumberVar = response.salesNumber;
                }
                Swal.fire({
                    title: "Success",
                    text: getMessage()
                }).then(function () {
                    suksesSubmit();
                });
            },
            error: function (data) {
                Swal.fire({
                    icon: "error",
                    title: data.statusText,
                    text: data.responseJSON.message
                });
            }
        });
    }
    function suksesSubmit() {
        salesNumberVar = "";
        $("#modal_add").modal("hide");
        setDataTable();
        window.parent.postMessage({message: "to_top"}, window.location.origin);
    }
});