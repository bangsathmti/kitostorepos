$(function () {
    "use strict";
    const purchaseNumberId = $('#purchaseNumberId').text().toString();
    var formInput = $("#formRequestAdd");
    var formEdit = $("#formRequestEdit");
    var commodityPurchased = "";
    function getAvailableCommodity(def = null) {
        $.ajax({
            url: "/purchasing/availableCommodity?purchaseNumber=" + purchaseNumberId,
            type: "get",
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                var cmb = $('#commodity');
                cmb.empty();
                $.each(response, function (index, val) {
                    if (val.id != "0") {
                        cmb.append($('<option></option>').attr('value', val.id).text(val.name));
                    }
                });
                if (def != null) {
                    cmb.val(def);
                    cmb.trigger('change');
                }
            },
            error: function (response) {
                console.log(response);
            }
        });
    }
    getAvailableCommodity();
    formInput.validate({
        rules: {
            commodity: {required: true},
            amount: {required: true},
            unitPrice: {required: true}
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("is-invalid");
        }
    });
    formEdit.validate({
        rules: {
            commodityIdEdit: {required: true},
            amountEdit: {required: true},
            unitPriceEdit: {required: true}
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("is-invalid");
        }
    });
    function setHeader() {
        $('#transactionPurchasing').addClass("active");
        $.ajax({
            url: "/purchasing/id?purchaseNumber=" + purchaseNumberId,
            type: 'get',
            success: function (response) {
                $("#purchaseDate").text(response.purchaseDate);
                $("#supplierName").text(response.supplier.name);
                var myArray = response.note.split("\r\n");
                var outerHtmlNote = "";
                myArray.forEach(function (value, i) {
                    if (i == 0) {
                        outerHtmlNote += '<dd class="col-sm-8" id="note">' + value + '</dd>';
                    } else {
                        outerHtmlNote += '<dd class="col-sm-8 offset-sm-4">' + value + '</dd>'
                    }
                });
                document.getElementById("note").outerHTML = outerHtmlNote;
                $("#grandTotal").text(response.grandTotal);
                $("#purchasingDetailList").DataTable().clear().destroy();
                var table = $("#purchasingDetailList").DataTable({
                    data: response.purchasingDetails,
                    columns: [
                        {mRender: function () {
                                return '<div>' + purchaseNumberId + '</div>';
                            }, visible: false, searchable: false},
                        {data: "id.commodityId", visible: false, searchable: false},
                        {data: "commodity.name"},
                        {data: "commodity.category.name"},
                        {data: "amount", visible: false, searchable: false},
                        {data: "amountString"},
                        {data: "unitPrice", visible: false, searchable: false},
                        {data: "unitPriceString"},
                        {data: "total"},
                        {
                            mRender: function () {
                                return `<div align="center">
                      <a id="editFunc" href="javascript:void(0);" title="Ubah item"><i class="fas fa-edit fa-fw mr-1 edit-data-grid"></i></a>
                    </div>`;
                            }, orderable: false, searchable: false
                        }
                    ]
                });
                $("#purchasingDetailList").find("tbody").off("click", "#editFunc");
                $("#purchasingDetailList tbody").on("click", "#editFunc", function () {
                    var data = table.row($(this).parents("tr")).data();
                    setValueEdit(data);
                });
            }
        });
    }
    setHeader();
    $("#addButton").click(function () {
        addForm();
        $(".modal-title").text("Tambah item pembelian");
        $("#modal_add").modal("show");
    });
    function addForm() {
        formInput[0].reset();
        document.getElementById("commodity").disabled = false;
        document.getElementById("amount").disabled = false;
        document.getElementById("unitPrice").disabled = false;
    }
    $("#backAddButton").click(function () {
        setHeader();
        $("#modal_add").modal("hide");
        parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
    });
    $("#submitAddButton").click(function () {
        if (formInput.valid()) {
            submitForm();
        } else {
            Swal.fire({
                icon: "warning",
                title: "Warning",
                text: "Mohon lengkapi data yang dibutuhkan"
            });
        }
        parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
    });
    function setValueEdit(data) {
        $("#commodityIdEdit").val(data.id.commodityId);
        document.getElementById("commodityIdEdit").disabled = false;
        $("#commodityEdit").val(data.commodity.name);
        document.getElementById("commodityEdit").disabled = false;
        $("#amountEdit").val(data.amount);
        document.getElementById("amountEdit").disabled = false;
        $("#unitPriceEdit").val(data.unitPrice);
        document.getElementById("unitPriceEdit").disabled = false;
        $(".modaledit-title").text("Ubah item " + data.commodityName);
        $("#modal_edit").modal("show");
    }
    $("#backEditButton").click(function () {
        setHeader();
        $("#modal_edit").modal("hide");
        parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
    });
    $("#submitEditButton").click(function () {
        if (formEdit.valid()) {
            submitEditForm();
        } else {
            Swal.fire({
                icon: "warning",
                title: "Warning",
                text: "Mohon lengkapi data yang dibutuhkan"
            });
        }
        parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
    });
    function getMessage(jenisSubmit) {
        if (jenisSubmit == "POST") {
            return "Commodity " + commodityPurchased + " berhasil ditambahkan ke pembelian";
        } else {
            return "Commodity " + commodityPurchased + " berhasil diubah";
        }
    }
    function submitForm() {
        var unindexed_array = formInput.serializeArray();
        var indexed_array = {};
        var form = {};
        $.map(unindexed_array, function (n) {
            indexed_array[n["name"]] = n["value"];
        });
        var commodityCombo = document.getElementById("commodity");
        commodityPurchased = commodityCombo.options[commodityCombo.selectedIndex].innerHTML;
        form = {
            id: {
                purchaseNumber: purchaseNumberId,
                commodityId: indexed_array.commodity,
            },
            amount: indexed_array.amount,
            unitPrice: indexed_array.unitPrice
        };
        var value = JSON.stringify(form);
        $.ajax({
            url: "/purchasing/detail",
            type: "POST",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            data: value,
            success: function () {
                Swal.fire({
                    title: "Success",
                    text: getMessage("POST")
                }).then(function () {
                    commodityPurchased = "";
                    $("#modal_add").modal("hide");
                    setHeader();
                    getAvailableCommodity();
                    window.parent.postMessage({message: "to_top"}, window.location.origin);
                });
            },
            error: function (data) {
                Swal.fire({
                    icon: "error",
                    title: data.statusText,
                    text: data.responseJSON.message
                });
            }
        });
    }
    function submitEditForm() {
        var unindexed_array = formEdit.serializeArray();
        var indexed_array = {};
        var form = {};
        $.map(unindexed_array, function (n) {
            indexed_array[n["name"]] = n["value"];
        });
        commodityPurchased = indexed_array.commodityEdit;
        form = {
            id: {
                purchaseNumber: purchaseNumberId,
                commodityId: indexed_array.commodityIdEdit,
            },
            amount: indexed_array.amountEdit,
            unitPrice: indexed_array.unitPriceEdit
        };
        var value = JSON.stringify(form);
        $.ajax({
            url: "/purchasing/detail",
            type: "PUT",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            data: value,
            success: function () {
                Swal.fire({
                    title: "Success",
                    text: getMessage("PUT")
                }).then(function () {
                    commodityPurchased = "";
                    $("#modal_edit").modal("hide");
                    setHeader();
                    window.parent.postMessage({message: "to_top"}, window.location.origin);
                });
            },
            error: function (data) {
                Swal.fire({
                    icon: "error",
                    title: data.statusText,
                    text: data.responseJSON.message
                });
            }
        });
    }
});