"use strict";
function setReceiptData() {
    document.getElementById("receiptPrintButtonPrintPreview").focus();
    $.ajax({
        url: "/sales/salesNumber?salesNumber=" + $('#receiptNumberSession').text().toString(),
        type: "get",
        success: function (response) {
            $("#receiptNumberTablePrintPreview").text($('#receiptNumberSession').text().toString());
            $("#receiptSalesDatePrintPreview").text(response.salesDate);
            $("#receiptCashierNamePrintPreview").text(response.cashierName);
            var itemList = "";
            for (var i = 0; i < response.salesDetails.length; i++) {
                itemList += '<div class="col-sm-3 border-right border-bottom">' + response.salesDetails[i].commodity.name + '</div>';
                itemList += '<div class="col-sm-3 border-left border-right border-bottom d-flex align-items-center justify-content-center">' + response.salesDetails[i].amount + '</div>';
                itemList += '<div class="col-sm-3 border-left border-right border-bottom d-flex align-items-center justify-content-end">' + response.salesDetails[i].unitPriceString + '</div>';
                itemList += '<div class="col-sm-3 border-left border-bottom d-flex align-items-center justify-content-end">' + response.salesDetails[i].total + '</div>';
            }
            document.getElementById("receiptItemListPrintPreview").outerHTML = itemList;
            var grandTotal = "";
            if (response.personalDiscount == 0) {
                grandTotal += '<div class="col-sm-5"><b>Total bayar</b></div>';
                grandTotal += '<div class="col-sm-7 text-right">' + response.totalPayment + '</div>';
            } else {
                grandTotal += '<div class="col-sm-5"><b>Grand total</b></div>';
                grandTotal += '<div class="col-sm-7 text-right">' + response.grandTotal + '</div>';
                grandTotal += '<div class="col-sm-5"><b>Diskon</b></div>';
                grandTotal += '<div class="col-sm-7 text-right">' + response.personalDiscountString + '</div>';
                grandTotal += '<div class="col-sm-12"><b>===============================================</b></div>';
                grandTotal += '<div class="col-sm-5"><b>Total bayar</b></div>';
                grandTotal += '<div class="col-sm-7 text-right">' + response.totalPayment + '</div>';
            }
            document.getElementById("receiptGrandTotalPrintPreview").outerHTML = grandTotal;
        },
        error: function (data) {
            Swal.fire({
                icon: "error",
                title: data.statusText,
                text: data.responseJSON.message
            }).then(function () {
                history.back();
            });
        }
    });
}
function receiptChangeContent(){
    changeContent($('#menuBeforeReceipt').text().toString());
}
function printReceiptFromPrintPreview(){
    window.open("/transaction/cashier/printStruct?salesNumber=" + $('#receiptNumberSession').text().toString());
}