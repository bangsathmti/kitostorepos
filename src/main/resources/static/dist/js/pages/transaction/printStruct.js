$(function () {
    "use strict";
    const salesNumber = $('#salesNumber').text().toString();
    function setData() {
        $.ajax({
            url: "/sales/salesNumber?salesNumber=" + salesNumber,
            type: "get",
            success: function (response) {
                $("#salesNumberDesc").text(salesNumber);
                $("#salesDate").text(response.salesDate);
                $("#cashierName").text(response.cashierName);
                var itemList = '<table style="width: 100%;"><tr><td style="width: 35%; text-align: center;"><b>Commodity</b></td><td style="width: 15%; text-align: left;"><b>Jumlah</b></td>'
                        + '<td style="width: 25%; text-align: left;"><b>Harga</b></td><td style="width: 25%; text-align: center;"><b>Total</b></td></tr>';
                for (var i = 0; i < response.salesDetails.length; i++) {
                    itemList += '<tr>';
                    itemList += '<td>' + response.salesDetails[i].commodity.name + '</td>';
                    itemList += '<td>' + response.salesDetails[i].amount + '</td>';
                    itemList += '<td>' + response.salesDetails[i].unitPriceString + '</td>';
                    itemList += '<td style="text-align: right;">' + response.salesDetails[i].total + '</td>';
                    itemList += '</tr>';
                }
                itemList += '</table>';
                document.getElementById("itemList").outerHTML = itemList;
                var grandTotal = "";
                if (response.personalDiscount != 0) {
                    grandTotal += '<table style="width: 100%;"><tr>';
                    grandTotal += '<td style="width: 75%;"><b>Grand total</b></td>';
                    grandTotal += '<td style="width: 25%; text-align: right;">' + response.grandTotal + '</td></tr>';
                    grandTotal += '<tr><td><b>Diskon</b></td>';
                    grandTotal += '<td style="text-align: right;">' + response.personalDiscountString + '</td>';
                    grandTotal += '</tr></table>';
                    grandTotal += '<b>=======================================================</b>';

                }
                grandTotal += '<table style="width: 100%;"><tr>';
                grandTotal += '<td style="width: 75%;"><b>Total bayar</b></td>';
                grandTotal += '<td style="width: 25%; text-align: right;">' + response.totalPayment + '</td>';
                grandTotal += '</tr></table>';
                document.getElementById("grandTotal").outerHTML = grandTotal;
                window.print();
//                history.go(-2);
                window.open('', '_self', ''); window.close();
            },
            error: function (data) {
//                history.go(-2);
                window.open('', '_self', ''); window.close();
            }
        });
    }
    setData();
});