$(function () {
    "use strict";
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const source = urlParams.get('source')
    const salesNumber = $('#salesNumber').text().toString();
    function setData() {
        $.ajax({
            url: "/sales/salesNumber?salesNumber=" + salesNumber,
            type: "get",
            success: function (response) {
                $("#salesNumberDesc").text(salesNumber);
                $("#salesDate").text(response.salesDate);
                $("#cashierName").text(response.cashierName);
                var itemList = "";
                for (var i = 0; i < response.salesDetails.length; i++) {
                    itemList += '<div class="col-sm-3 border-right">' + response.salesDetails[i].commodity.name + '</div>';
                    itemList += '<div class="col-sm-3 border-left border-right">' + response.salesDetails[i].amount + '</div>';
                    itemList += '<div class="col-sm-3 border-left border-right text-left">' + response.salesDetails[i].unitPriceString + '</div>';
                    itemList += '<div class="col-sm-3 border-left text-right">' + response.salesDetails[i].total + '</div>';
                }
                document.getElementById("itemList").outerHTML = itemList;
                var grandTotal = "";
                if (response.personalDiscount == 0) {
                    grandTotal += '<div class="col-sm-5"><b>Total bayar</b></div>';
                    grandTotal += '<div class="col-sm-7 text-right">' + response.totalPayment + '</div>';
                } else {
                    grandTotal += '<div class="col-sm-5"><b>Grand total</b></div>';
                    grandTotal += '<div class="col-sm-7 text-right">' + response.grandTotal + '</div>';
                    grandTotal += '<div class="col-sm-5"><b>Diskon</b></div>';
                    grandTotal += '<div class="col-sm-7 text-right">' + response.personalDiscountString + '</div>';
                    grandTotal += '<div class="col-sm-12"><b>==================================================</b></div>';
                    grandTotal += '<div class="col-sm-5"><b>Total bayar</b></div>';
                    grandTotal += '<div class="col-sm-7 text-right">' + response.totalPayment + '</div>';
                }
                document.getElementById("grandTotal").outerHTML = grandTotal;
            },
            error: function (data) {
                Swal.fire({
                    icon: "error",
                    title: data.statusText,
                    text: data.responseJSON.message
                }).then(function () {
                    history.back();
                });
            }
        });
    }
    setData();
    $("#printBtn").click(function () {
        var prtContent = document.getElementById("struct");
        var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
        WinPrint.document.write(prtContent.innerHTML);
        WinPrint.document.close();
        WinPrint.focus();
        WinPrint.print();
        WinPrint.close();
    });
    $("#backButton").click(function () {
        if(source == "sales"){
            history.go(-1);
        }else{
            window.location.href = "/transaction/cashier";
        }
    });
});