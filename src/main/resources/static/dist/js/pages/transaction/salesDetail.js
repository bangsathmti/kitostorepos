"use strict";
var salesDetailSalesNumberSession = $('#receiptNumberSession').text().toString();
var formSalesAddDetailsInput = $("#formSalesRequestAddDetail");
var formSalesEditDetailsInput = $("#formSalesRequestEditDetail");
var commoditySaledVar = "";

function getAvailableCommodityForSalesDetails(def = null) {
    $.ajax({
        url: "/sales/availableCommodity?salesNumber=" + salesDetailSalesNumberSession,
        type: "get",
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            var cmb = $('#commoditySalesDetailInputField');
            cmb.empty();
            $.each(response, function (index, val) {
                if (val.id != "0") {
                    cmb.append($('<option></option>').attr('value', val.id).text(val.name));
                }
            });
            if (def != null) {
                cmb.val(def);
                cmb.trigger('change');
            }
        },
        error: function (response) {
            console.log(response);
        }
    });
}

function setSalesDetailMenuContent() {
    salesDetailSalesNumberSession = $('#receiptNumberSession').text().toString();
    formSalesAddDetailsInput = $("#formSalesRequestAddDetail");
    formSalesEditDetailsInput = $("#formSalesRequestEditDetail");
    commoditySaledVar = "";
    formSalesAddDetailsInput.validate({
        rules: {
            commoditySalesDetailInputField: {required: true},
            amountSalesDetailInputField: {required: true}
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("is-invalid");
        }
    });
    formSalesEditDetailsInput.validate({
        rules: {
            commodityIdEditSalesDetailInputField: {required: true},
            amountEditSalesDetailInputField: {required: true}
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("is-invalid");
        }
    });

    getAvailableCommodityForSalesDetails();
    reloadDataSalesDetailMenu();
}

function reloadDataSalesDetailMenu() {
    $.ajax({
        url: "/sales/id?salesNumber=" + salesDetailSalesNumberSession,
        type: 'get',
        success: function (response) {
            $("#salesNumberDetailSession").text(salesDetailSalesNumberSession);
            $("#salesNumberPurchaseDetailHeader").text(salesDetailSalesNumberSession);
            $("#salesNumberSalesDetailInfo").text(salesDetailSalesNumberSession);
            $("#salesDateSalesDetail").text(response.salesDate);
            $("#cashierNameSalesDetail").text(response.cashierName);
            $("#grandTotalSalesDetail").text(response.grandTotal);
            $("#personalDiscountSalesDetail").text(response.personalDiscountString);
            $("#totalPaymentSalesDetail").text(response.totalPayment);
            $("#salesDetailList").DataTable().clear().destroy();
            var table = $("#salesDetailList").DataTable({
                data: response.salesDetails,
                language: {
                    processing: '<img src="/dist/img/logokito.png">'
                },
                columns: [
                    {mRender: function () {
                            return '<div>' + salesDetailSalesNumberSession + '</div>';
                        }, visible: false, searchable: false},
                    {data: "id.commodityId", visible: false, searchable: false},
                    {data: "commodity.name"},
                    {data: "amount", visible: false, searchable: false},
                    {data: "amountString"},
                    {data: "unitPrice", visible: false, searchable: false},
                    {data: "unitPriceString"},
                    {data: "total"},
                    {
                        mRender: function () {
                            return `<div align="center">
                      <a id="editSalesDetailFunc" href="javascript:void(0);" title="Ubah item"><i class="fas fa-edit fa-fw mr-1 edit-data-grid"></i></a>
                    </div>`;
                        }, orderable: false, searchable: false
                    }
                ]
            });
            $("#salesDetailList").find("tbody").off("click", "#editSalesDetailFunc");
            $("#salesDetailList tbody").on("click", "#editSalesDetailFunc", function () {
                var data = table.row($(this).parents("tr")).data();
                setValueEditSalesDetail(data);
            });
        }
    });
}
function openReceiptFromSalesDetail() {
    changeContent('receipt');
}
function addSalesDetailItemData() {
    formSalesAddDetailsInput[0].reset();
    document.getElementById("commoditySalesDetailInputField").disabled = false;
    document.getElementById("amountSalesDetailInputField").disabled = false;
    $("#salesDetailAddModalTitle").text("Tambah item penjualan");
    $("#salesDetailAddModal").modal("show");
}

function cancelSalesDetailAddOperation() {
    $("#salesDetailAddModal").modal("hide");
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
}

function submitSalesDetailAddOperation() {

    var unindexed_array = formSalesAddDetailsInput.serializeArray();
    var indexed_array = {};
    var form = {};
    $.map(unindexed_array, function (n) {
        indexed_array[n["name"]] = n["value"];
    });
    var commodityCombo = document.getElementById("commoditySalesDetailInputField");
    commoditySaledVar = commodityCombo.options[commodityCombo.selectedIndex].innerHTML;
    form = {
        id: {
            salesNumber: salesDetailSalesNumberSession,
            commodityId: indexed_array.commoditySalesDetailInputField,
        },
        amount: indexed_array.amountSalesDetailInputField
    };
    var value = JSON.stringify(form);
    $.ajax({
        url: "/sales/detail",
        type: "POST",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        data: value,
        success: function () {
            Swal.fire({
                title: "Success",
                text: getMessageAddSalesDetail("POST")
            }).then(function () {
                commoditySaledVar = "";
                $("#salesDetailAddModal").modal("hide");
                reloadDataSalesDetailMenu();
                getAvailableCommodityForSalesDetails();
                window.parent.postMessage({message: "to_top"}, window.location.origin);
            });
        },
        error: function (data) {
            Swal.fire({
                icon: "error",
                title: data.responseJSON.error,
                text: data.responseJSON.message
            });
        }
    });
}

function getMessageAddSalesDetail(jenisSubmit) {
    if (jenisSubmit == "POST") {
        return "Commodity " + commoditySaledVar + " berhasil ditambahkan ke penjualan";
    } else {
        return "Commodity " + commoditySaledVar + " berhasil diubah";
    }
}

function setValueEditSalesDetail(data) {

    $("#commodityIdEditSalesDetailInputField").val(data.id.commodityId);
    document.getElementById("commodityIdEditSalesDetailInputField").disabled = false;
    $("#commodityNameEditSalesDetailInputField").val(data.commodity.name);
    document.getElementById("commodityNameEditSalesDetailInputField").disabled = false;
    $("#amountEditSalesDetailInputField").val(data.amount);
    document.getElementById("amountEditSalesDetailInputField").disabled = false;
    $("#salesDetailEditModalTitle").text("Ubah item " + data.commodityName);
    $("#salesDetailEditModal").modal("show");
}

function backEditSalesDetail() {
    $("#salesDetailEditModal").modal("hide");
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
}

function submitEditSalesDetail() {
    var unindexed_array = formSalesEditDetailsInput.serializeArray();
    var indexed_array = {};
    var form = {};
    $.map(unindexed_array, function (n) {
        indexed_array[n["name"]] = n["value"];
    });
    commoditySaledVar = indexed_array.commodityNameEditSalesDetailInputField;
    form = {
        id: {
            salesNumber: salesDetailSalesNumberSession,
            commodityId: indexed_array.commodityIdEditSalesDetailInputField,
        },
        amount: indexed_array.amountEditSalesDetailInputField
    };
    var value = JSON.stringify(form);
    $.ajax({
        url: "/sales/detail",
        type: "PUT",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        data: value,
        success: function () {
            Swal.fire({
                title: "Success",
                text: getMessageAddSalesDetail("PUT")
            }).then(function () {
                commoditySaledVar = "";
                $("#salesDetailEditModal").modal("hide");
                reloadDataSalesDetailMenu();
                window.parent.postMessage({message: "to_top"}, window.location.origin);
            });
        },
        error: function (data) {
            Swal.fire({
                icon: "error",
                title: data.responseJSON.error,
                text: data.responseJSON.message
            });
        }
    });
}
