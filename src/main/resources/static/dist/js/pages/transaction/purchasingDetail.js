"use strict";
var purchaseNumberIdPurchaseDetailSession = $('#purchaseNumberSession').text().toString();
var formAddPurchaseDetail = $("#unitMeasurementAddForm");
var formEditPurchaseDetail = $("#purchasingDetailEditForm");
var commodityPurchasedPurchaseDetailVar = "";
function getAvailableCommodity(def = null) {
    $.ajax({
        url: "/purchasing/availableCommodity?purchaseNumber=" + purchaseNumberIdPurchaseDetailSession,
        type: "get",
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            var cmb = $('#commodityPurchaseDetailAddInputField');
            cmb.empty();
            $.each(response, function (index, val) {
                if (val.id != "0") {
                    cmb.append($('<option></option>').attr('value', val.id).text(val.name));
                }
            });
            if (def != null) {
                cmb.val(def);
                cmb.trigger('change');
            }
        },
        error: function (response) {
            console.log(response);
        }
    });
}
/*
 ============================================================================
 =================                Initiation              ===================
 ============================================================================
 **/
function setPurchasingDetailMenuContent() {
    purchaseNumberIdPurchaseDetailSession = $('#purchaseNumberSession').text().toString();
    formAddPurchaseDetail = $("#purchasingDetailAddForm");
    formEditPurchaseDetail = $("#purchasingDetailEditForm");
    commodityPurchasedPurchaseDetailVar = "";
    getAvailableCommodity();
    formAddPurchaseDetail.validate({
        rules: {
            commodityPurchaseDetailAddInputField: {required: true},
            amountPurchaseDetailAddInputField: {required: true},
            unitPricePurchaseDetailAddInputField: {required: true}
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("is-invalid");
        }
    });
    formEditPurchaseDetail.validate({
        rules: {
            commodityIdPurchaseDetailEditInputField: {required: true},
            amountPurchaseDetailEditInputField: {required: true},
            unitPricePurchaseDetailEditInputField: {required: true}
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("is-invalid");
        }
    });
    reloadPurchasingDetailList();
}
function reloadPurchasingDetailList() {
    $.ajax({
        url: "/purchasing/id?purchaseNumber=" + purchaseNumberIdPurchaseDetailSession,
        type: 'get',
        success: function (response) {
            $("#purchaseNumberPurchaseDetailInfo").text(response.purchaseNumber);
            $("#purchaseNumberPurchaseDetailHeader").text(response.purchaseNumber);
            $("#purchaseNumberDetailSession").text(response.purchaseNumber);
            $("#purchaseDatePurchaseDetailInfo").text(response.purchaseDate);
            $("#supplierNamePurchaseDetailInfo").text(response.supplier.name);
            var myArray = response.note.split("\r\n");
            var outerHtmlNote = "";
            myArray.forEach(function (value, i) {
                if (i == 0) {
                    outerHtmlNote += '<dd class="col-sm-8" id="notePurchaseDetailInfo">' + value;
                } else {
                    outerHtmlNote += '<br>' + value;
                }
            });
            outerHtmlNote += '</dd>';
            document.getElementById("notePurchaseDetailInfo").outerHTML = outerHtmlNote;
            $("#grandTotalPurchaseDetailInfo").text(response.grandTotal);
            $("#purchasingDetailList").DataTable().clear().destroy();
            var table = $("#purchasingDetailList").DataTable({
                data: response.purchasingDetails,
                language: {
                    processing: '<img src="/dist/img/logokito.png">'
                },
                columns: [
                    {mRender: function () {
                            return '<div>' + purchaseNumberIdPurchaseDetailSession + '</div>';
                        }, visible: false, searchable: false},
                    {data: "id.commodityId", visible: false, searchable: false},
                    {data: "commodity.name"},
                    {data: "commodity.category.name"},
                    {data: "amount", visible: false, searchable: false},
                    {data: "amountString"},
                    {data: "unitPrice", visible: false, searchable: false},
                    {data: "unitPriceString"},
                    {data: "total"},
                    {
                        mRender: function () {
                            return `<div align="center"><a id="purchaseDetailEditTrigger" href="javascript:void(0);" title="Ubah item"><i class="fas fa-edit fa-fw mr-1 edit-data-grid"></i></a></div>`;
                        }, orderable: false, searchable: false
                    }
                ]
            });
            $("#purchasingDetailList").find("tbody").off("click", "#purchaseDetailEditTrigger");
            $("#purchasingDetailList tbody").on("click", "#purchaseDetailEditTrigger", function () {
                var data = table.row($(this).parents("tr")).data();
                purchaseDetailEditInitiation(data);
            });
        }
    });
}
/*
 ============================================================================
 =================                Cancel modal            ===================
 ============================================================================
 **/
function cancelPurchaseDetailAddOperation() {
    $("#purchaseDetailAddModal").modal("hide");
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
}
function cancelPurchaseDetailEditOperation() {
    $("#purchaseDetailEditModal").modal("hide");
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
}
/*
 ============================================================================
 =================                add modal               ===================
 ============================================================================
 **/
function addPurchaseDetailInitiation() {
    formAddPurchaseDetail[0].reset();
    document.getElementById("commodityPurchaseDetailAddInputField").disabled = false;
    document.getElementById("amountPurchaseDetailAddInputField").disabled = false;
    document.getElementById("unitPricePurchaseDetailAddInputField").disabled = false;
    $("#purchaseDetailAddModalTitle").text("Tambah item pembelian");
    $("#purchaseDetailAddModal").modal("show");
}
/*
 ============================================================================
 =================                edit modal              ===================
 ============================================================================
 **/
function purchaseDetailEditInitiation(data) {
    $("#commodityIdPurchaseDetailEditInputField").val(data.id.commodityId);
    document.getElementById("commodityIdPurchaseDetailEditInputField").disabled = false;
    $("#commodityNamePurchaseDetailEditInputField").val(data.commodity.name);
    document.getElementById("commodityNamePurchaseDetailEditInputField").disabled = false;
    $("#amountPurchaseDetailEditInputField").val(data.amount);
    document.getElementById("amountPurchaseDetailEditInputField").disabled = false;
    $("#unitPricePurchaseDetailEditInputField").val(data.unitPrice);
    document.getElementById("unitPricePurchaseDetailEditInputField").disabled = false;
    $("#purchaseDetailEditModalTitle").text("Ubah item " + data.commodityName);
    $("#purchaseDetailEditModal").modal("show");
}
/*
 ============================================================================
 =================            Submit add operation        ===================
 ============================================================================
 **/
function submitPurchaseDetailAddForm() {
    if (formAddPurchaseDetail.valid()) {
        var unIndexed_array = formAddPurchaseDetail.serializeArray();
        var indexed_array = {};
        var form = {};
        $.map(unIndexed_array, function (n) {
            indexed_array[n["name"]] = n["value"];
        });
        var commodityCombo = document.getElementById("commodityPurchaseDetailAddInputField");
        commodityPurchasedPurchaseDetailVar = commodityCombo.options[commodityCombo.selectedIndex].innerHTML;
        form = {
            id: {
                purchaseNumber: purchaseNumberIdPurchaseDetailSession,
                commodityId: indexed_array.commodityPurchaseDetailAddInputField,
            },
            amount: indexed_array.amountPurchaseDetailAddInputField,
            unitPrice: indexed_array.unitPricePurchaseDetailAddInputField
        };
        var value = JSON.stringify(form);
        $.ajax({
            url: "/purchasing/detail",
            type: "POST",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            data: value,
            success: function () {
                Swal.fire({
                    icon: "success",
                    title: "Success",
                    text: getPurchasingDetailMessage("POST")
                }).then(function () {
                    unitMeasurementNameVar = "";
                    $("#purchaseDetailAddModal").modal("hide");
                    reloadPurchasingDetailList();
                    window.parent.postMessage({message: "to_top"}, window.location.origin);
                });
            },
            error: function (data) {
                Swal.fire({
                    icon: "error",
                    title: data.responseJSON.error,
                    text: data.responseJSON.message
                });
            }
        });
    } else {
        Swal.fire({
            icon: "warning",
            title: "Warning",
            text: "Mohon lengkapi data yang dibutuhkan"
        });
    }
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
}
function getPurchasingDetailMessage(purchasingDetailsSubmitType) {
    if (purchasingDetailsSubmitType == "POST") {
        return "Commodity " + commodityPurchasedPurchaseDetailVar + " berhasil ditambahkan ke pembelian";
    } else {
        return "Commodity " + commodityPurchasedPurchaseDetailVar + " berhasil diubah";
    }
}
/*
 ============================================================================
 =================            Submit edit operation       ===================
 ============================================================================
 **/
function submitPurchaseDetailEditForm() {
    if (formEditPurchaseDetail.valid()) {
        var unIndexed_array = formEditPurchaseDetail.serializeArray();
        var indexed_array = {};
        var form = {};
        $.map(unIndexed_array, function (n) {
            indexed_array[n["name"]] = n["value"];
        });
        commodityPurchasedPurchaseDetailVar = indexed_array.commodityNamePurchaseDetailEditInputField;
        form = {
            id: {
                purchaseNumber: purchaseNumberIdPurchaseDetailSession,
                commodityId: indexed_array.commodityIdPurchaseDetailEditInputField,
            },
            amount: indexed_array.amountPurchaseDetailEditInputField,
            unitPrice: indexed_array.unitPricePurchaseDetailEditInputField
        };
        var value = JSON.stringify(form);
        $.ajax({
            url: "/purchasing/detail",
            type: "PUT",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            data: value,
            success: function () {
                Swal.fire({
                    icon: "success",
                    title: "Success",
                    text: getPurchasingDetailMessage("PUT")
                }).then(function () {
                    unitMeasurementNameVar = "";
                    $("#purchaseDetailEditModal").modal("hide");
                    reloadPurchasingDetailList();
                    window.parent.postMessage({message: "to_top"}, window.location.origin);
                });
            },
            error: function (data) {
                Swal.fire({
                    icon: "error",
                    title: data.responseJSON.error,
                    text: data.responseJSON.message
                });
            }
        });
    } else {
        Swal.fire({
            icon: "warning",
            title: "Warning",
            text: "Mohon lengkapi data yang dibutuhkan"
        });
    }
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
}