$(function () {
    "use strict";
    const salesNumberId = $('#salesNumberId').text().toString();
    var formInput = $("#formRequestAdd");
    var formEdit = $("#formRequestEdit");
    var commoditySaled = "";
    function getAvailableCommodity(def = null) {
            $.ajax({
                url: "/sales/availableCommodity?salesNumber=" + salesNumberId,
                type: "get",
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    var cmb = $('#commodity');
                    cmb.empty();
                    $.each(response, function (index, val) {
                        if (val.id != "0") {
                            cmb.append($('<option></option>').attr('value', val.id).text(val.name));
                        }
                    });
                    if (def != null) {
                        cmb.val(def);
                        cmb.trigger('change');
                    }
                },
                error: function (response) {
                    console.log(response);
                }
            });
        }
        getAvailableCommodity();
    formInput.validate({
        rules: {
            commodity: {required: true},
            amount: {required: true}
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("is-invalid");
        }
    });
    formEdit.validate({
            rules: {
                commodityIdEdit: {required: true},
                amountEdit: {required: true}
            },
            errorElement: "span",
            errorPlacement: function (error, element) {
                error.addClass("invalid-feedback");
                element.closest(".form-group").append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass("is-invalid");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass("is-invalid");
            }
        });
    function setHeader() {
        $('#transactionSales').addClass("active");
        $.ajax({
            url: "/sales/id?salesNumber=" + salesNumberId,
            type: 'get',
            success: function (response) {
                $("#salesDate").text(response.salesDate);
                $("#cashierName").text(response.cashierName);
                $("#grandTotal").text(response.grandTotal);
                $("#personalDiscount").text(response.personalDiscountString);
                $("#totalPayment").text(response.totalPayment);
                $("#salesDetailList").DataTable().clear().destroy();
                var table = $("#salesDetailList").DataTable({
                    data: response.salesDetails,
                    columns: [
                        {mRender: function () {
                                return '<div>' + salesNumberId + '</div>';
                            }, visible: false, searchable: false},
                        {data: "id.commodityId", visible: false, searchable: false},
                        {data: "commodity.name"},
                        {data: "amount", visible: false, searchable: false},
                        {data: "amountString"},
                        {data: "unitPrice", visible: false, searchable: false},
                        {data: "unitPriceString"},
                        {data: "total"},
                        {
                            mRender: function () {
                                return `<div align="center">
                      <a id="editFunc" href="javascript:void(0);" title="Ubah item"><i class="fas fa-edit fa-fw mr-1 edit-data-grid"></i></a>
                    </div>`;
                            }, orderable: false, searchable: false
                        }
                    ]
                });
                $("#salesDetailList").find("tbody").off("click", "#editFunc");
                $("#salesDetailList tbody").on("click", "#editFunc", function () {
                    var data = table.row($(this).parents("tr")).data();
                    setValueEdit(data);
                });
            }
        });
    }
    setHeader();
    $("#addButton").click(function () {
        addForm();
        $(".modal-title").text("Tambah item penjualan");
        $("#modal_add").modal("show");
    });
    function addForm() {
        formInput[0].reset();
        document.getElementById("commodity").disabled = false;
        document.getElementById("amount").disabled = false;
    }
    $("#backAddButton").click(function () {
        setHeader();
        $("#modal_add").modal("hide");
        parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
    });
    $("#submitAddButton").click(function () {
        if (formInput.valid()) {
            submitForm();
        } else {
            Swal.fire({
                icon: "warning",
                title: "Warning",
                text: "Mohon lengkapi data yang dibutuhkan"
            });
        }
        parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
    });
    function setValueEdit(data) {
        $("#commodityIdEdit").val(data.id.commodityId);
        document.getElementById("commodityIdEdit").disabled = false;
                $("#commodityEdit").val(data.commodity.name);
                document.getElementById("commodityEdit").disabled = false;
        $("#amountEdit").val(data.amount);
        document.getElementById("amountEdit").disabled = false;
        $(".modaledit-title").text("Ubah item " + data.commodityName);
        $("#modal_edit").modal("show");
    }
    $("#backEditButton").click(function () {
            setHeader();
            $("#modal_edit").modal("hide");
            parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
        });
        $("#submitEditButton").click(function () {
            if (formEdit.valid()) {
                submitEditForm();
            } else {
                Swal.fire({
                    icon: "warning",
                    title: "Warning",
                    text: "Mohon lengkapi data yang dibutuhkan"
                });
            }
            parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
        });
    function getMessage(jenisSubmit) {
        if (jenisSubmit == "POST") {
            return "Commodity " + commoditySaled + " berhasil ditambahkan ke penjualan";
        } else {
            return "Commodity " + commoditySaled + " berhasil diubah";
        }
    }



    function submitForm() {
            var unindexed_array = formInput.serializeArray();
            var indexed_array = {};
            var form = {};
            $.map(unindexed_array, function (n) {
                indexed_array[n["name"]] = n["value"];
            });
            var commodityCombo = document.getElementById("commodity");
            commoditySaled = commodityCombo.options[commodityCombo.selectedIndex].innerHTML;
            form = {
                id: {
                    salesNumber: salesNumberId,
                    commodityId: indexed_array.commodity,
                },
                amount: indexed_array.amount
            };
            var value = JSON.stringify(form);
            $.ajax({
                url: "/sales/detail",
                type: "POST",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                data: value,
                success: function () {
                    Swal.fire({
                        title: "Success",
                        text: getMessage("POST")
                    }).then(function () {
                        commoditySaled = "";
                        $("#modal_add").modal("hide");
                        setHeader();
                        getAvailableCommodity();
                        window.parent.postMessage({message: "to_top"}, window.location.origin);
                    });
                },
                error: function (data) {
                    Swal.fire({
                        icon: "error",
                        title: data.statusText,
                        text: data.responseJSON.message
                    });
                }
            });
        }
        function submitEditForm() {
            var unindexed_array = formEdit.serializeArray();
            var indexed_array = {};
            var form = {};
            $.map(unindexed_array, function (n) {
                indexed_array[n["name"]] = n["value"];
            });
            commoditySaled = indexed_array.commodityEdit;
            form = {
                id: {
                    salesNumber: salesNumberId,
                    commodityId: indexed_array.commodityIdEdit,
                },
                amount: indexed_array.amountEdit
            };
            var value = JSON.stringify(form);
            $.ajax({
                url: "/sales/detail",
                type: "PUT",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                data: value,
                success: function () {
                    Swal.fire({
                        title: "Success",
                        text: getMessage("PUT")
                    }).then(function () {
                        commoditySaled = "";
                        $("#modal_edit").modal("hide");
                        setHeader();
                        window.parent.postMessage({message: "to_top"}, window.location.origin);
                    });
                },
                error: function (data) {
                    Swal.fire({
                        icon: "error",
                        title: data.statusText,
                        text: data.responseJSON.message
                    });
                }
            });
        }
});