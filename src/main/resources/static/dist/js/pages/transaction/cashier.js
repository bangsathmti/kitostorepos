"use strict";
var formCashierEdit = $("#formCashierEdit");
var formCashierSave = $("#formCashierSave");
var cashierCommodityName = "";
var cashierCommodityAmount = 0;
/*
============================================================================
=================                Initiation              ===================
============================================================================
**/
function setCashierMenuContent() {
    document.getElementById("cashierName").innerHTML = "Kasir : " + securityHolderUsername;
    formCashierEdit = $("#formCashierEdit");
    formCashierSave = $("#formCashierSave");
    formCashierSave.validate({
        rules: {
            personalDiscount: {required: true}
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("is-invalid");
        }
    });
    formCashierEdit.validate({
        rules: {
            commodityId: {required: true},
            amount: {required: true}
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("is-invalid");
        }
    });
    reloadCashierMenuContent();
}
/*
============================================================================
=================                reload list             ===================
============================================================================
**/
function reloadCashierMenuContent(){
    document.getElementById("barcodeText").focus();
    $("#barcodeText").val("");
    cashierCommodityName = "";
    cashierCommodityAmount = 0;
    $.ajax({
        url: "/sales/cashier/grandTotal",
        type: "get",
        success: function (response) {
            $("#cashierGrandTotal").text(response.grandTotal);
            $("#cashierList").DataTable().clear().destroy();
            var table = $("#cashierList").DataTable({
                paging: false,
                searching: false,
                lengthChange: false,
                serverMethod: "GET",
                language: {
                    processing: '<img src="/dist/img/logokito.png">'
                },
                ajax: {
                    url: "/sales/cashier",
                    dataSrc: ""
                },
                columns: [
                    {data: "commodityId", visible: false, searchable: false},
                    {data: "commodityName"},
                    {data: "amount", visible: false, searchable: false},
                    {data: "amountString"},
                    {data: "unitPrice", visible: false, searchable: false},
                    {data: "unitPriceString"},
                    {data: "total"},
                    {
                        mRender: function () {
                            return `<div align="center"><a id="editCashierFunc" href="javascript:void(0);" title="Ubah data"><i class="fas fa-edit fa-fw mr-1 edit-data-grid"></i></a><a id="deletecashierFunc" href="javascript:void(0);" title="Hapus item"><i class="fas fa-trash fa-fw text-red"></i></a></div>`;
                        }, orderable: false, searchable: false
                    }
                ]
            });
            $("#cashierList").find("tbody").off("click", "#editCashierFunc");
            $("#cashierList").find("tbody").off("click", "#deletecashierFunc");
            $("#cashierList tbody").on("click", "#editCashierFunc", function () {
                var data = table.row($(this).parents("tr")).data();
                editCashierCommodityCachePreparation(data);
            });
            $("#cashierList tbody").on("click", "#deletecashierFunc", function () {
                var data = table.row($(this).parents("tr")).data();
                deleteCashierCommodityCache(data);
            });
        }
    });
}
/*
============================================================================
=================       barcode operation on enter       ===================
============================================================================
**/
function barcodeTextKeyDown(barcode){
    if (event.key === 'Enter') {
        addCacheData();
    }
}
/*
============================================================================
=================             add cache data             ===================
============================================================================
**/
function addCacheData() {
    var value = $("#barcodeText").val();
    $.ajax({
        url: "/sales/cashier?barcode=" + value,
        type: "POST",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        success: function (response) {
            cashierCommodityName = "";
            cashierCommodityAmount = 0;
            $("#barcodeText").val("");
            reloadCashierMenuContent();
        },
        error: function (data) {
            cashierCommodityName = "";
            cashierCommodityAmount = 0;
            $("#barcodeText").val("");
            Swal.fire({
                icon: "error",
                title: data.responseJSON.error,
                text: data.responseJSON.message
            }).then(function () {
                document.getElementById("barcodeText").focus();
            });
        }
    });
}
/*
============================================================================
=================               edit modal               ===================
============================================================================
**/
function editCashierCommodityCachePreparation(data) {
    $("#barcodeText").val("");
    $("#commodityId").val(data.commodityId);
    document.getElementById("commodityId").disabled = false;
    $("#commodityName").val(data.commodityName);
    document.getElementById("commodityName").disabled = false;
    $("#amount").val(data.amount);
    document.getElementById("amount").disabled = false;
    $("#cashierEditTitle").text("Ubah jumlah penjualan " + data.commodityName);
    $("#cashierEdit").modal("show");
}
/*
============================================================================
=================          cancel edit cache             ===================
============================================================================
**/
function backCashierEdit(){
    document.getElementById("barcodeText").focus();
    $("#cashierEdit").modal("hide");
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
}
/*
============================================================================
=================          submit edit cache             ===================
============================================================================
**/
function submitEditCashierCache(){
    if (formCashierEdit.valid()) {
        $("#barcodeText").val("");
        var unIndexed_array = formCashierEdit.serializeArray();
        var indexed_array = {};
        var form = {};
        $.map(unIndexed_array, function (n) {
            indexed_array[n["name"]] = n["value"];
        });
        cashierCommodityName = indexed_array.commodityName;
        cashierCommodityAmount = indexed_array.amount;
        form = {
            commodityId: indexed_array.commodityId,
            amount: indexed_array.amount
        };
        var value = JSON.stringify(form);
        $.ajax({
            url: "/sales/cashier",
            type: "PUT",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            data: value,
            success: function () {
                Swal.fire({
                    icon: "success",
                    title: "Success",
                    text: "Jumlah " + cashierCommodityName + " diubah ke " + cashierCommodityAmount
                }).then(function () {
                    cashierCommodityName = "";
                    cashierCommodityAmount = 0;
                    $("#cashierEdit").modal("hide");
                    reloadCashierMenuContent();
                    window.parent.postMessage({message: "to_top"}, window.location.origin);
                });
            },
            error: function (data) {
                Swal.fire({
                    icon: "error",
                    title: data.responseJSON.error,
                    text: data.responseJSON.message
                }).then(function () {
                    document.getElementById("barcodeText").focus();
                });
            }
        });
    } else {
        $("#barcodeText").val("");
        Swal.fire({
            icon: "warning",
            title: "Warning",
            text: "Mohon lengkapi data yang dibutuhkan"
        });
    }
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
}
/*
============================================================================
=================               delete cache             ===================
============================================================================
**/
function deleteCashierCommodityCache(salesDetail) {
    cashierCommodityName = "";
    cashierCommodityAmount = 0;
    Swal.fire({
        title: "Anda yakin akan menghapus commodity " + salesDetail.commodityName + " dari penjualan",
        icon: "warning",
        showCancelButton: true,
        dangerMode: true
    }).then(function (result) {
        if (result.isConfirmed) {
            $.ajax({
                url: "/sales/cashier/commodity?commodityId=" + salesDetail.commodityId,
                type: "DELETE",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    Swal.fire({
                        title: "Commodity " + salesDetail.commodityName + " berhasil dihapus dari penjualan",
                        icon: "success"
                    });
                    reloadCashierMenuContent();
                },
                error: function (data) {
                    Swal.close();
                    if (data.status === 200 || data.status === 201) {
                        Swal.fire({
                            icon: "success",
                            title: "Success",
                            text: "Commodity " + salesDetail.commodityName + " berhasil dihapus dari penjualan"
                        }).then(function () {
                            reloadCashierMenuContent();
                        });
                    } else {
                        Swal.fire({
                            icon: "error",
                            title: data.responseJSON.error,
                            text: data.responseJSON.message
                        });
                    }
                },
            })
        }
    });
}
/*
============================================================================
=================             sales processing           ===================
============================================================================
**/
function processCashierSales(){
    if(securityHolderRole == "KASIR" || securityHolderRole == "ADMIN"){
        saveCashierSales();
    }else{
        formCashierSave[0].reset();
        document.getElementById("grandTotalAfterDiscount").innerHTML = document.getElementById("cashierGrandTotal").innerHTML;
        document.getElementById("grandTotalBeforeDiscount").value = document.getElementById("cashierGrandTotal").innerHTML;
        document.getElementById("cashierPersonalDiscount").disabled = false;
        document.getElementById("cashierPersonalDiscount").defaultValue = 0;
        $("#saveCashierSalesModalTitle").text("Proses penjualan");
        $("#saveCashierSalesModal").modal("show");
    }
}
/*
============================================================================
=================  calculate grand total after discount  ===================
============================================================================
**/
function calculateGrandTotalAfterDiscountChange(){
    var grandTotalBeforeText = document.getElementById("grandTotalBeforeDiscount").value.replace("Rp ","").replace(".","").replace(",",".");
    var grandTotalBefore = parseFloat(grandTotalBeforeText);
    var grandTotalAfter = grandTotalBefore - document.getElementById("cashierPersonalDiscount").value;
    document.getElementById("grandTotalAfterDiscount").innerHTML = "Rp " + grandTotalAfter.toLocaleString().replace(",", ":").replace(".", ",").replace(":", ".");
}
/*
============================================================================
=================        cancel sales processing         ===================
============================================================================
**/
function cancelSalesProcessing(){
    $("#saveCashierSalesModal").modal("hide");
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
}
/*
============================================================================
=================             sales processing           ===================
============================================================================
**/
function saveCashierSales(){
    if (formCashierSave.valid()) {
        var unIndexed_array = formCashierSave.serializeArray();
        var indexed_array = {};
        var form = {};
        $.map(unIndexed_array, function (n) {
            indexed_array[n["name"]] = n["value"];
        });
        form = {
            personalDiscount: indexed_array.cashierPersonalDiscount
        };
        var value =  JSON.stringify(form);
        $.ajax({
            url: "/sales/add",
            type: "POST",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            data: value,
            success: function (response) {
                reloadCashierMenuContent();
                $("#saveCashierSalesModal").modal("hide");
                document.getElementById("receiptNumberSession").innerHTML = response.salesNumber;
                parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
                changeContent('receipt');
            },
            error: function (data) {
                document.getElementById("barcodeText").focus();
                $("#barcodeText").val("");
                Swal.fire({
                    icon: "error",
                    title: data.responseJSON.error,
                    text: data.responseJSON.message
                });
            }
        });
    } else {
        Swal.fire({
            icon: "warning",
            title: "Warning",
            text: "Mohon lengkapi data yang dibutuhkan"
        });
    }
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
}