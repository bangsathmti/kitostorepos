$(function () {
    "use strict";
    const securityHolderRole = $('#securityHolderRole').text().toString();
    var formEdit = $("#formEdit");
    var formSave = $("#formSave");
    var commodityName = "";
    var commodityAmount = 0;
    formSave.validate({
        rules: {
            personalDiscount: {required: true}
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("is-invalid");
        }
    });
    formEdit.validate({
        rules: {
            commodityId: {required: true},
            amount: {required: true}
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("is-invalid");
        }
    });
    function setData() {
        $('#transactionCashier').addClass("active");
        $("#barcodeText").val("");
        $.ajax({
            url: "/sales/cashier/grandTotal",
            type: "get",
            success: function (response) {
                $("#grandTotal").text(response.grandTotal);
                $("#cashierList").DataTable().clear().destroy();
                var table = $("#cashierList").DataTable({
                    paging: false,
                    searching: false,
                    lengthChange: false,
                    serverMethod: "GET",
                    ajax: {
                        url: "/sales/cashier",
                        dataSrc: ""
                    },
                    columns: [
                        {data: "commodityId", visible: false, searchable: false},
                        {data: "commodityName"},
                        {data: "amount", visible: false, searchable: false},
                        {data: "amountString"},
                        {data: "unitPrice", visible: false, searchable: false},
                        {data: "unitPriceString"},
                        {data: "total"},
                        {
                            mRender: function () {
                                return `<div align="center">
                      <a id="editFunc" href="javascript:void(0);" title="Ubah data"><i class="fas fa-edit fa-fw mr-1 edit-data-grid"></i></a>
                      <a id="deleteFunc" href="javascript:void(0);" title="Hapus item"><i class="fas fa-trash fa-fw text-red"></i></a>
                    </div>`;
                            }, orderable: false, searchable: false
                        }
                    ]
                });
                $("#cashierList").find("tbody").off("click", "#editFunc");
                $("#cashierList").find("tbody").off("click", "#deleteFunc");
                $("#cashierList tbody").on("click", "#editFunc", function () {
                    var data = table.row($(this).parents("tr")).data();
                    setValueEdit(data);
                });
                $("#cashierList tbody").on("click", "#deleteFunc", function () {
                    var data = table.row($(this).parents("tr")).data();
                    deleteFunc(data);
                });
            }
        });
    }
    setData();
    $("#addButton").click(function () {
        addForm();
        if(securityHolderRole == "KASIR" || securityHolderRole == "ADMIN"){
            saveTransaction();
        }else{
            $(".modalsave-title").text("Proses penjualan");
            $("#modal_save").modal("show");
        }
    });
    function addForm() {
        formSave[0].reset();
        document.getElementById("personalDiscount").disabled = false;
        document.getElementById("personalDiscount").defaultValue = 0;
    }
    $("#backTransaction").click(function () {
        setData();
        $("#modal_save").modal("hide");
        parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
    });
    $("#saveTransaction").click(function () {
        if (formSave.valid()) {
            saveTransaction();
        } else {
            Swal.fire({
                icon: "warning",
                title: "Warning",
                text: "Mohon lengkapi data yang dibutuhkan"
            });
        }
        parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
    });
    function getTransactionValue($form) {
        var unindexed_array = $form.serializeArray();
        var indexed_array = {};
        var form = {};
        $.map(unindexed_array, function (n) {
            indexed_array[n["name"]] = n["value"];
        });
        form = {
            personalDiscount: indexed_array.personalDiscount
        };
        return JSON.stringify(form);
    }
    function saveTransaction() {
        var value = getTransactionValue(formSave);
        $.ajax({
            url: "/sales/add",
            type: "POST",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            data: value,
            success: function (response) {
                window.location.href = "/transaction/cashier/struct?salesNumber=" + response.salesNumber+"&source=cashier";
            },
            error: function (data) {
                Swal.fire({
                    icon: "error",
                    title: data.statusText,
                    text: data.responseJSON.message
                });
            }
        });
    }
    function deleteFunc(salesDetail) {
        commodityName = "";
        commodityAmount = 0;
        Swal.fire({
            title: "Anda yakin akan menghapus commodity " + salesDetail.commodityName + " dari penjualan",
            icon: "warning",
            showCancelButton: true,
            dangerMode: true
        }).then(function (result) {
            if (result.isConfirmed) {
                $.ajax({
                    url: "/sales/cashier/commodity?commodityId=" + salesDetail.commodityId,
                    type: "DELETE",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        Swal.fire({
                            title: "Commodity " + salesDetail.commodityName + " berhasil dihapus dari penjualan",
                            icon: "success"
                        });
                        setData();
                    },
                    error: function (data) {
                        Swal.close();
                        if (data.status === 200 || data.status === 201) {
                            Swal.fire({
                                title: "Success",
                                text: "Commodity " + salesDetail.commodityName + " berhasil dihapus dari penjualan"
                            }).then(function () {
                                setData();
                            });
                        } else {
                            Swal.fire({
                                icon: "error",
                                title: "Error",
                                text: data.responseJSON.message
                            });
                        }
                    },
                })
            } else {
                Swal.fire("Cancel", "Commodity " + salesDetail.commodityName + " batal dihapus dari penjualan");
            }
        });
    }
    function setValueEdit(data) {
        $("#barcodeText").val("");
        $("#commodityId").val(data.commodityId);
        document.getElementById("commodityId").disabled = false;
        $("#commodityName").val(data.commodityName);
        document.getElementById("commodityName").disabled = false;
        $("#amount").val(data.amount);
        document.getElementById("amount").disabled = false;
        $(".modal-title").text("Ubah jumlah penjualan " + data.commodityName);
        $("#modal_edit").modal("show");
    }
    $("#backEdit").click(function () {
        setData();
        $("#modal_edit").modal("hide");
        parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
    });
    $("#submitEdit").click(function () {
        if (formEdit.valid()) {
            submitEditForm();
        } else {
            $("#barcodeText").val("");
            Swal.fire({
                icon: "warning",
                title: "Warning",
                text: "Mohon lengkapi data yang dibutuhkan"
            });
        }
        parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
    });
    function getMessage() {
        return "Jumlah " + commodityName + " diubah ke " + commodityAmount;
    }
    function getFormEditValue($form) {
        var unindexed_array = $form.serializeArray();
        var indexed_array = {};
        var form = {};
        $.map(unindexed_array, function (n) {
            indexed_array[n["name"]] = n["value"];
        });
        commodityName = indexed_array.commodityName;
        commodityAmount = indexed_array.amount;
        form = {
            commodityId: indexed_array.commodityId,
            amount: indexed_array.amount
        };
        return JSON.stringify(form);
    }
    function submitEditForm() {
        $("#barcodeText").val("");
        var value = getFormEditValue(formEdit);
        $.ajax({
            url: "/sales/cashier",
            type: "PUT",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            data: value,
            success: function () {
                Swal.fire({
                    title: "Success",
                    text: getMessage()
                }).then(function () {
                    suksesEdit();
                });
            },
            error: function (data) {
                Swal.fire({
                    icon: "error",
                    title: data.statusText,
                    text: data.responseJSON.message
                });
            }
        });
    }
    function suksesEdit() {
        commodityName = "";
        commodityAmount = 0;
        $("#modal_edit").modal("hide");
        setData();
        window.parent.postMessage({message: "to_top"}, window.location.origin);
    }
    function addSuccess() {
        commodityName = "";
        commodityAmount = 0;
        $("#barcodeText").val("");
        setData();
    }
    $("#barcodeText").keypress(function (e) {
        var key = e.which;
        if (key == 13) {
            addCacheData();
        }
    });
    $("#addCacheBtn").click(function () {
        addCacheData();
    });
    function addCacheData() {
        var value = $("#barcodeText").val();
        $.ajax({
            url: "/sales/cashier?barcode=" + value,
            type: "POST",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            success: function (response) {
                addSuccess(response);
            },
            error: function (data) {
                Swal.fire({
                    icon: "error",
                    title: data.statusText,
                    text: data.responseJSON.message
                });
            }
        });
    }
});