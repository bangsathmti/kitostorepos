"use strict";
var formInputPenjualan = $("#formRequestAddPenjualan");
var formPenjualanSubmitType = "";
var penjualanNumberVar = "";

/*
 ============================================================================
 =================                Initiation              ===================
 ============================================================================
 **/

function setSalesMenuContent() {
    formInputPenjualan = $("#formRequestAddPenjualan");
    formPenjualanSubmitType = "";
    penjualanNumberVar = "";

    formInputPenjualan.validate({
        rules: {
            penjualanNumber: {required: true},
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("is-invalid");
        }
    });

    reloadPenjualanList();
}

function reloadPenjualanList() {
    $("#salesList").DataTable().clear().destroy();
    var table = $("#salesList").DataTable({
        serverSide: true,
        processing: true,
        paging: true,
        searching: false,
        ordering: false,
        order: [],
        lengthChange: false,
        serverMethod: "GET",
        language: {
            processing: '<img src="/dist/img/logokito.png">'
        },
        ajax: {
            url: "/sales",
            dataSrc: function (data) {
                data.recordsTotal = data.totalElements;
                data.recordsFiltered = data.totalElements;
                return data.content;
            },
            data: function (data) {
                var page = 0;
                if (data.start != 0) {
                    page = data.start / data.length;
                }
                const exUrl = "page=" + page;
                return exUrl;
            }
        },
        columns: [
            {data: "salesNumber", orderable: false},
            {data: "salesDate", orderable: false},
            {data: "cashierName", orderable: false},
            {data: "grandTotal", orderable: false},
            {data: "personalDiscount", visible: false, searchable: false},
            {data: "personalDiscountString", orderable: false},
            {data: "totalPayment", orderable: false},
            {
                mRender: function () {
                    return `<div align="center">
                        <a id="viewPenjualanDetail" href="javascript:void(0);" title="Lihat detail"><i class="fas fa-eye fa-fw mr-1 edit-data-grid text-green"></i></a>
                      <a id="editFuncPenjualan" href="javascript:void(0);" title="Ubah data penjualan"><i class="fas fa-edit fa-fw mr-1 edit-data-grid"></i></a>
                    <a id="viewReceiptPenjualan" href="javascript:void(0);" title="Lihat struk"><i class="fas fa-receipt fa-fw mr-1 edit-data-grid text-red"></i></a>
                    </div>`;
                }, orderable: false, searchable: false
            }
        ]
    });

    $("#salesList").find("tbody").off("click", "#viewPenjualanDetail");
    $("#salesList").find("tbody").off("click", "#editFuncPenjualan");
    $("#salesList").find("tbody").off("click", "#viewReceiptPenjualan");
    $("#salesList tbody").on("click", "#editFuncPenjualan", function () {
        var data = table.row($(this).parents("tr")).data();
        setSalesMenuEditInitiation(data);
    });
    $("#salesList tbody").on("click", "#viewPenjualanDetail", function () {
        var data = table.row($(this).parents("tr")).data();
        openSalesMenuViewDetails(data);
    });
    $("#salesList tbody").on("click", "#viewReceiptPenjualan", function () {
        var data = table.row($(this).parents("tr")).data();
        viewSalesReceiptMenuSales(data);
    });
}

/*
 ============================================================================
 =================                Cancel modal            ===================
 ============================================================================
 **/

function cancelPenjualanOperation() {
    formSupplierSubmitType = "";
    $("#penjualanModal").modal("hide");
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
}
/*
 ============================================================================
 =================                add modal               ===================
 ============================================================================
 **/
function addPenjualanInitiation() {
    formPenjualanSubmitType = "POST";
    formInputPenjualan[0].reset();
    document.getElementById("penjualanDiscount").disabled = false;
    document.getElementById("penjualanDiscount").defaultValue = 0;
    $("#penjualan-modal-title").text("Tambah supplier");
    $("#penjualanModal").modal("show");
}
/*
 ============================================================================
 =================                edit modal              ===================
 ============================================================================
 **/
function editPenjualanInitiation(data) {
    formSupplierSubmitType = "PUT";
    $("#penjualanNumber").val(data.salesNumber);
    document.getElementById("penjualanNumber").disabled = false;
    $("#penjualanDiscount").val(data.personalDiscount);
    document.getElementById("penjualanDiscount").disabled = false;
    $("#penjualanModal").modal("show");
}
/*
 ============================================================================
 =================                Submit modal              ===================
 ============================================================================
 **/
function submitPenjualanForm() {
    if (formInputPenjualan.valid()) {
        var unIndexed_array = formInputPenjualan.serializeArray();
        var indexed_array = {};
        var form = {};
        $.map(unIndexed_array, function (n) {
            indexed_array[n["name"]] = n["value"];
        });
        penjualanNumberVar = indexed_array.penjualanNumber;
        if (formPenjualanSubmitType == "POST") {
            form = {
                personalDiscount: indexed_array.penjualanDiscount
            };
        } else {
            form = {
                salesNumber: indexed_array.penjualanNumber,
                personalDiscount: indexed_array.penjualanDiscount
            };
        }
        var value = JSON.stringify(form);
        $.ajax({
            url: "/sales",
            type: formPenjualanSubmitType,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            data: value,
            success: function (response) {
                if (formPenjualanSubmitType == "POST") {
                    penjualanNumberVar = response.salesNumber;
                }
                Swal.fire({
                    icon: "success",
                    title: "Success",
                    text: getPenjualanOparationMessage()
                }).then(function () {
                    penjualanNumberVar = "";
                    $("#penjualanModal").modal("hide");
                    reloadPenjualanList();
                    window.parent.postMessage({message: "to_top"}, window.location.origin);
                });
            },
            error: function (data) {
                Swal.fire({
                    icon: "error",
                    title: data.responseJSON.error,
                    text: data.responseJSON.message
                });
            }
        });
    } else {
        Swal.fire({
            icon: "warning",
            title: "Warning",
            text: "Mohon lengkapi data yang dibutuhkan"
        });
    }
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
}

//    var value = getFormValue(formInput);
//    $.ajax({
//        url: "/sales",
//        type: formPenjualanSubmitType,
//        beforeSend: function (xhr) {
//            xhr.setRequestHeader("Content-Type", "application/json");
//        },
//        data: value,
//        success: function (response) {
//            if (formPenjualanSubmitType == "POST") {
//                penjualanNumberVar = response.salesNumber;
//            }
//            Swal.fire({
//                title: "Success",
//                text: getMessage()
//            }).then(function () {
//                suksesSubmit();
//            });
//        },
//        error: function (data) {
//            Swal.fire({
//                icon: "error",
//                title: data.statusText,
//                text: data.responseJSON.message
//            });
//        }
//    });
//}

function getPenjualanOparationMessage() {
    if (formPenjualanSubmitType == "POST") {
        return "Supplier " + penjualanNumberVar + " berhasil ditambahkan";
    } else {
        return "Supplier diubah ke " + penjualanNumberVar;
    }
}

function viewSalesReceiptMenuSales(data) {
    document.getElementById("receiptNumberSession").innerHTML = data.salesNumber;
    changeContent('receipt');
}

function setSalesMenuEditInitiation(data) {
    formPenjualanSubmitType = "PUT";
    $("#penjualanNumber").val(data.salesNumber);
    document.getElementById("penjualanNumber").disabled = false;
    $("#penjualanDiscount").val(data.personalDiscount);
    document.getElementById("penjualanDiscount").disabled = false;
    $("#penjualan-modal-title").text("Ubah data penjualan " + data.salesNumber);
    $("#penjualanModal").modal("show");
}

function openSalesMenuViewDetails(data) {
    document.getElementById("receiptNumberSession").innerHTML = data.salesNumber;
    changeContent('salesDetail');
}






