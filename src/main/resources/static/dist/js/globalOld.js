'use strict'
function getComboCategory(def = null) {
    $.ajax({
        url: "/category",
        type: "get",
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            var cmb = $('#category');
            cmb.empty();
            $.each(response, function (index, val) {
                if (val.id != "0") {
                    cmb.append($('<option></option>').attr('value', val.id).text(val.name));
                }
            });
            if (def != null) {
                cmb.val(def);
                cmb.trigger('change');
            }
        },
        error: function (response) {
            console.log(response);
        }
    });
}
function getComboSalesMeasurement(def = null) {
    $.ajax({
        url: "/salesMeasurement",
        type: "get",
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            var cmb = $('#salesMeasurement');
            cmb.empty();
            $.each(response, function (index, val) {
                if (val.id != "0") {
                    cmb.append($('<option></option>').attr('value', val.id).text(val.name));
                }
            });
            if (def != null) {
                cmb.val(def);
                cmb.trigger('change');
            }
        },
        error: function (response) {
            console.log(response);
        }
    });
}
function getComboUnitMeasurement(def = null) {
    $.ajax({
        url: "/unitMeasurement",
        type: "get",
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            var cmb = $('#unitMeasurement');
            cmb.empty();
            $.each(response, function (index, val) {
                if (val.id != "0") {
                    cmb.append($('<option></option>').attr('value', val.id).text(val.name));
                }
            });
            if (def != null) {
                cmb.val(def);
                cmb.trigger('change');
            }
        },
        error: function (response) {
            console.log(response);
        }
    });
}
function getComboSupplier(def = null) {
    $.ajax({
        url: "/supplier",
        type: "get",
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            var cmb = $('#supplier');
            cmb.empty();
            $.each(response, function (index, val) {
                if (val.id != "0") {
                    cmb.append($('<option></option>').attr('value', val.id).text(val.name));
                }
            });
            if (def != null) {
                cmb.val(def);
                cmb.trigger('change');
            }
        },
        error: function (response) {
            console.log(response);
        }
    });
}