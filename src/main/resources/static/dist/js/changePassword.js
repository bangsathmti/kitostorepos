"use strict";
var changePasswordForm = $("#changePasswordForm");
/*
 ============================================================================
 =================                Initiation              ===================
 ============================================================================
 **/
function changePasswordInitiation(){
    changePasswordForm = $("#changePasswordForm");
    changePasswordForm.validate({
        rules: {
            oldPassword: {required: true},
            newPassword: {required: true},
            newPasswordConfirmation: {required: true}
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("is-invalid");
        }
    });
    changePasswordForm[0].reset();
    $("#modal_change_password").modal("show");
}
/*
 ============================================================================
 =================          cancelChangePassword          ===================
 ============================================================================
 **/
function cancelChangePassword(){
    $("#modal_change_password").modal("hide");
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
}
/*
 ============================================================================
 =================             changePassword             ===================
 ============================================================================
 **/
 function submitChangePassword(){
    if (changePasswordForm.valid()) {
        var unindexed_array = changePasswordForm.serializeArray();
        var indexed_array = {};
        var form = {};
        $.map(unindexed_array, function (n) {
            indexed_array[n["name"]] = n["value"];
        });
        form = {
            oldPassword: indexed_array.oldPassword,
            newPassword: indexed_array.newPassword,
            newPasswordConfirmation: indexed_array.newPasswordConfirmation
        };
        var value = JSON.stringify(form);
        $.ajax({
            url: "/userApp/changePassword",
            type: "PUT",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            data: value,
            success: function () {
                Swal.fire({
                    title: "Success",
                    text: "Change password success"
                }).then(function () {
                    $("#modal_change_password").modal("hide");
                    window.parent.postMessage({message: "to_top"}, window.location.origin);
                });
            },
            error: function (data) {
                Swal.fire({
                    icon: "error",
                    title: data.statusText,
                    text: data.responseJSON.message
                });
            }
        });
    } else {
        Swal.fire({
            icon: "warning",
            title: "Warning",
            text: "Please complete the input form"
        });
    }
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
    parent.postMessage(JSON.stringify({to_top: "to_top"}), "*");
 }