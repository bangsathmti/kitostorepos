"use strict";
const securityHolderRole = $('#securityHolderRole').text().toString();
const securityHolderUsername = $('#securityHolderUsername').text().toString();
//function clearMenuActive(){
//    if(securityHolderRole != "KASIR"){
//        document.getElementById("dashboardMenu").classList.remove("active");
//        document.getElementById("userAppMenu").classList.remove("active");
//        document.getElementById("transactionSales").classList.remove("active");
//        document.getElementById("masterCategory").classList.remove("active");
//        document.getElementById("masterSupplier").classList.remove("active");
//    }
//    document.getElementById("transactionCashier").classList.remove("active");
//}
function loadContent(moduleName = "home"){
    switch(moduleName) {
        case "home":
            document.getElementById("dashboardMenu").classList.add("active");
            setDashboardData();
            break;
        case "userApp":
            setUserMenuContent();
            document.getElementById("userAppMenu").classList.add("active");
            break;
        case "category":
            document.getElementById("masterCategory").classList.add("active");
            setCategoryMenuContent();
            break;
        case "supplier":
            document.getElementById("masterSupplier").classList.add("active");
            setSupplierMenuContent();
            break;
        case "unitMeasurement":
            document.getElementById("masterUnitMeasurement").classList.add("active");
            setUnitMeasurementMenuContent();
            break;
        case "salesMeasurement":
            document.getElementById("masterSalesMeasurement").classList.add("active");
            setSalesMeasurementMenuContent();
            break;
        case "commodity":
            document.getElementById("masterCommodity").classList.add("active");
            setCommodityMenuContent();
            break;
        case "purchasing":
            document.getElementById("transactionPurchasing").classList.add("active");
            setPurchasingMenuContent();
            break;
        case "purchasingDetail":
            document.getElementById("transactionPurchasing").classList.add("active");
            setPurchasingDetailMenuContent();
            break;
        case "cashier":
            document.getElementById("transactionCashier").classList.add("active");
            setCashierMenuContent();
            break;
        case "receipt":
            document.getElementById("transactionCashier").classList.add("active");
            document.getElementById("receiptNumberPrintPreview").textContent = $('#receiptNumberSession').text().toString();
            setReceiptData();
            break;
        case "sales":
            document.getElementById("transactionSales").classList.add("active");
            setSalesMenuContent();
            break;
        case "salesDetail":
            document.getElementById("transactionSales").classList.add("active");
            setSalesDetailMenuContent();
            break;
    }
}
function changeContent(moduleName = "home"){
    const xhttpMenu = new XMLHttpRequest();
    xhttpMenu.onload = function() {document.getElementById("menuSidebarDiv").innerHTML = this.responseText;
    //    clearMenuActive();
        var prefixUri = "/page?page=";
        var uri = "home";
        switch(moduleName) {
            case "home":
                if(securityHolderRole == "KASIR"){uri = "transaction/cashier"; moduleName="cashier";}else{uri = "home";}
                break;
            case "userApp":
                uri = "master/userApp";
                break;
            case "category":
                uri = "master/category";
                break;
            case "supplier":
                uri = "master/supplier";
                break;
            case "unitMeasurement":
                uri = "master/unitMeasurement";
                break;
            case "salesMeasurement":
                uri = "master/salesMeasurement";
                break;
            case "commodity":
                uri = "master/commodity";
                break;
            case "purchasing":
                uri = "transaction/purchasing";
                break;
            case "purchasingDetail":
                uri = "transaction/purchasingDetail";
                break;
            case "cashier":
                uri = "transaction/cashier";
                document.getElementById("menuBeforeReceipt").innerHTML = 'cashier';
                break;
            case "receipt":
                uri = "transaction/receipt";
                break;
            case "sales":
                uri = "transaction/sales";
                document.getElementById("menuBeforeReceipt").innerHTML = 'sales';
                break;
            case "salesDetail":
                uri = "transaction/salesDetail";
                document.getElementById("menuBeforeReceipt").innerHTML = 'salesDetail';
                break;
          default:
                uri = "notFound";
        }
        const xhttp = new XMLHttpRequest();
        xhttp.onload = function() {
            document.getElementById("contentBody").innerHTML = this.responseText;
            loadContent(moduleName);
        }
        xhttp.open("GET", prefixUri + uri, true);
        xhttp.send();
    }
    xhttpMenu.open("GET",  "/fragment?page=sidebar", true);
    xhttpMenu.send();
}
changeContent();
function onlyNumberKey(evt) {
    var ASCIICode = evt.which ? evt.which : evt.keyCode;
    if ((ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57)) && (ASCIICode != 13)) return false;
    return true;
}